-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Client: 127.0.0.1
-- Généré le: Mer 10 Juin 2015 à 07:49
-- Version du serveur: 5.6.14
-- Version de PHP: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `corbeilassoc`
--

-- --------------------------------------------------------

--
-- Structure de la table `crbass_commentmeta`
--

CREATE TABLE IF NOT EXISTS `crbass_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `crbass_comments`
--

CREATE TABLE IF NOT EXISTS `crbass_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=2 ;

--
-- Contenu de la table `crbass_comments`
--

INSERT INTO `crbass_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Monsieur WordPress', '', 'https://wordpress.org/', '', '2014-12-24 18:35:51', '2014-12-24 18:35:51', 'Bonjour, ceci est un commentaire.\nPour supprimer un commentaire, connectez-vous et affichez les commentaires de cet article. Vous pourrez alors les modifier ou les supprimer.', 0, '1', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `crbass_eo_events`
--

CREATE TABLE IF NOT EXISTS `crbass_eo_events` (
  `event_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) NOT NULL,
  `StartDate` date NOT NULL,
  `EndDate` date NOT NULL,
  `StartTime` time NOT NULL,
  `FinishTime` time NOT NULL,
  `event_occurrence` bigint(20) NOT NULL,
  PRIMARY KEY (`event_id`),
  KEY `StartDate` (`StartDate`),
  KEY `EndDate` (`EndDate`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `crbass_eo_events`
--

INSERT INTO `crbass_eo_events` (`event_id`, `post_id`, `StartDate`, `EndDate`, `StartTime`, `FinishTime`, `event_occurrence`) VALUES
(1, 4, '2014-12-28', '2014-12-28', '08:00:00', '12:00:00', 0),
(3, 130, '2015-03-23', '2015-03-23', '00:00:00', '23:59:00', 0),
(4, 132, '2015-03-23', '2015-03-23', '00:00:00', '23:59:00', 0),
(5, 176, '2015-06-06', '2015-06-06', '20:00:00', '22:00:00', 0),
(6, 177, '2015-05-30', '2015-05-30', '10:00:00', '17:00:00', 0);

-- --------------------------------------------------------

--
-- Structure de la table `crbass_eo_venuemeta`
--

CREATE TABLE IF NOT EXISTS `crbass_eo_venuemeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `eo_venue_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`meta_id`),
  KEY `eo_venue_id` (`eo_venue_id`),
  KEY `meta_key` (`meta_key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Contenu de la table `crbass_eo_venuemeta`
--

INSERT INTO `crbass_eo_venuemeta` (`meta_id`, `eo_venue_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_address', 'place du Comte-Haymon'),
(2, 2, '_city', 'Corbeil-Essonnes'),
(3, 2, '_postcode', '91100'),
(4, 2, '_lat', '48.612444'),
(5, 2, '_lng', '2.481946'),
(6, 2, '_state', ''),
(7, 2, '_country', ''),
(8, 2, '_description', ''),
(9, 9, '_address', 'Stade Nautique'),
(10, 9, '_city', 'Corbeil-Essonnes'),
(11, 9, '_country', 'France'),
(12, 9, '_lat', '48.610260'),
(13, 9, '_lng', '2.474805'),
(14, 16, '_address', '15 Place Léon Cassé'),
(15, 16, '_city', 'Corbeil-Essonnes'),
(16, 16, '_postcode', '91100'),
(17, 16, '_country', 'France'),
(18, 16, '_lat', '48.603951'),
(19, 16, '_lng', '2.468978'),
(20, 18, '_address', '12-32 Rue Fernand Laguide'),
(21, 18, '_city', 'Corbeil-Essonnes'),
(22, 18, '_postcode', '91100'),
(23, 18, '_lat', '48.599125'),
(24, 18, '_lng', '2.457667');

-- --------------------------------------------------------

--
-- Structure de la table `crbass_links`
--

CREATE TABLE IF NOT EXISTS `crbass_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `crbass_options`
--

CREATE TABLE IF NOT EXISTS `crbass_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=3428 ;

--
-- Contenu de la table `crbass_options`
--

INSERT INTO `crbass_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://127.0.0.1/corbeilassoc', 'yes'),
(2, 'home', 'http://127.0.0.1/corbeilassoc', 'yes'),
(3, 'blogname', 'Associations de Corbeil', 'yes'),
(4, 'blogdescription', 'Le site internet des associations de la ville', 'yes'),
(5, 'users_can_register', '1', 'yes'),
(6, 'admin_email', 'contact@tiar-florian.fr', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '11', 'yes'),
(19, 'default_comment_status', 'closed', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j F Y', 'yes'),
(24, 'time_format', 'G\\hi', 'yes'),
(25, 'links_updated_date_format', 'j F Y G \\h i \\m\\i\\n', 'yes'),
(26, 'comment_moderation', '', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'gzipcompression', '0', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:9:{i:0;s:41:"CorbeilinfosPlugin/CorbeilinfosPlugin.php";i:1;s:34:"advanced-custom-fields-pro/acf.php";i:2;s:37:"association-type/association-type.php";i:3;s:43:"event-organiser-fes/event-organiser-fes.php";i:4;s:35:"event-organiser/event-organiser.php";i:5;s:19:"if-menu/if-menu.php";i:6;s:27:"user-system/user-system.php";i:7;s:41:"wordpress-importer/wordpress-importer.php";i:8;s:28:"wysija-newsletters/index.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'advanced_edit', '0', 'yes'),
(37, 'comment_max_links', '2', 'yes'),
(38, 'gmt_offset', '2', 'yes'),
(39, 'default_email_category', '1', 'yes'),
(40, 'recently_edited', 'a:5:{i:0;s:75:"C:\\xampp\\htdocs\\corbeilassoc/wp-content/plugins/user-system/user-system.php";i:2;s:74:"C:\\xampp\\htdocs\\corbeilassoc/wp-content/themes/sequential/associations.php";i:3;s:67:"C:\\xampp\\htdocs\\corbeilassoc/wp-content/themes/sequential/style.css";i:4;s:83:"C:\\xampp\\htdocs\\corbeilassoc/wp-content/plugins/event-organiser/event-organiser.php";i:5;s:89:"C:\\xampp\\htdocs\\corbeilassoc/wp-content/plugins/CorbeilinfosPlugin/CorbeilinfosPlugin.php";}', 'no'),
(41, 'template', 'sequential', 'yes'),
(42, 'stylesheet', 'sequential-child', 'yes'),
(43, 'comment_whitelist', '1', 'yes'),
(44, 'blacklist_keys', '', 'no'),
(45, 'comment_registration', '', 'yes'),
(46, 'html_type', 'text/html', 'yes'),
(47, 'use_trackback', '0', 'yes'),
(48, 'default_role', 'subscriber', 'yes'),
(49, 'db_version', '31533', 'yes'),
(50, 'uploads_use_yearmonth_folders', '1', 'yes'),
(51, 'upload_path', '', 'yes'),
(52, 'blog_public', '0', 'yes'),
(53, 'default_link_category', '0', 'yes'),
(54, 'show_on_front', 'page', 'yes'),
(55, 'tag_base', '', 'yes'),
(56, 'show_avatars', '1', 'yes'),
(57, 'avatar_rating', 'G', 'yes'),
(58, 'upload_url_path', '', 'yes'),
(59, 'thumbnail_size_w', '150', 'yes'),
(60, 'thumbnail_size_h', '150', 'yes'),
(61, 'thumbnail_crop', '1', 'yes'),
(62, 'medium_size_w', '300', 'yes'),
(63, 'medium_size_h', '300', 'yes'),
(64, 'avatar_default', 'mystery', 'yes'),
(65, 'large_size_w', '1024', 'yes'),
(66, 'large_size_h', '1024', 'yes'),
(67, 'image_default_link_type', 'file', 'yes'),
(68, 'image_default_size', '', 'yes'),
(69, 'image_default_align', '', 'yes'),
(70, 'close_comments_for_old_posts', '', 'yes'),
(71, 'close_comments_days_old', '14', 'yes'),
(72, 'thread_comments', '1', 'yes'),
(73, 'thread_comments_depth', '5', 'yes'),
(74, 'page_comments', '', 'yes'),
(75, 'comments_per_page', '50', 'yes'),
(76, 'default_comments_page', 'newest', 'yes'),
(77, 'comment_order', 'asc', 'yes'),
(78, 'sticky_posts', 'a:0:{}', 'yes'),
(79, 'widget_categories', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(80, 'widget_text', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(81, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(82, 'uninstall_plugins', 'a:1:{s:35:"event-organiser/event-organiser.php";s:24:"eventorganiser_uninstall";}', 'no'),
(83, 'timezone_string', '', 'yes'),
(84, 'page_for_posts', '10', 'yes'),
(85, 'page_on_front', '8', 'yes'),
(86, 'default_post_format', '0', 'yes'),
(87, 'link_manager_enabled', '0', 'yes'),
(88, 'initial_db_version', '30133', 'yes'),
(89, 'crbass_user_roles', 'a:7:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:84:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:9:"add_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;s:11:"edit_events";b:1;s:14:"publish_events";b:1;s:13:"delete_events";b:1;s:18:"edit_others_events";b:1;s:20:"delete_others_events";b:1;s:19:"read_private_events";b:1;s:13:"manage_venues";b:1;s:23:"manage_event_categories";b:1;s:9:"edit_post";b:1;s:9:"read_post";b:1;s:11:"delete_post";b:1;s:18:"wysija_newsletters";b:1;s:18:"wysija_subscribers";b:1;s:13:"wysija_config";b:1;s:16:"wysija_theme_tab";b:1;s:16:"wysija_style_tab";b:1;s:22:"wysija_stats_dashboard";b:1;s:20:"publish_associations";b:1;s:17:"edit_associations";b:1;s:19:"delete_associations";b:1;s:24:"edit_others_associations";b:1;s:26:"delete_others_associations";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:48:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:9:"edit_post";b:1;s:9:"read_post";b:1;s:11:"delete_post";b:1;s:11:"edit_events";b:1;s:14:"publish_events";b:1;s:13:"delete_events";b:1;s:18:"edit_others_events";b:1;s:13:"manage_venues";b:1;s:23:"manage_event_categories";b:1;s:20:"publish_associations";b:1;s:17:"edit_associations";b:1;s:19:"delete_associations";b:1;s:24:"edit_others_associations";b:1;s:26:"delete_others_associations";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:8:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:11:"edit_events";b:1;s:14:"publish_events";b:1;s:13:"delete_events";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}s:4:"cool";a:2:{s:4:"name";s:12:"Cools Editor";s:12:"capabilities";a:14:{s:9:"edit_post";b:1;s:9:"read_post";b:1;s:11:"delete_post";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:13:"publish_posts";b:1;s:18:"read_private_posts";b:1;s:4:"read";b:1;s:12:"delete_posts";b:1;s:20:"delete_private_posts";b:1;s:22:"delete_published_posts";b:1;s:19:"delete_others_posts";b:1;s:18:"edit_private_posts";b:1;s:20:"edit_published_posts";b:1;}}s:17:"association_owner";a:2:{s:4:"name";s:22:"Responsable associatif";s:12:"capabilities";a:16:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"publish_associations";b:1;s:17:"edit_associations";b:1;s:19:"delete_associations";b:1;s:11:"edit_events";b:1;s:14:"publish_events";b:1;s:13:"delete_events";b:1;}}}', 'yes'),
(90, 'WPLANG', 'fr_FR', 'yes'),
(91, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(92, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(93, 'widget_recent-comments', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(94, 'widget_archives', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(95, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(96, 'sidebars_widgets', 'a:3:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:8:"wysija-2";i:2;s:21:"widget-corbeilinfos-2";i:3;s:22:"eo_event_list_widget-2";i:4;s:14:"recent-posts-2";i:5;s:6:"meta-2";}s:13:"array_version";i:3;}', 'yes'),
(97, 'cron', 'a:8:{i:1433916932;a:1:{s:26:"um_hourly_scheduled_events";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:6:"hourly";s:4:"args";a:0:{}s:8:"interval";i:3600;}}}i:1433918215;a:3:{s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1433920200;a:1:{s:20:"wp_maybe_auto_update";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1433945731;a:1:{s:25:"um_daily_scheduled_events";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1433945732;a:1:{s:30:"um_twicedaily_scheduled_events";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1433961437;a:1:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1433970052;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}s:7:"version";i:2;}', 'yes'),
(100, '_transient_random_seed', 'a6bdc97e89cc69bf83d800e0ad18cc07', 'yes'),
(127, 'current_theme', 'Sequential Child', 'yes'),
(128, 'theme_mods_sequential', 'a:3:{i:0;b:0;s:18:"nav_menu_locations";a:2:{s:7:"primary";i:3;s:6:"footer";i:3;}s:16:"sidebars_widgets";a:2:{s:4:"time";i:1432968253;s:4:"data";a:3:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:8:"wysija-2";i:2;s:21:"widget-corbeilinfos-2";i:3;s:22:"eo_event_list_widget-2";i:4;s:14:"recent-posts-2";i:5;s:6:"meta-2";}s:9:"sidebar-2";a:0:{}}}}', 'yes'),
(129, 'theme_switched', '', 'yes'),
(131, 'recently_activated', 'a:0:{}', 'yes'),
(134, 'eventorganiser_options', 'a:22:{s:8:"supports";a:6:{i:0;s:6:"author";i:1;s:9:"thumbnail";i:2;s:7:"excerpt";i:3;s:8:"eventtag";i:4;s:5:"title";i:5;s:6:"editor";}s:14:"event_redirect";s:6:"events";s:10:"dateformat";s:5:"d-m-Y";s:9:"prettyurl";i:1;s:9:"templates";i:1;s:9:"addtomenu";s:9:"principal";s:17:"excludefromsearch";i:0;s:8:"showpast";i:1;s:12:"group_events";s:0:"";s:9:"url_venue";s:15:"evenements/lieu";s:7:"url_cat";s:20:"evenements/categorie";s:7:"url_tag";s:19:"evenements/mot-clef";s:8:"navtitle";s:12:"Événements";s:8:"eventtag";i:1;s:4:"feed";i:1;s:16:"runningisnotpast";b:0;s:13:"deleteexpired";i:0;s:11:"disable_css";i:0;s:15:"menu_item_db_id";i:7;s:9:"url_event";s:9:"evenement";s:10:"url_events";s:10:"evenements";s:6:"url_on";s:2:"on";}', 'yes'),
(135, 'eventorganiser_admin_notices', 'a:2:{i:0;s:15:"autofillvenue17";i:1;s:17:"changedtemplate17";}', 'yes'),
(137, 'eventorganiser_version', '2.13.0', 'yes'),
(163, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:"auto_add";a:0:{}}', 'yes'),
(168, 'widget_eo_events_agenda_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(169, 'widget_eo_calendar_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(170, 'widget_eo_event_list_widget', 'a:2:{i:2;a:10:{s:5:"title";s:6:"Agenda";s:11:"numberposts";i:5;s:14:"event-category";s:0:"";s:5:"venue";s:0:"";s:5:"order";s:3:"asc";s:7:"orderby";s:10:"eventstart";s:14:"showpastevents";i:0;s:15:"group_events_by";s:0:"";s:8:"template";s:0:"";s:9:"no_events";s:9:"No Events";}s:12:"_multiwidget";i:1;}', 'yes'),
(269, 'acf_version', '5.0.8', 'yes'),
(270, 'simple-custom-types', 'a:1:{s:11:"customtypes";a:1:{s:4:"cool";a:25:{s:4:"name";s:4:"cool";s:6:"labels";a:11:{s:4:"name";s:5:"Cools";s:13:"singular_name";s:4:"Cool";s:7:"add_new";s:7:"Ajouter";s:12:"add_new_item";s:23:"Ajouter un nouveau cool";s:9:"edit_item";s:16:"Modifier le cool";s:8:"new_item";s:12:"Nouveau cool";s:9:"view_item";s:12:"Voir le cool";s:12:"search_items";s:16:"Chercher un cool";s:9:"not_found";s:18:"Aucun cool trouvé";s:18:"not_found_in_trash";s:36:"Aucun cool trouvé dans la corbeille";s:17:"parent_item_colon";s:13:"Cool parent :";}s:11:"description";s:0:"";s:18:"publicly_queryable";s:1:"1";s:19:"exclude_from_search";s:1:"0";s:15:"capability_type";s:4:"post";s:22:"capability_type_custom";s:10:"post,posts";s:12:"capabilities";a:14:{s:9:"edit_post";s:0:"";s:9:"read_post";s:0:"";s:10:"edit_posts";s:0:"";s:11:"delete_post";s:0:"";s:17:"edit_others_posts";s:0:"";s:13:"publish_posts";s:0:"";s:18:"read_private_posts";s:0:"";s:4:"read";s:0:"";s:12:"delete_posts";s:0:"";s:20:"delete_private_posts";s:0:"";s:22:"delete_published_posts";s:0:"";s:19:"delete_others_posts";s:0:"";s:18:"edit_private_posts";s:0:"";s:20:"edit_published_posts";s:0:"";}s:12:"map_meta_cap";s:1:"1";s:12:"hierarchical";s:1:"0";s:6:"public";s:0:"";s:7:"rewrite";s:1:"1";s:11:"has_archive";s:1:"0";s:9:"query_var";s:3:"sss";s:12:"archive_slug";s:0:"";s:8:"supports";a:3:{i:0;s:5:"title";i:1;s:6:"editor";i:2;s:9:"thumbnail";}s:10:"taxonomies";a:1:{i:0;s:8:"category";}s:7:"show_ui";s:1:"1";s:13:"menu_position";s:2:"30";s:9:"menu_icon";s:0:"";s:20:"custom_role_checkbox";s:0:"";s:11:"custom_role";s:0:"";s:10:"can_export";s:1:"1";s:17:"show_in_nav_menus";s:1:"1";s:12:"show_in_menu";s:1:"1";}}}', 'yes'),
(436, 'redux-framework-tracking', 'a:3:{s:8:"dev_mode";b:0;s:4:"hash";s:32:"0a8ad9143770e281d8358d8a3e4acca4";s:14:"allow_tracking";s:2:"no";}', 'yes'),
(437, 'um_version', '1.0.85', 'yes');
INSERT INTO `crbass_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(438, 'um_cache_fonticons', 'a:1212:{i:0;s:13:"um-icon-alert";i:1;s:21:"um-icon-alert-circled";i:2;s:19:"um-icon-android-add";i:3;s:26:"um-icon-android-add-circle";i:4;s:27:"um-icon-android-alarm-clock";i:5;s:21:"um-icon-android-alert";i:6;s:20:"um-icon-android-apps";i:7;s:23:"um-icon-android-archive";i:8;s:26:"um-icon-android-arrow-back";i:9;s:26:"um-icon-android-arrow-down";i:10;s:30:"um-icon-android-arrow-dropdown";i:11;s:37:"um-icon-android-arrow-dropdown-circle";i:12;s:30:"um-icon-android-arrow-dropleft";i:13;s:37:"um-icon-android-arrow-dropleft-circle";i:14;s:31:"um-icon-android-arrow-dropright";i:15;s:38:"um-icon-android-arrow-dropright-circle";i:16;s:28:"um-icon-android-arrow-dropup";i:17;s:35:"um-icon-android-arrow-dropup-circle";i:18;s:29:"um-icon-android-arrow-forward";i:19;s:24:"um-icon-android-arrow-up";i:20;s:22:"um-icon-android-attach";i:21;s:19:"um-icon-android-bar";i:22;s:23:"um-icon-android-bicycle";i:23;s:20:"um-icon-android-boat";i:24;s:24:"um-icon-android-bookmark";i:25;s:20:"um-icon-android-bulb";i:26;s:19:"um-icon-android-bus";i:27;s:24:"um-icon-android-calendar";i:28;s:20:"um-icon-android-call";i:29;s:22:"um-icon-android-camera";i:30;s:22:"um-icon-android-cancel";i:31;s:19:"um-icon-android-car";i:32;s:20:"um-icon-android-cart";i:33;s:20:"um-icon-android-chat";i:34;s:24:"um-icon-android-checkbox";i:35;s:30:"um-icon-android-checkbox-blank";i:36;s:32:"um-icon-android-checkbox-outline";i:37;s:38:"um-icon-android-checkbox-outline-blank";i:38;s:32:"um-icon-android-checkmark-circle";i:39;s:25:"um-icon-android-clipboard";i:40;s:21:"um-icon-android-close";i:41;s:21:"um-icon-android-cloud";i:42;s:28:"um-icon-android-cloud-circle";i:43;s:26:"um-icon-android-cloud-done";i:44;s:29:"um-icon-android-cloud-outline";i:45;s:29:"um-icon-android-color-palette";i:46;s:23:"um-icon-android-compass";i:47;s:23:"um-icon-android-contact";i:48;s:24:"um-icon-android-contacts";i:49;s:24:"um-icon-android-contract";i:50;s:22:"um-icon-android-create";i:51;s:22:"um-icon-android-delete";i:52;s:23:"um-icon-android-desktop";i:53;s:24:"um-icon-android-document";i:54;s:20:"um-icon-android-done";i:55;s:24:"um-icon-android-done-all";i:56;s:24:"um-icon-android-download";i:57;s:22:"um-icon-android-drafts";i:58;s:20:"um-icon-android-exit";i:59;s:22:"um-icon-android-expand";i:60;s:24:"um-icon-android-favorite";i:61;s:32:"um-icon-android-favorite-outline";i:62;s:20:"um-icon-android-film";i:63;s:22:"um-icon-android-folder";i:64;s:27:"um-icon-android-folder-open";i:65;s:22:"um-icon-android-funnel";i:66;s:21:"um-icon-android-globe";i:67;s:20:"um-icon-android-hand";i:68;s:23:"um-icon-android-hangout";i:69;s:21:"um-icon-android-happy";i:70;s:20:"um-icon-android-home";i:71;s:21:"um-icon-android-image";i:72;s:22:"um-icon-android-laptop";i:73;s:20:"um-icon-android-list";i:74;s:22:"um-icon-android-locate";i:75;s:20:"um-icon-android-lock";i:76;s:20:"um-icon-android-mail";i:77;s:19:"um-icon-android-map";i:78;s:20:"um-icon-android-menu";i:79;s:26:"um-icon-android-microphone";i:80;s:30:"um-icon-android-microphone-off";i:81;s:31:"um-icon-android-more-horizontal";i:82;s:29:"um-icon-android-more-vertical";i:83;s:24:"um-icon-android-navigate";i:84;s:29:"um-icon-android-notifications";i:85;s:34:"um-icon-android-notifications-none";i:86;s:33:"um-icon-android-notifications-off";i:87;s:20:"um-icon-android-open";i:88;s:23:"um-icon-android-options";i:89;s:22:"um-icon-android-people";i:90;s:22:"um-icon-android-person";i:91;s:26:"um-icon-android-person-add";i:92;s:31:"um-icon-android-phone-landscape";i:93;s:30:"um-icon-android-phone-portrait";i:94;s:19:"um-icon-android-pin";i:95;s:21:"um-icon-android-plane";i:96;s:25:"um-icon-android-playstore";i:97;s:21:"um-icon-android-print";i:98;s:32:"um-icon-android-radio-button-off";i:99;s:31:"um-icon-android-radio-button-on";i:100;s:23:"um-icon-android-refresh";i:101;s:22:"um-icon-android-remove";i:102;s:29:"um-icon-android-remove-circle";i:103;s:26:"um-icon-android-restaurant";i:104;s:19:"um-icon-android-sad";i:105;s:22:"um-icon-android-search";i:106;s:20:"um-icon-android-send";i:107;s:24:"um-icon-android-settings";i:108;s:21:"um-icon-android-share";i:109;s:25:"um-icon-android-share-alt";i:110;s:20:"um-icon-android-star";i:111;s:25:"um-icon-android-star-half";i:112;s:28:"um-icon-android-star-outline";i:113;s:25:"um-icon-android-stopwatch";i:114;s:22:"um-icon-android-subway";i:115;s:21:"um-icon-android-sunny";i:116;s:20:"um-icon-android-sync";i:117;s:23:"um-icon-android-textsms";i:118;s:20:"um-icon-android-time";i:119;s:21:"um-icon-android-train";i:120;s:22:"um-icon-android-unlock";i:121;s:22:"um-icon-android-upload";i:122;s:27:"um-icon-android-volume-down";i:123;s:27:"um-icon-android-volume-mute";i:124;s:26:"um-icon-android-volume-off";i:125;s:25:"um-icon-android-volume-up";i:126;s:20:"um-icon-android-walk";i:127;s:23:"um-icon-android-warning";i:128;s:21:"um-icon-android-watch";i:129;s:20:"um-icon-android-wifi";i:130;s:16:"um-icon-aperture";i:131;s:15:"um-icon-archive";i:132;s:20:"um-icon-arrow-down-a";i:133;s:20:"um-icon-arrow-down-b";i:134;s:20:"um-icon-arrow-down-c";i:135;s:20:"um-icon-arrow-expand";i:136;s:29:"um-icon-arrow-graph-down-left";i:137;s:30:"um-icon-arrow-graph-down-right";i:138;s:27:"um-icon-arrow-graph-up-left";i:139;s:28:"um-icon-arrow-graph-up-right";i:140;s:20:"um-icon-arrow-left-a";i:141;s:20:"um-icon-arrow-left-b";i:142;s:20:"um-icon-arrow-left-c";i:143;s:18:"um-icon-arrow-move";i:144;s:20:"um-icon-arrow-resize";i:145;s:25:"um-icon-arrow-return-left";i:146;s:26:"um-icon-arrow-return-right";i:147;s:21:"um-icon-arrow-right-a";i:148;s:21:"um-icon-arrow-right-b";i:149;s:21:"um-icon-arrow-right-c";i:150;s:20:"um-icon-arrow-shrink";i:151;s:18:"um-icon-arrow-swap";i:152;s:18:"um-icon-arrow-up-a";i:153;s:18:"um-icon-arrow-up-b";i:154;s:18:"um-icon-arrow-up-c";i:155;s:16:"um-icon-asterisk";i:156;s:10:"um-icon-at";i:157;s:17:"um-icon-backspace";i:158;s:25:"um-icon-backspace-outline";i:159;s:11:"um-icon-bag";i:160;s:24:"um-icon-battery-charging";i:161;s:21:"um-icon-battery-empty";i:162;s:20:"um-icon-battery-full";i:163;s:20:"um-icon-battery-half";i:164;s:19:"um-icon-battery-low";i:165;s:14:"um-icon-beaker";i:166;s:12:"um-icon-beer";i:167;s:17:"um-icon-bluetooth";i:168;s:15:"um-icon-bonfire";i:169;s:16:"um-icon-bookmark";i:170;s:14:"um-icon-bowtie";i:171;s:17:"um-icon-briefcase";i:172;s:11:"um-icon-bug";i:173;s:18:"um-icon-calculator";i:174;s:16:"um-icon-calendar";i:175;s:14:"um-icon-camera";i:176;s:12:"um-icon-card";i:177;s:12:"um-icon-cash";i:178;s:15:"um-icon-chatbox";i:179;s:23:"um-icon-chatbox-working";i:180;s:17:"um-icon-chatboxes";i:181;s:18:"um-icon-chatbubble";i:182;s:26:"um-icon-chatbubble-working";i:183;s:19:"um-icon-chatbubbles";i:184;s:17:"um-icon-checkmark";i:185;s:25:"um-icon-checkmark-circled";i:186;s:23:"um-icon-checkmark-round";i:187;s:20:"um-icon-chevron-down";i:188;s:20:"um-icon-chevron-left";i:189;s:21:"um-icon-chevron-right";i:190;s:18:"um-icon-chevron-up";i:191;s:17:"um-icon-clipboard";i:192;s:13:"um-icon-clock";i:193;s:13:"um-icon-close";i:194;s:21:"um-icon-close-circled";i:195;s:19:"um-icon-close-round";i:196;s:25:"um-icon-closed-captioning";i:197;s:13:"um-icon-cloud";i:198;s:12:"um-icon-code";i:199;s:21:"um-icon-code-download";i:200;s:20:"um-icon-code-working";i:201;s:14:"um-icon-coffee";i:202;s:15:"um-icon-compass";i:203;s:15:"um-icon-compose";i:204;s:23:"um-icon-connection-bars";i:205;s:16:"um-icon-contrast";i:206;s:12:"um-icon-crop";i:207;s:12:"um-icon-cube";i:208;s:12:"um-icon-disc";i:209;s:16:"um-icon-document";i:210;s:21:"um-icon-document-text";i:211;s:12:"um-icon-drag";i:212;s:13:"um-icon-earth";i:213;s:13:"um-icon-easel";i:214;s:12:"um-icon-edit";i:215;s:11:"um-icon-egg";i:216;s:13:"um-icon-eject";i:217;s:13:"um-icon-email";i:218;s:20:"um-icon-email-unread";i:219;s:24:"um-icon-erlenmeyer-flask";i:220;s:32:"um-icon-erlenmeyer-flask-bubbles";i:221;s:11:"um-icon-eye";i:222;s:20:"um-icon-eye-disabled";i:223;s:14:"um-icon-female";i:224;s:14:"um-icon-filing";i:225;s:19:"um-icon-film-marker";i:226;s:16:"um-icon-fireball";i:227;s:12:"um-icon-flag";i:228;s:13:"um-icon-flame";i:229;s:13:"um-icon-flash";i:230;s:17:"um-icon-flash-off";i:231;s:14:"um-icon-folder";i:232;s:12:"um-icon-fork";i:233;s:17:"um-icon-fork-repo";i:234;s:15:"um-icon-forward";i:235;s:14:"um-icon-funnel";i:236;s:14:"um-icon-gear-a";i:237;s:14:"um-icon-gear-b";i:238;s:12:"um-icon-grid";i:239;s:14:"um-icon-hammer";i:240;s:13:"um-icon-happy";i:241;s:21:"um-icon-happy-outline";i:242;s:17:"um-icon-headphone";i:243;s:13:"um-icon-heart";i:244;s:20:"um-icon-heart-broken";i:245;s:12:"um-icon-help";i:246;s:17:"um-icon-help-buoy";i:247;s:20:"um-icon-help-circled";i:248;s:12:"um-icon-home";i:249;s:16:"um-icon-icecream";i:250;s:13:"um-icon-image";i:251;s:14:"um-icon-images";i:252;s:19:"um-icon-information";i:253;s:27:"um-icon-information-circled";i:254;s:13:"um-icon-ionic";i:255;s:17:"um-icon-ios-alarm";i:256;s:25:"um-icon-ios-alarm-outline";i:257;s:18:"um-icon-ios-albums";i:258;s:26:"um-icon-ios-albums-outline";i:259;s:28:"um-icon-ios-americanfootball";i:260;s:36:"um-icon-ios-americanfootball-outline";i:261;s:21:"um-icon-ios-analytics";i:262;s:29:"um-icon-ios-analytics-outline";i:263;s:22:"um-icon-ios-arrow-back";i:264;s:22:"um-icon-ios-arrow-down";i:265;s:25:"um-icon-ios-arrow-forward";i:266;s:22:"um-icon-ios-arrow-left";i:267;s:23:"um-icon-ios-arrow-right";i:268;s:27:"um-icon-ios-arrow-thin-down";i:269;s:27:"um-icon-ios-arrow-thin-left";i:270;s:28:"um-icon-ios-arrow-thin-right";i:271;s:25:"um-icon-ios-arrow-thin-up";i:272;s:20:"um-icon-ios-arrow-up";i:273;s:14:"um-icon-ios-at";i:274;s:22:"um-icon-ios-at-outline";i:275;s:19:"um-icon-ios-barcode";i:276;s:27:"um-icon-ios-barcode-outline";i:277;s:20:"um-icon-ios-baseball";i:278;s:28:"um-icon-ios-baseball-outline";i:279;s:22:"um-icon-ios-basketball";i:280;s:30:"um-icon-ios-basketball-outline";i:281;s:16:"um-icon-ios-bell";i:282;s:24:"um-icon-ios-bell-outline";i:283;s:16:"um-icon-ios-body";i:284;s:24:"um-icon-ios-body-outline";i:285;s:16:"um-icon-ios-bolt";i:286;s:24:"um-icon-ios-bolt-outline";i:287;s:16:"um-icon-ios-book";i:288;s:24:"um-icon-ios-book-outline";i:289;s:21:"um-icon-ios-bookmarks";i:290;s:29:"um-icon-ios-bookmarks-outline";i:291;s:15:"um-icon-ios-box";i:292;s:23:"um-icon-ios-box-outline";i:293;s:21:"um-icon-ios-briefcase";i:294;s:29:"um-icon-ios-briefcase-outline";i:295;s:20:"um-icon-ios-browsers";i:296;s:28:"um-icon-ios-browsers-outline";i:297;s:22:"um-icon-ios-calculator";i:298;s:30:"um-icon-ios-calculator-outline";i:299;s:20:"um-icon-ios-calendar";i:300;s:28:"um-icon-ios-calendar-outline";i:301;s:18:"um-icon-ios-camera";i:302;s:26:"um-icon-ios-camera-outline";i:303;s:16:"um-icon-ios-cart";i:304;s:24:"um-icon-ios-cart-outline";i:305;s:21:"um-icon-ios-chatboxes";i:306;s:29:"um-icon-ios-chatboxes-outline";i:307;s:22:"um-icon-ios-chatbubble";i:308;s:30:"um-icon-ios-chatbubble-outline";i:309;s:21:"um-icon-ios-checkmark";i:310;s:27:"um-icon-ios-checkmark-empty";i:311;s:29:"um-icon-ios-checkmark-outline";i:312;s:25:"um-icon-ios-circle-filled";i:313;s:26:"um-icon-ios-circle-outline";i:314;s:17:"um-icon-ios-clock";i:315;s:25:"um-icon-ios-clock-outline";i:316;s:17:"um-icon-ios-close";i:317;s:23:"um-icon-ios-close-empty";i:318;s:25:"um-icon-ios-close-outline";i:319;s:17:"um-icon-ios-cloud";i:320;s:26:"um-icon-ios-cloud-download";i:321;s:34:"um-icon-ios-cloud-download-outline";i:322;s:25:"um-icon-ios-cloud-outline";i:323;s:24:"um-icon-ios-cloud-upload";i:324;s:32:"um-icon-ios-cloud-upload-outline";i:325;s:18:"um-icon-ios-cloudy";i:326;s:24:"um-icon-ios-cloudy-night";i:327;s:32:"um-icon-ios-cloudy-night-outline";i:328;s:26:"um-icon-ios-cloudy-outline";i:329;s:15:"um-icon-ios-cog";i:330;s:23:"um-icon-ios-cog-outline";i:331;s:24:"um-icon-ios-color-filter";i:332;s:32:"um-icon-ios-color-filter-outline";i:333;s:22:"um-icon-ios-color-wand";i:334;s:30:"um-icon-ios-color-wand-outline";i:335;s:19:"um-icon-ios-compose";i:336;s:27:"um-icon-ios-compose-outline";i:337;s:19:"um-icon-ios-contact";i:338;s:27:"um-icon-ios-contact-outline";i:339;s:16:"um-icon-ios-copy";i:340;s:24:"um-icon-ios-copy-outline";i:341;s:16:"um-icon-ios-crop";i:342;s:23:"um-icon-ios-crop-strong";i:343;s:20:"um-icon-ios-download";i:344;s:28:"um-icon-ios-download-outline";i:345;s:16:"um-icon-ios-drag";i:346;s:17:"um-icon-ios-email";i:347;s:25:"um-icon-ios-email-outline";i:348;s:15:"um-icon-ios-eye";i:349;s:23:"um-icon-ios-eye-outline";i:350;s:23:"um-icon-ios-fastforward";i:351;s:31:"um-icon-ios-fastforward-outline";i:352;s:18:"um-icon-ios-filing";i:353;s:26:"um-icon-ios-filing-outline";i:354;s:16:"um-icon-ios-film";i:355;s:24:"um-icon-ios-film-outline";i:356;s:16:"um-icon-ios-flag";i:357;s:24:"um-icon-ios-flag-outline";i:358;s:17:"um-icon-ios-flame";i:359;s:25:"um-icon-ios-flame-outline";i:360;s:17:"um-icon-ios-flask";i:361;s:25:"um-icon-ios-flask-outline";i:362;s:18:"um-icon-ios-flower";i:363;s:26:"um-icon-ios-flower-outline";i:364;s:18:"um-icon-ios-folder";i:365;s:26:"um-icon-ios-folder-outline";i:366;s:20:"um-icon-ios-football";i:367;s:28:"um-icon-ios-football-outline";i:368;s:29:"um-icon-ios-game-controller-a";i:369;s:37:"um-icon-ios-game-controller-a-outline";i:370;s:29:"um-icon-ios-game-controller-b";i:371;s:37:"um-icon-ios-game-controller-b-outline";i:372;s:16:"um-icon-ios-gear";i:373;s:24:"um-icon-ios-gear-outline";i:374;s:19:"um-icon-ios-glasses";i:375;s:27:"um-icon-ios-glasses-outline";i:376;s:21:"um-icon-ios-grid-view";i:377;s:29:"um-icon-ios-grid-view-outline";i:378;s:17:"um-icon-ios-heart";i:379;s:25:"um-icon-ios-heart-outline";i:380;s:16:"um-icon-ios-help";i:381;s:22:"um-icon-ios-help-empty";i:382;s:24:"um-icon-ios-help-outline";i:383;s:16:"um-icon-ios-home";i:384;s:24:"um-icon-ios-home-outline";i:385;s:20:"um-icon-ios-infinite";i:386;s:28:"um-icon-ios-infinite-outline";i:387;s:23:"um-icon-ios-information";i:388;s:29:"um-icon-ios-information-empty";i:389;s:31:"um-icon-ios-information-outline";i:390;s:25:"um-icon-ios-ionic-outline";i:391;s:18:"um-icon-ios-keypad";i:392;s:26:"um-icon-ios-keypad-outline";i:393;s:21:"um-icon-ios-lightbulb";i:394;s:29:"um-icon-ios-lightbulb-outline";i:395;s:16:"um-icon-ios-list";i:396;s:24:"um-icon-ios-list-outline";i:397;s:20:"um-icon-ios-location";i:398;s:28:"um-icon-ios-location-outline";i:399;s:18:"um-icon-ios-locked";i:400;s:26:"um-icon-ios-locked-outline";i:401;s:16:"um-icon-ios-loop";i:402;s:23:"um-icon-ios-loop-strong";i:403;s:19:"um-icon-ios-medical";i:404;s:27:"um-icon-ios-medical-outline";i:405;s:18:"um-icon-ios-medkit";i:406;s:26:"um-icon-ios-medkit-outline";i:407;s:15:"um-icon-ios-mic";i:408;s:19:"um-icon-ios-mic-off";i:409;s:23:"um-icon-ios-mic-outline";i:410;s:17:"um-icon-ios-minus";i:411;s:23:"um-icon-ios-minus-empty";i:412;s:25:"um-icon-ios-minus-outline";i:413;s:19:"um-icon-ios-monitor";i:414;s:27:"um-icon-ios-monitor-outline";i:415;s:16:"um-icon-ios-moon";i:416;s:24:"um-icon-ios-moon-outline";i:417;s:16:"um-icon-ios-more";i:418;s:24:"um-icon-ios-more-outline";i:419;s:24:"um-icon-ios-musical-note";i:420;s:25:"um-icon-ios-musical-notes";i:421;s:20:"um-icon-ios-navigate";i:422;s:28:"um-icon-ios-navigate-outline";i:423;s:21:"um-icon-ios-nutrition";i:424;s:29:"um-icon-ios-nutrition-outline";i:425;s:17:"um-icon-ios-paper";i:426;s:25:"um-icon-ios-paper-outline";i:427;s:22:"um-icon-ios-paperplane";i:428;s:30:"um-icon-ios-paperplane-outline";i:429;s:23:"um-icon-ios-partlysunny";i:430;s:31:"um-icon-ios-partlysunny-outline";i:431;s:17:"um-icon-ios-pause";i:432;s:25:"um-icon-ios-pause-outline";i:433;s:15:"um-icon-ios-paw";i:434;s:23:"um-icon-ios-paw-outline";i:435;s:18:"um-icon-ios-people";i:436;s:26:"um-icon-ios-people-outline";i:437;s:18:"um-icon-ios-person";i:438;s:26:"um-icon-ios-person-outline";i:439;s:21:"um-icon-ios-personadd";i:440;s:29:"um-icon-ios-personadd-outline";i:441;s:18:"um-icon-ios-photos";i:442;s:26:"um-icon-ios-photos-outline";i:443;s:15:"um-icon-ios-pie";i:444;s:23:"um-icon-ios-pie-outline";i:445;s:16:"um-icon-ios-pint";i:446;s:24:"um-icon-ios-pint-outline";i:447;s:16:"um-icon-ios-play";i:448;s:24:"um-icon-ios-play-outline";i:449;s:16:"um-icon-ios-plus";i:450;s:22:"um-icon-ios-plus-empty";i:451;s:24:"um-icon-ios-plus-outline";i:452;s:20:"um-icon-ios-pricetag";i:453;s:28:"um-icon-ios-pricetag-outline";i:454;s:21:"um-icon-ios-pricetags";i:455;s:29:"um-icon-ios-pricetags-outline";i:456;s:19:"um-icon-ios-printer";i:457;s:27:"um-icon-ios-printer-outline";i:458;s:17:"um-icon-ios-pulse";i:459;s:24:"um-icon-ios-pulse-strong";i:460;s:17:"um-icon-ios-rainy";i:461;s:25:"um-icon-ios-rainy-outline";i:462;s:21:"um-icon-ios-recording";i:463;s:29:"um-icon-ios-recording-outline";i:464;s:16:"um-icon-ios-redo";i:465;s:24:"um-icon-ios-redo-outline";i:466;s:19:"um-icon-ios-refresh";i:467;s:25:"um-icon-ios-refresh-empty";i:468;s:27:"um-icon-ios-refresh-outline";i:469;s:18:"um-icon-ios-reload";i:470;s:26:"um-icon-ios-reverse-camera";i:471;s:34:"um-icon-ios-reverse-camera-outline";i:472;s:18:"um-icon-ios-rewind";i:473;s:26:"um-icon-ios-rewind-outline";i:474;s:16:"um-icon-ios-rose";i:475;s:24:"um-icon-ios-rose-outline";i:476;s:18:"um-icon-ios-search";i:477;s:25:"um-icon-ios-search-strong";i:478;s:20:"um-icon-ios-settings";i:479;s:27:"um-icon-ios-settings-strong";i:480;s:19:"um-icon-ios-shuffle";i:481;s:26:"um-icon-ios-shuffle-strong";i:482;s:24:"um-icon-ios-skipbackward";i:483;s:32:"um-icon-ios-skipbackward-outline";i:484;s:23:"um-icon-ios-skipforward";i:485;s:31:"um-icon-ios-skipforward-outline";i:486;s:17:"um-icon-ios-snowy";i:487;s:23:"um-icon-ios-speedometer";i:488;s:31:"um-icon-ios-speedometer-outline";i:489;s:16:"um-icon-ios-star";i:490;s:21:"um-icon-ios-star-half";i:491;s:24:"um-icon-ios-star-outline";i:492;s:21:"um-icon-ios-stopwatch";i:493;s:29:"um-icon-ios-stopwatch-outline";i:494;s:17:"um-icon-ios-sunny";i:495;s:25:"um-icon-ios-sunny-outline";i:496;s:21:"um-icon-ios-telephone";i:497;s:29:"um-icon-ios-telephone-outline";i:498;s:22:"um-icon-ios-tennisball";i:499;s:30:"um-icon-ios-tennisball-outline";i:500;s:24:"um-icon-ios-thunderstorm";i:501;s:32:"um-icon-ios-thunderstorm-outline";i:502;s:16:"um-icon-ios-time";i:503;s:24:"um-icon-ios-time-outline";i:504;s:17:"um-icon-ios-timer";i:505;s:25:"um-icon-ios-timer-outline";i:506;s:18:"um-icon-ios-toggle";i:507;s:26:"um-icon-ios-toggle-outline";i:508;s:17:"um-icon-ios-trash";i:509;s:25:"um-icon-ios-trash-outline";i:510;s:16:"um-icon-ios-undo";i:511;s:24:"um-icon-ios-undo-outline";i:512;s:20:"um-icon-ios-unlocked";i:513;s:28:"um-icon-ios-unlocked-outline";i:514;s:18:"um-icon-ios-upload";i:515;s:26:"um-icon-ios-upload-outline";i:516;s:20:"um-icon-ios-videocam";i:517;s:28:"um-icon-ios-videocam-outline";i:518;s:23:"um-icon-ios-volume-high";i:519;s:22:"um-icon-ios-volume-low";i:520;s:21:"um-icon-ios-wineglass";i:521;s:29:"um-icon-ios-wineglass-outline";i:522;s:17:"um-icon-ios-world";i:523;s:25:"um-icon-ios-world-outline";i:524;s:12:"um-icon-ipad";i:525;s:14:"um-icon-iphone";i:526;s:12:"um-icon-ipod";i:527;s:11:"um-icon-jet";i:528;s:11:"um-icon-key";i:529;s:13:"um-icon-knife";i:530;s:14:"um-icon-laptop";i:531;s:12:"um-icon-leaf";i:532;s:14:"um-icon-levels";i:533;s:17:"um-icon-lightbulb";i:534;s:12:"um-icon-link";i:535;s:14:"um-icon-load-a";i:536;s:14:"um-icon-load-b";i:537;s:14:"um-icon-load-c";i:538;s:14:"um-icon-load-d";i:539;s:16:"um-icon-location";i:540;s:24:"um-icon-lock-combination";i:541;s:14:"um-icon-locked";i:542;s:14:"um-icon-log-in";i:543;s:15:"um-icon-log-out";i:544;s:12:"um-icon-loop";i:545;s:14:"um-icon-magnet";i:546;s:12:"um-icon-male";i:547;s:11:"um-icon-man";i:548;s:11:"um-icon-map";i:549;s:14:"um-icon-medkit";i:550;s:13:"um-icon-merge";i:551;s:13:"um-icon-mic-a";i:552;s:13:"um-icon-mic-b";i:553;s:13:"um-icon-mic-c";i:554;s:13:"um-icon-minus";i:555;s:21:"um-icon-minus-circled";i:556;s:19:"um-icon-minus-round";i:557;s:15:"um-icon-model-s";i:558;s:15:"um-icon-monitor";i:559;s:12:"um-icon-more";i:560;s:13:"um-icon-mouse";i:561;s:18:"um-icon-music-note";i:562;s:15:"um-icon-navicon";i:563;s:21:"um-icon-navicon-round";i:564;s:16:"um-icon-navigate";i:565;s:15:"um-icon-network";i:566;s:18:"um-icon-no-smoking";i:567;s:15:"um-icon-nuclear";i:568;s:14:"um-icon-outlet";i:569;s:18:"um-icon-paintbrush";i:570;s:19:"um-icon-paintbucket";i:571;s:22:"um-icon-paper-airplane";i:572;s:17:"um-icon-paperclip";i:573;s:13:"um-icon-pause";i:574;s:14:"um-icon-person";i:575;s:18:"um-icon-person-add";i:576;s:22:"um-icon-person-stalker";i:577;s:17:"um-icon-pie-graph";i:578;s:11:"um-icon-pin";i:579;s:16:"um-icon-pinpoint";i:580;s:13:"um-icon-pizza";i:581;s:13:"um-icon-plane";i:582;s:14:"um-icon-planet";i:583;s:12:"um-icon-play";i:584;s:19:"um-icon-playstation";i:585;s:12:"um-icon-plus";i:586;s:20:"um-icon-plus-circled";i:587;s:18:"um-icon-plus-round";i:588;s:14:"um-icon-podium";i:589;s:13:"um-icon-pound";i:590;s:13:"um-icon-power";i:591;s:16:"um-icon-pricetag";i:592;s:17:"um-icon-pricetags";i:593;s:15:"um-icon-printer";i:594;s:20:"um-icon-pull-request";i:595;s:18:"um-icon-qr-scanner";i:596;s:13:"um-icon-quote";i:597;s:19:"um-icon-radio-waves";i:598;s:14:"um-icon-record";i:599;s:15:"um-icon-refresh";i:600;s:13:"um-icon-reply";i:601;s:17:"um-icon-reply-all";i:602;s:16:"um-icon-ribbon-a";i:603;s:16:"um-icon-ribbon-b";i:604;s:11:"um-icon-sad";i:605;s:19:"um-icon-sad-outline";i:606;s:16:"um-icon-scissors";i:607;s:14:"um-icon-search";i:608;s:16:"um-icon-settings";i:609;s:13:"um-icon-share";i:610;s:15:"um-icon-shuffle";i:611;s:21:"um-icon-skip-backward";i:612;s:20:"um-icon-skip-forward";i:613;s:22:"um-icon-social-android";i:614;s:30:"um-icon-social-android-outline";i:615;s:22:"um-icon-social-angular";i:616;s:30:"um-icon-social-angular-outline";i:617;s:20:"um-icon-social-apple";i:618;s:28:"um-icon-social-apple-outline";i:619;s:22:"um-icon-social-bitcoin";i:620;s:30:"um-icon-social-bitcoin-outline";i:621;s:21:"um-icon-social-buffer";i:622;s:29:"um-icon-social-buffer-outline";i:623;s:21:"um-icon-social-chrome";i:624;s:29:"um-icon-social-chrome-outline";i:625;s:22:"um-icon-social-codepen";i:626;s:30:"um-icon-social-codepen-outline";i:627;s:19:"um-icon-social-css3";i:628;s:27:"um-icon-social-css3-outline";i:629;s:27:"um-icon-social-designernews";i:630;s:35:"um-icon-social-designernews-outline";i:631;s:23:"um-icon-social-dribbble";i:632;s:31:"um-icon-social-dribbble-outline";i:633;s:22:"um-icon-social-dropbox";i:634;s:30:"um-icon-social-dropbox-outline";i:635;s:19:"um-icon-social-euro";i:636;s:27:"um-icon-social-euro-outline";i:637;s:23:"um-icon-social-facebook";i:638;s:31:"um-icon-social-facebook-outline";i:639;s:25:"um-icon-social-foursquare";i:640;s:33:"um-icon-social-foursquare-outline";i:641;s:28:"um-icon-social-freebsd-devil";i:642;s:21:"um-icon-social-github";i:643;s:29:"um-icon-social-github-outline";i:644;s:21:"um-icon-social-google";i:645;s:29:"um-icon-social-google-outline";i:646;s:25:"um-icon-social-googleplus";i:647;s:33:"um-icon-social-googleplus-outline";i:648;s:25:"um-icon-social-hackernews";i:649;s:33:"um-icon-social-hackernews-outline";i:650;s:20:"um-icon-social-html5";i:651;s:28:"um-icon-social-html5-outline";i:652;s:24:"um-icon-social-instagram";i:653;s:32:"um-icon-social-instagram-outline";i:654;s:25:"um-icon-social-javascript";i:655;s:33:"um-icon-social-javascript-outline";i:656;s:23:"um-icon-social-linkedin";i:657;s:31:"um-icon-social-linkedin-outline";i:658;s:23:"um-icon-social-markdown";i:659;s:21:"um-icon-social-nodejs";i:660;s:22:"um-icon-social-octocat";i:661;s:24:"um-icon-social-pinterest";i:662;s:32:"um-icon-social-pinterest-outline";i:663;s:21:"um-icon-social-python";i:664;s:21:"um-icon-social-reddit";i:665;s:29:"um-icon-social-reddit-outline";i:666;s:18:"um-icon-social-rss";i:667;s:26:"um-icon-social-rss-outline";i:668;s:19:"um-icon-social-sass";i:669;s:20:"um-icon-social-skype";i:670;s:28:"um-icon-social-skype-outline";i:671;s:23:"um-icon-social-snapchat";i:672;s:31:"um-icon-social-snapchat-outline";i:673;s:21:"um-icon-social-tumblr";i:674;s:29:"um-icon-social-tumblr-outline";i:675;s:18:"um-icon-social-tux";i:676;s:21:"um-icon-social-twitch";i:677;s:29:"um-icon-social-twitch-outline";i:678;s:22:"um-icon-social-twitter";i:679;s:30:"um-icon-social-twitter-outline";i:680;s:18:"um-icon-social-usd";i:681;s:26:"um-icon-social-usd-outline";i:682;s:20:"um-icon-social-vimeo";i:683;s:28:"um-icon-social-vimeo-outline";i:684;s:23:"um-icon-social-whatsapp";i:685;s:31:"um-icon-social-whatsapp-outline";i:686;s:22:"um-icon-social-windows";i:687;s:30:"um-icon-social-windows-outline";i:688;s:24:"um-icon-social-wordpress";i:689;s:32:"um-icon-social-wordpress-outline";i:690;s:20:"um-icon-social-yahoo";i:691;s:28:"um-icon-social-yahoo-outline";i:692;s:18:"um-icon-social-yen";i:693;s:26:"um-icon-social-yen-outline";i:694;s:22:"um-icon-social-youtube";i:695;s:30:"um-icon-social-youtube-outline";i:696;s:16:"um-icon-soup-can";i:697;s:24:"um-icon-soup-can-outline";i:698;s:20:"um-icon-speakerphone";i:699;s:19:"um-icon-speedometer";i:700;s:13:"um-icon-spoon";i:701;s:12:"um-icon-star";i:702;s:18:"um-icon-stats-bars";i:703;s:13:"um-icon-steam";i:704;s:12:"um-icon-stop";i:705;s:19:"um-icon-thermometer";i:706;s:18:"um-icon-thumbsdown";i:707;s:16:"um-icon-thumbsup";i:708;s:14:"um-icon-toggle";i:709;s:21:"um-icon-toggle-filled";i:710;s:19:"um-icon-transgender";i:711;s:15:"um-icon-trash-a";i:712;s:15:"um-icon-trash-b";i:713;s:14:"um-icon-trophy";i:714;s:14:"um-icon-tshirt";i:715;s:22:"um-icon-tshirt-outline";i:716;s:16:"um-icon-umbrella";i:717;s:18:"um-icon-university";i:718;s:16:"um-icon-unlocked";i:719;s:14:"um-icon-upload";i:720;s:11:"um-icon-usb";i:721;s:19:"um-icon-videocamera";i:722;s:19:"um-icon-volume-high";i:723;s:18:"um-icon-volume-low";i:724;s:21:"um-icon-volume-medium";i:725;s:19:"um-icon-volume-mute";i:726;s:12:"um-icon-wand";i:727;s:17:"um-icon-waterdrop";i:728;s:12:"um-icon-wifi";i:729;s:17:"um-icon-wineglass";i:730;s:13:"um-icon-woman";i:731;s:14:"um-icon-wrench";i:732;s:12:"um-icon-xbox";i:733;s:15:"um-faicon-glass";i:734;s:15:"um-faicon-music";i:735;s:16:"um-faicon-search";i:736;s:20:"um-faicon-envelope-o";i:737;s:15:"um-faicon-heart";i:738;s:14:"um-faicon-star";i:739;s:16:"um-faicon-star-o";i:740;s:14:"um-faicon-user";i:741;s:14:"um-faicon-film";i:742;s:18:"um-faicon-th-large";i:743;s:12:"um-faicon-th";i:744;s:17:"um-faicon-th-list";i:745;s:15:"um-faicon-check";i:746;s:15:"um-faicon-times";i:747;s:21:"um-faicon-search-plus";i:748;s:22:"um-faicon-search-minus";i:749;s:19:"um-faicon-power-off";i:750;s:16:"um-faicon-signal";i:751;s:13:"um-faicon-cog";i:752;s:17:"um-faicon-trash-o";i:753;s:14:"um-faicon-home";i:754;s:16:"um-faicon-file-o";i:755;s:17:"um-faicon-clock-o";i:756;s:14:"um-faicon-road";i:757;s:18:"um-faicon-download";i:758;s:29:"um-faicon-arrow-circle-o-down";i:759;s:27:"um-faicon-arrow-circle-o-up";i:760;s:15:"um-faicon-inbox";i:761;s:23:"um-faicon-play-circle-o";i:762;s:16:"um-faicon-repeat";i:763;s:17:"um-faicon-refresh";i:764;s:18:"um-faicon-list-alt";i:765;s:14:"um-faicon-lock";i:766;s:14:"um-faicon-flag";i:767;s:20:"um-faicon-headphones";i:768;s:20:"um-faicon-volume-off";i:769;s:21:"um-faicon-volume-down";i:770;s:19:"um-faicon-volume-up";i:771;s:16:"um-faicon-qrcode";i:772;s:17:"um-faicon-barcode";i:773;s:13:"um-faicon-tag";i:774;s:14:"um-faicon-tags";i:775;s:14:"um-faicon-book";i:776;s:18:"um-faicon-bookmark";i:777;s:15:"um-faicon-print";i:778;s:16:"um-faicon-camera";i:779;s:14:"um-faicon-font";i:780;s:14:"um-faicon-bold";i:781;s:16:"um-faicon-italic";i:782;s:21:"um-faicon-text-height";i:783;s:20:"um-faicon-text-width";i:784;s:20:"um-faicon-align-left";i:785;s:22:"um-faicon-align-center";i:786;s:21:"um-faicon-align-right";i:787;s:23:"um-faicon-align-justify";i:788;s:14:"um-faicon-list";i:789;s:17:"um-faicon-outdent";i:790;s:16:"um-faicon-indent";i:791;s:22:"um-faicon-video-camera";i:792;s:19:"um-faicon-picture-o";i:793;s:16:"um-faicon-pencil";i:794;s:20:"um-faicon-map-marker";i:795;s:16:"um-faicon-adjust";i:796;s:14:"um-faicon-tint";i:797;s:25:"um-faicon-pencil-square-o";i:798;s:24:"um-faicon-share-square-o";i:799;s:24:"um-faicon-check-square-o";i:800;s:16:"um-faicon-arrows";i:801;s:23:"um-faicon-step-backward";i:802;s:23:"um-faicon-fast-backward";i:803;s:18:"um-faicon-backward";i:804;s:14:"um-faicon-play";i:805;s:15:"um-faicon-pause";i:806;s:14:"um-faicon-stop";i:807;s:17:"um-faicon-forward";i:808;s:22:"um-faicon-fast-forward";i:809;s:22:"um-faicon-step-forward";i:810;s:15:"um-faicon-eject";i:811;s:22:"um-faicon-chevron-left";i:812;s:23:"um-faicon-chevron-right";i:813;s:21:"um-faicon-plus-circle";i:814;s:22:"um-faicon-minus-circle";i:815;s:22:"um-faicon-times-circle";i:816;s:22:"um-faicon-check-circle";i:817;s:25:"um-faicon-question-circle";i:818;s:21:"um-faicon-info-circle";i:819;s:20:"um-faicon-crosshairs";i:820;s:24:"um-faicon-times-circle-o";i:821;s:24:"um-faicon-check-circle-o";i:822;s:13:"um-faicon-ban";i:823;s:20:"um-faicon-arrow-left";i:824;s:21:"um-faicon-arrow-right";i:825;s:18:"um-faicon-arrow-up";i:826;s:20:"um-faicon-arrow-down";i:827;s:15:"um-faicon-share";i:828;s:16:"um-faicon-expand";i:829;s:18:"um-faicon-compress";i:830;s:14:"um-faicon-plus";i:831;s:15:"um-faicon-minus";i:832;s:18:"um-faicon-asterisk";i:833;s:28:"um-faicon-exclamation-circle";i:834;s:14:"um-faicon-gift";i:835;s:14:"um-faicon-leaf";i:836;s:14:"um-faicon-fire";i:837;s:13:"um-faicon-eye";i:838;s:19:"um-faicon-eye-slash";i:839;s:30:"um-faicon-exclamation-triangle";i:840;s:15:"um-faicon-plane";i:841;s:18:"um-faicon-calendar";i:842;s:16:"um-faicon-random";i:843;s:17:"um-faicon-comment";i:844;s:16:"um-faicon-magnet";i:845;s:20:"um-faicon-chevron-up";i:846;s:22:"um-faicon-chevron-down";i:847;s:17:"um-faicon-retweet";i:848;s:23:"um-faicon-shopping-cart";i:849;s:16:"um-faicon-folder";i:850;s:21:"um-faicon-folder-open";i:851;s:18:"um-faicon-arrows-v";i:852;s:18:"um-faicon-arrows-h";i:853;s:19:"um-faicon-bar-chart";i:854;s:24:"um-faicon-twitter-square";i:855;s:25:"um-faicon-facebook-square";i:856;s:22:"um-faicon-camera-retro";i:857;s:13:"um-faicon-key";i:858;s:14:"um-faicon-cogs";i:859;s:18:"um-faicon-comments";i:860;s:21:"um-faicon-thumbs-o-up";i:861;s:23:"um-faicon-thumbs-o-down";i:862;s:19:"um-faicon-star-half";i:863;s:17:"um-faicon-heart-o";i:864;s:18:"um-faicon-sign-out";i:865;s:25:"um-faicon-linkedin-square";i:866;s:20:"um-faicon-thumb-tack";i:867;s:23:"um-faicon-external-link";i:868;s:17:"um-faicon-sign-in";i:869;s:16:"um-faicon-trophy";i:870;s:23:"um-faicon-github-square";i:871;s:16:"um-faicon-upload";i:872;s:17:"um-faicon-lemon-o";i:873;s:15:"um-faicon-phone";i:874;s:18:"um-faicon-square-o";i:875;s:20:"um-faicon-bookmark-o";i:876;s:22:"um-faicon-phone-square";i:877;s:17:"um-faicon-twitter";i:878;s:18:"um-faicon-facebook";i:879;s:16:"um-faicon-github";i:880;s:16:"um-faicon-unlock";i:881;s:21:"um-faicon-credit-card";i:882;s:13:"um-faicon-rss";i:883;s:15:"um-faicon-hdd-o";i:884;s:18:"um-faicon-bullhorn";i:885;s:14:"um-faicon-bell";i:886;s:21:"um-faicon-certificate";i:887;s:22:"um-faicon-hand-o-right";i:888;s:21:"um-faicon-hand-o-left";i:889;s:19:"um-faicon-hand-o-up";i:890;s:21:"um-faicon-hand-o-down";i:891;s:27:"um-faicon-arrow-circle-left";i:892;s:28:"um-faicon-arrow-circle-right";i:893;s:25:"um-faicon-arrow-circle-up";i:894;s:27:"um-faicon-arrow-circle-down";i:895;s:15:"um-faicon-globe";i:896;s:16:"um-faicon-wrench";i:897;s:15:"um-faicon-tasks";i:898;s:16:"um-faicon-filter";i:899;s:19:"um-faicon-briefcase";i:900;s:20:"um-faicon-arrows-alt";i:901;s:15:"um-faicon-users";i:902;s:14:"um-faicon-link";i:903;s:15:"um-faicon-cloud";i:904;s:15:"um-faicon-flask";i:905;s:18:"um-faicon-scissors";i:906;s:17:"um-faicon-files-o";i:907;s:19:"um-faicon-paperclip";i:908;s:18:"um-faicon-floppy-o";i:909;s:16:"um-faicon-square";i:910;s:14:"um-faicon-bars";i:911;s:17:"um-faicon-list-ul";i:912;s:17:"um-faicon-list-ol";i:913;s:23:"um-faicon-strikethrough";i:914;s:19:"um-faicon-underline";i:915;s:15:"um-faicon-table";i:916;s:15:"um-faicon-magic";i:917;s:15:"um-faicon-truck";i:918;s:19:"um-faicon-pinterest";i:919;s:26:"um-faicon-pinterest-square";i:920;s:28:"um-faicon-google-plus-square";i:921;s:21:"um-faicon-google-plus";i:922;s:15:"um-faicon-money";i:923;s:20:"um-faicon-caret-down";i:924;s:18:"um-faicon-caret-up";i:925;s:20:"um-faicon-caret-left";i:926;s:21:"um-faicon-caret-right";i:927;s:17:"um-faicon-columns";i:928;s:14:"um-faicon-sort";i:929;s:19:"um-faicon-sort-desc";i:930;s:18:"um-faicon-sort-asc";i:931;s:18:"um-faicon-envelope";i:932;s:18:"um-faicon-linkedin";i:933;s:14:"um-faicon-undo";i:934;s:15:"um-faicon-gavel";i:935;s:20:"um-faicon-tachometer";i:936;s:19:"um-faicon-comment-o";i:937;s:20:"um-faicon-comments-o";i:938;s:14:"um-faicon-bolt";i:939;s:17:"um-faicon-sitemap";i:940;s:18:"um-faicon-umbrella";i:941;s:19:"um-faicon-clipboard";i:942;s:21:"um-faicon-lightbulb-o";i:943;s:18:"um-faicon-exchange";i:944;s:24:"um-faicon-cloud-download";i:945;s:22:"um-faicon-cloud-upload";i:946;s:17:"um-faicon-user-md";i:947;s:21:"um-faicon-stethoscope";i:948;s:18:"um-faicon-suitcase";i:949;s:16:"um-faicon-bell-o";i:950;s:16:"um-faicon-coffee";i:951;s:17:"um-faicon-cutlery";i:952;s:21:"um-faicon-file-text-o";i:953;s:20:"um-faicon-building-o";i:954;s:20:"um-faicon-hospital-o";i:955;s:19:"um-faicon-ambulance";i:956;s:16:"um-faicon-medkit";i:957;s:21:"um-faicon-fighter-jet";i:958;s:14:"um-faicon-beer";i:959;s:18:"um-faicon-h-square";i:960;s:21:"um-faicon-plus-square";i:961;s:27:"um-faicon-angle-double-left";i:962;s:28:"um-faicon-angle-double-right";i:963;s:25:"um-faicon-angle-double-up";i:964;s:27:"um-faicon-angle-double-down";i:965;s:20:"um-faicon-angle-left";i:966;s:21:"um-faicon-angle-right";i:967;s:18:"um-faicon-angle-up";i:968;s:20:"um-faicon-angle-down";i:969;s:17:"um-faicon-desktop";i:970;s:16:"um-faicon-laptop";i:971;s:16:"um-faicon-tablet";i:972;s:16:"um-faicon-mobile";i:973;s:18:"um-faicon-circle-o";i:974;s:20:"um-faicon-quote-left";i:975;s:21:"um-faicon-quote-right";i:976;s:17:"um-faicon-spinner";i:977;s:16:"um-faicon-circle";i:978;s:15:"um-faicon-reply";i:979;s:20:"um-faicon-github-alt";i:980;s:18:"um-faicon-folder-o";i:981;s:23:"um-faicon-folder-open-o";i:982;s:17:"um-faicon-smile-o";i:983;s:17:"um-faicon-frown-o";i:984;s:15:"um-faicon-meh-o";i:985;s:17:"um-faicon-gamepad";i:986;s:20:"um-faicon-keyboard-o";i:987;s:16:"um-faicon-flag-o";i:988;s:24:"um-faicon-flag-checkered";i:989;s:18:"um-faicon-terminal";i:990;s:14:"um-faicon-code";i:991;s:19:"um-faicon-reply-all";i:992;s:21:"um-faicon-star-half-o";i:993;s:24:"um-faicon-location-arrow";i:994;s:14:"um-faicon-crop";i:995;s:19:"um-faicon-code-fork";i:996;s:22:"um-faicon-chain-broken";i:997;s:18:"um-faicon-question";i:998;s:14:"um-faicon-info";i:999;s:21:"um-faicon-exclamation";i:1000;s:21:"um-faicon-superscript";i:1001;s:19:"um-faicon-subscript";i:1002;s:16:"um-faicon-eraser";i:1003;s:22:"um-faicon-puzzle-piece";i:1004;s:20:"um-faicon-microphone";i:1005;s:26:"um-faicon-microphone-slash";i:1006;s:16:"um-faicon-shield";i:1007;s:20:"um-faicon-calendar-o";i:1008;s:27:"um-faicon-fire-extinguisher";i:1009;s:16:"um-faicon-rocket";i:1010;s:16:"um-faicon-maxcdn";i:1011;s:29:"um-faicon-chevron-circle-left";i:1012;s:30:"um-faicon-chevron-circle-right";i:1013;s:27:"um-faicon-chevron-circle-up";i:1014;s:29:"um-faicon-chevron-circle-down";i:1015;s:15:"um-faicon-html5";i:1016;s:14:"um-faicon-css3";i:1017;s:16:"um-faicon-anchor";i:1018;s:20:"um-faicon-unlock-alt";i:1019;s:18:"um-faicon-bullseye";i:1020;s:20:"um-faicon-ellipsis-h";i:1021;s:20:"um-faicon-ellipsis-v";i:1022;s:20:"um-faicon-rss-square";i:1023;s:21:"um-faicon-play-circle";i:1024;s:16:"um-faicon-ticket";i:1025;s:22:"um-faicon-minus-square";i:1026;s:24:"um-faicon-minus-square-o";i:1027;s:18:"um-faicon-level-up";i:1028;s:20:"um-faicon-level-down";i:1029;s:22:"um-faicon-check-square";i:1030;s:23:"um-faicon-pencil-square";i:1031;s:30:"um-faicon-external-link-square";i:1032;s:22:"um-faicon-share-square";i:1033;s:17:"um-faicon-compass";i:1034;s:29:"um-faicon-caret-square-o-down";i:1035;s:27:"um-faicon-caret-square-o-up";i:1036;s:30:"um-faicon-caret-square-o-right";i:1037;s:13:"um-faicon-eur";i:1038;s:13:"um-faicon-gbp";i:1039;s:13:"um-faicon-usd";i:1040;s:13:"um-faicon-inr";i:1041;s:13:"um-faicon-jpy";i:1042;s:13:"um-faicon-rub";i:1043;s:13:"um-faicon-krw";i:1044;s:13:"um-faicon-btc";i:1045;s:14:"um-faicon-file";i:1046;s:19:"um-faicon-file-text";i:1047;s:24:"um-faicon-sort-alpha-asc";i:1048;s:25:"um-faicon-sort-alpha-desc";i:1049;s:25:"um-faicon-sort-amount-asc";i:1050;s:26:"um-faicon-sort-amount-desc";i:1051;s:26:"um-faicon-sort-numeric-asc";i:1052;s:27:"um-faicon-sort-numeric-desc";i:1053;s:19:"um-faicon-thumbs-up";i:1054;s:21:"um-faicon-thumbs-down";i:1055;s:24:"um-faicon-youtube-square";i:1056;s:17:"um-faicon-youtube";i:1057;s:14:"um-faicon-xing";i:1058;s:21:"um-faicon-xing-square";i:1059;s:22:"um-faicon-youtube-play";i:1060;s:17:"um-faicon-dropbox";i:1061;s:24:"um-faicon-stack-overflow";i:1062;s:19:"um-faicon-instagram";i:1063;s:16:"um-faicon-flickr";i:1064;s:13:"um-faicon-adn";i:1065;s:19:"um-faicon-bitbucket";i:1066;s:26:"um-faicon-bitbucket-square";i:1067;s:16:"um-faicon-tumblr";i:1068;s:23:"um-faicon-tumblr-square";i:1069;s:25:"um-faicon-long-arrow-down";i:1070;s:23:"um-faicon-long-arrow-up";i:1071;s:25:"um-faicon-long-arrow-left";i:1072;s:26:"um-faicon-long-arrow-right";i:1073;s:15:"um-faicon-apple";i:1074;s:17:"um-faicon-windows";i:1075;s:17:"um-faicon-android";i:1076;s:15:"um-faicon-linux";i:1077;s:18:"um-faicon-dribbble";i:1078;s:15:"um-faicon-skype";i:1079;s:20:"um-faicon-foursquare";i:1080;s:16:"um-faicon-trello";i:1081;s:16:"um-faicon-female";i:1082;s:14:"um-faicon-male";i:1083;s:16:"um-faicon-gittip";i:1084;s:15:"um-faicon-sun-o";i:1085;s:16:"um-faicon-moon-o";i:1086;s:17:"um-faicon-archive";i:1087;s:13:"um-faicon-bug";i:1088;s:12:"um-faicon-vk";i:1089;s:15:"um-faicon-weibo";i:1090;s:16:"um-faicon-renren";i:1091;s:19:"um-faicon-pagelines";i:1092;s:24:"um-faicon-stack-exchange";i:1093;s:30:"um-faicon-arrow-circle-o-right";i:1094;s:29:"um-faicon-arrow-circle-o-left";i:1095;s:29:"um-faicon-caret-square-o-left";i:1096;s:22:"um-faicon-dot-circle-o";i:1097;s:20:"um-faicon-wheelchair";i:1098;s:22:"um-faicon-vimeo-square";i:1099;s:13:"um-faicon-try";i:1100;s:23:"um-faicon-plus-square-o";i:1101;s:23:"um-faicon-space-shuttle";i:1102;s:15:"um-faicon-slack";i:1103;s:25:"um-faicon-envelope-square";i:1104;s:19:"um-faicon-wordpress";i:1105;s:16:"um-faicon-openid";i:1106;s:20:"um-faicon-university";i:1107;s:24:"um-faicon-graduation-cap";i:1108;s:15:"um-faicon-yahoo";i:1109;s:16:"um-faicon-google";i:1110;s:16:"um-faicon-reddit";i:1111;s:23:"um-faicon-reddit-square";i:1112;s:28:"um-faicon-stumbleupon-circle";i:1113;s:21:"um-faicon-stumbleupon";i:1114;s:19:"um-faicon-delicious";i:1115;s:14:"um-faicon-digg";i:1116;s:20:"um-faicon-pied-piper";i:1117;s:24:"um-faicon-pied-piper-alt";i:1118;s:16:"um-faicon-drupal";i:1119;s:16:"um-faicon-joomla";i:1120;s:18:"um-faicon-language";i:1121;s:13:"um-faicon-fax";i:1122;s:18:"um-faicon-building";i:1123;s:15:"um-faicon-child";i:1124;s:13:"um-faicon-paw";i:1125;s:15:"um-faicon-spoon";i:1126;s:14:"um-faicon-cube";i:1127;s:15:"um-faicon-cubes";i:1128;s:17:"um-faicon-behance";i:1129;s:24:"um-faicon-behance-square";i:1130;s:15:"um-faicon-steam";i:1131;s:22:"um-faicon-steam-square";i:1132;s:17:"um-faicon-recycle";i:1133;s:13:"um-faicon-car";i:1134;s:14:"um-faicon-taxi";i:1135;s:14:"um-faicon-tree";i:1136;s:17:"um-faicon-spotify";i:1137;s:20:"um-faicon-deviantart";i:1138;s:20:"um-faicon-soundcloud";i:1139;s:18:"um-faicon-database";i:1140;s:20:"um-faicon-file-pdf-o";i:1141;s:21:"um-faicon-file-word-o";i:1142;s:22:"um-faicon-file-excel-o";i:1143;s:27:"um-faicon-file-powerpoint-o";i:1144;s:22:"um-faicon-file-image-o";i:1145;s:24:"um-faicon-file-archive-o";i:1146;s:22:"um-faicon-file-audio-o";i:1147;s:22:"um-faicon-file-video-o";i:1148;s:21:"um-faicon-file-code-o";i:1149;s:14:"um-faicon-vine";i:1150;s:17:"um-faicon-codepen";i:1151;s:18:"um-faicon-jsfiddle";i:1152;s:19:"um-faicon-life-ring";i:1153;s:24:"um-faicon-circle-o-notch";i:1154;s:15:"um-faicon-rebel";i:1155;s:16:"um-faicon-empire";i:1156;s:20:"um-faicon-git-square";i:1157;s:13:"um-faicon-git";i:1158;s:21:"um-faicon-hacker-news";i:1159;s:23:"um-faicon-tencent-weibo";i:1160;s:12:"um-faicon-qq";i:1161;s:16:"um-faicon-weixin";i:1162;s:21:"um-faicon-paper-plane";i:1163;s:23:"um-faicon-paper-plane-o";i:1164;s:17:"um-faicon-history";i:1165;s:21:"um-faicon-circle-thin";i:1166;s:16:"um-faicon-header";i:1167;s:19:"um-faicon-paragraph";i:1168;s:17:"um-faicon-sliders";i:1169;s:19:"um-faicon-share-alt";i:1170;s:26:"um-faicon-share-alt-square";i:1171;s:14:"um-faicon-bomb";i:1172;s:18:"um-faicon-futbol-o";i:1173;s:13:"um-faicon-tty";i:1174;s:20:"um-faicon-binoculars";i:1175;s:14:"um-faicon-plug";i:1176;s:20:"um-faicon-slideshare";i:1177;s:16:"um-faicon-twitch";i:1178;s:14:"um-faicon-yelp";i:1179;s:21:"um-faicon-newspaper-o";i:1180;s:14:"um-faicon-wifi";i:1181;s:20:"um-faicon-calculator";i:1182;s:16:"um-faicon-paypal";i:1183;s:23:"um-faicon-google-wallet";i:1184;s:17:"um-faicon-cc-visa";i:1185;s:23:"um-faicon-cc-mastercard";i:1186;s:21:"um-faicon-cc-discover";i:1187;s:17:"um-faicon-cc-amex";i:1188;s:19:"um-faicon-cc-paypal";i:1189;s:19:"um-faicon-cc-stripe";i:1190;s:20:"um-faicon-bell-slash";i:1191;s:22:"um-faicon-bell-slash-o";i:1192;s:15:"um-faicon-trash";i:1193;s:19:"um-faicon-copyright";i:1194;s:12:"um-faicon-at";i:1195;s:20:"um-faicon-eyedropper";i:1196;s:21:"um-faicon-paint-brush";i:1197;s:23:"um-faicon-birthday-cake";i:1198;s:20:"um-faicon-area-chart";i:1199;s:19:"um-faicon-pie-chart";i:1200;s:20:"um-faicon-line-chart";i:1201;s:16:"um-faicon-lastfm";i:1202;s:23:"um-faicon-lastfm-square";i:1203;s:20:"um-faicon-toggle-off";i:1204;s:19:"um-faicon-toggle-on";i:1205;s:17:"um-faicon-bicycle";i:1206;s:13:"um-faicon-bus";i:1207;s:17:"um-faicon-ioxhost";i:1208;s:19:"um-faicon-angellist";i:1209;s:12:"um-faicon-cc";i:1210;s:13:"um-faicon-ils";i:1211;s:18:"um-faicon-meanpath";}', 'yes'),
(439, '__ultimatemember_sitekey', '127.0.0.1/corbeilassoc-BcfDWRr7w8qf1LI8Cnix', 'yes'),
(440, 'um_is_installed', '1', 'yes'),
(441, 'um_core_forms', 'a:3:{i:61;i:61;i:62;i:62;i:63;i:63;}', 'yes'),
(442, 'um_core_directories', 'a:1:{i:64;i:64;}', 'yes'),
(443, 'um_core_pages', 'a:7:{s:4:"user";i:65;s:5:"login";i:67;s:8:"register";i:69;s:7:"members";i:71;s:6:"logout";i:73;s:7:"account";i:75;s:14:"password-reset";i:77;}', 'yes'),
(444, 'um_first_setup_roles', '1', 'yes'),
(445, 'um_hashed_passwords_fix', '1', 'yes');
INSERT INTO `crbass_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(447, 'um_options', 'a:165:{s:8:"last_tab";s:0:"";s:12:"default_role";s:6:"member";s:14:"permalink_base";s:10:"user_login";s:12:"display_name";s:9:"full_name";s:18:"display_name_field";s:0:"";s:15:"author_redirect";s:1:"1";s:12:"members_page";s:1:"1";s:13:"use_gravatars";s:1:"0";s:24:"reset_require_strongpass";s:1:"0";s:20:"account_tab_password";s:1:"1";s:19:"account_tab_privacy";s:1:"1";s:25:"account_tab_notifications";s:1:"1";s:18:"account_tab_delete";s:1:"1";s:19:"delete_account_text";s:190:"Êtes-vous sûr de vouloir supprimer votre compte ? Cela effacera toutes les données du comptes présentent sur le site. Pour supprimer votre compte, entrer votre mot de passe ci-dessous.";s:12:"account_name";s:1:"1";s:26:"account_require_strongpass";s:1:"0";s:9:"panic_key";s:10:"9AaLl1sosK";s:10:"accessible";s:1:"0";s:15:"access_redirect";s:0:"";s:22:"exclude_from_main_loop";s:1:"1";s:24:"exclude_from_search_loop";s:1:"1";s:25:"exclude_from_archive_loop";s:1:"1";s:13:"wpadmin_login";s:1:"1";s:22:"wpadmin_login_redirect";s:13:"um_login_page";s:26:"wpadmin_login_redirect_url";s:0:"";s:16:"wpadmin_register";s:1:"1";s:25:"wpadmin_register_redirect";s:16:"um_register_page";s:29:"wpadmin_register_redirect_url";s:0:"";s:17:"wpadmin_allow_ips";s:0:"";s:11:"blocked_ips";s:0:"";s:14:"blocked_emails";s:0:"";s:13:"blocked_words";s:47:"admin\r\nadministrator\r\nwebmaster\r\nsupport\r\nstaff";s:9:"mail_from";s:32:"Associations de Corbeil-Essonnes";s:14:"mail_from_addr";s:23:"contact@tiar-florian.fr";s:10:"email_html";s:1:"0";s:16:"welcome_email_on";s:1:"1";s:17:"welcome_email_sub";s:38:"Bienvenue sur le site des {site_name}!";s:13:"welcome_email";s:370:"Bonjour {display_name},\r\n\r\nMerci de vous êtes inscrit sur le site des {site_name} ! Votre compte est maintenant activé.\r\n\r\nCliquez sur le lien suivant pour vous connectez :\r\n\r\n{login_url}\r\n\r\nVotre adresse e-mail: {email}\r\nVotre compte : {username}\r\n\r\nSi vous avez le moindre problème, contactez nous à l''adresse mail suivante : {admin_email}\r\n\r\nMerci,\r\n\r\n{site_name}";s:18:"checkmail_email_on";s:1:"1";s:19:"checkmail_email_sub";s:35:"Activation du compte :  {site_name}";s:15:"checkmail_email";s:329:"Bonjour {display_name},\r\n\r\nMerci de vous êtes inscrit sur le site des {site_name} ! \r\n\r\nPour activer votre compte, cliquez sur le lien suivant pour confirmer votre adresse email.\r\n\r\n{account_activation_link}\r\n\r\nSi vous avez le moindre problème, contactez nous à l''adresse mail suivante : {admin_email}\r\n\r\nMerci,\r\n\r\n{site_name}";s:16:"pending_email_on";s:1:"1";s:17:"pending_email_sub";s:30:"Your account is pending review";s:13:"pending_email";s:272:"Hi {display_name},\r\n\r\nThank you for signing up with {site_name}! Your account is currently being reviewed by a member of our team.\r\n\r\nPlease allow us some time to process your request.\r\n\r\nIf you have any problems, please contact us at {admin_email}\r\n\r\nThanks,\r\n{site_name}";s:17:"approved_email_on";s:1:"1";s:18:"approved_email_sub";s:41:"Your account at {site_name} is now active";s:14:"approved_email";s:363:"Hi {display_name},\r\n\r\nThank you for signing up with {site_name}! Your account has been approved and is now active.\r\n\r\nTo login please visit the following url:\r\n\r\n{login_url}\r\n\r\nYour account e-mail: {email}\r\nYour account username: {username}\r\nYour account password: {password}\r\n\r\nIf you have any problems, please contact us at {admin_email}\r\n\r\nThanks,\r\n{site_name}";s:17:"rejected_email_on";s:1:"1";s:18:"rejected_email_sub";s:30:"Your account has been rejected";s:14:"rejected_email";s:260:"Hi {display_name},\r\n\r\nThank you for applying for membership to {site_name}! We have reviewed your information and unfortunately we are unable to accept you as a member at this moment.\r\n\r\nPlease feel free to apply again at a future date.\r\n\r\nThanks,\r\n{site_name}";s:17:"inactive_email_on";s:1:"1";s:18:"inactive_email_sub";s:33:"Your account has been deactivated";s:14:"inactive_email";s:222:"Hi {display_name},\r\n\r\nThis is an automated email to let you know your {site_name} account has been deactivated.\r\n\r\nIf you would like your account to be reactivated please contact us at {admin_email}\r\n\r\nThanks,\r\n{site_name}";s:17:"deletion_email_on";s:1:"1";s:18:"deletion_email_sub";s:29:"Your account has been deleted";s:14:"deletion_email";s:332:"Hi {display_name},\r\n\r\nThis is an automated email to let you know your {site_name} account has been deleted. All of your personal information has been permanently deleted and you will no longer be able to login to {site_name}.\r\n\r\nIf your account has been deleted by accident please contact us at {admin_email}\r\n\r\nThanks,\r\n{site_name}";s:16:"resetpw_email_on";s:1:"1";s:17:"resetpw_email_sub";s:19:"Reset your password";s:13:"resetpw_email";s:266:"Hi {display_name},\r\n\r\nWe received a request to reset the password for your account. If you made this request, click the link below to change your password:\r\n\r\n{password_reset_link}\r\n\r\nIf you didn''t make this request, you can ignore this email\r\n\r\nThanks,\r\n{site_name}";s:18:"changedpw_email_on";s:1:"1";s:19:"changedpw_email_sub";s:42:"Your {site_name} password has been changed";s:15:"changedpw_email";s:279:"Hi {display_name},\r\n\r\nYou recently changed the password associated with your {site_name} account.\r\n\r\nIf you did not make this change and believe your {site_name} account has been compromised, please contact us at the following email address: {admin_email}\r\n\r\nThanks,\r\n{site_name}";s:11:"admin_email";s:23:"contact@tiar-florian.fr";s:24:"notification_new_user_on";s:1:"1";s:25:"notification_new_user_sub";s:30:"[{site_name}] New user account";s:21:"notification_new_user";s:187:"{display_name} has just created an account on {site_name}. To view their profile click here:\r\n\r\n{user_profile_link}\r\n\r\nHere is the submitted registration form:\r\n\r\n{submitted_registration}";s:22:"notification_review_on";s:1:"1";s:23:"notification_review_sub";s:38:"[{site_name}] New user awaiting review";s:19:"notification_review";s:245:"{display_name} has just applied for membership to {site_name} and is waiting to be reviewed.\r\n\r\nTo review this member please click the following link:\r\n\r\n{user_profile_link}\r\n\r\nHere is the submitted registration form:\r\n\r\n{submitted_registration}";s:24:"notification_deletion_on";s:1:"0";s:25:"notification_deletion_sub";s:29:"[{site_name}] Account deleted";s:21:"notification_deletion";s:58:"{display_name} has just deleted their {site_name} account.";s:22:"profile_photo_max_size";s:0:"";s:20:"cover_photo_max_size";s:0:"";s:17:"photo_thumb_sizes";a:3:{i:0;s:2:"40";i:1;s:2:"80";i:2;s:3:"190";}s:17:"cover_thumb_sizes";a:2:{i:0;s:3:"300";i:1;s:3:"600";}s:17:"image_compression";s:2:"60";s:15:"image_max_width";s:4:"1000";s:15:"cover_min_width";s:4:"1000";s:13:"profile_title";s:49:"{display_name} | Associations de Corbeil-Essonnes";s:12:"profile_desc";s:83:"{display_name} is on {site_name}. Join {site_name} to view {display_name}''s profile";s:18:"directory_template";s:7:"members";s:12:"active_color";s:7:"#3ba1da";s:15:"secondary_color";s:7:"#44b0ec";s:17:"primary_btn_color";s:7:"#3ba1da";s:17:"primary_btn_hover";s:7:"#44b0ec";s:16:"primary_btn_text";s:7:"#ffffff";s:19:"secondary_btn_color";s:7:"#eeeeee";s:19:"secondary_btn_hover";s:7:"#e5e5e5";s:18:"secondary_btn_text";s:7:"#666666";s:14:"help_tip_color";s:7:"#cccccc";s:16:"form_field_label";s:7:"#555555";s:11:"form_border";s:14:"2px solid #ddd";s:13:"form_bg_color";s:7:"#ffffff";s:15:"form_text_color";s:7:"#666666";s:16:"form_placeholder";s:7:"#aaaaaa";s:15:"form_icon_color";s:7:"#aaaaaa";s:13:"form_asterisk";s:1:"0";s:19:"form_asterisk_color";s:7:"#aaaaaa";s:16:"profile_template";s:7:"profile";s:17:"profile_max_width";s:6:"1000px";s:22:"profile_area_max_width";s:5:"600px";s:13:"profile_align";s:6:"center";s:13:"profile_icons";s:5:"label";s:24:"profile_primary_btn_word";s:14:"Update Profile";s:21:"profile_secondary_btn";s:1:"1";s:26:"profile_secondary_btn_word";s:6:"Cancel";s:12:"profile_role";s:1:"0";s:15:"profile_main_bg";s:0:"";s:17:"profile_header_bg";s:0:"";s:14:"default_avatar";a:5:{s:3:"url";s:94:"http://127.0.0.1/corbeilassoc/wp-content/plugins/ultimate-member/assets/img/default_avatar.jpg";s:2:"id";s:0:"";s:6:"height";s:0:"";s:5:"width";s:0:"";s:9:"thumbnail";s:0:"";}s:13:"default_cover";a:5:{s:3:"url";s:0:"";s:2:"id";s:0:"";s:6:"height";s:0:"";s:5:"width";s:0:"";s:9:"thumbnail";s:0:"";}s:17:"profile_photosize";s:5:"190px";s:19:"profile_photocorner";s:1:"1";s:21:"profile_cover_enabled";s:1:"1";s:19:"profile_cover_ratio";s:5:"2.7:1";s:21:"profile_show_metaicon";s:1:"0";s:19:"profile_header_text";s:7:"#999999";s:25:"profile_header_link_color";s:7:"#555555";s:26:"profile_header_link_hcolor";s:7:"#444444";s:25:"profile_header_icon_color";s:7:"#aaaaaa";s:26:"profile_header_icon_hcolor";s:7:"#3ba1da";s:17:"profile_show_name";s:1:"1";s:16:"profile_show_bio";s:1:"1";s:20:"profile_bio_maxchars";s:3:"180";s:19:"profile_header_menu";s:2:"bc";s:18:"profile_empty_text";s:1:"1";s:22:"profile_empty_text_emo";s:1:"1";s:12:"profile_menu";s:1:"1";s:16:"profile_tab_main";s:1:"1";s:17:"profile_tab_posts";s:1:"1";s:20:"profile_tab_comments";s:1:"1";s:24:"profile_menu_default_tab";s:4:"main";s:18:"profile_menu_icons";s:1:"1";s:19:"profile_menu_counts";s:1:"1";s:17:"register_template";s:8:"register";s:18:"register_max_width";s:5:"450px";s:14:"register_align";s:6:"center";s:14:"register_icons";s:5:"label";s:25:"register_primary_btn_word";s:8:"Register";s:22:"register_secondary_btn";s:1:"1";s:27:"register_secondary_btn_word";s:5:"Login";s:26:"register_secondary_btn_url";s:0:"";s:13:"register_role";s:1:"0";s:14:"login_template";s:5:"login";s:15:"login_max_width";s:5:"450px";s:11:"login_align";s:6:"center";s:11:"login_icons";s:5:"label";s:22:"login_primary_btn_word";s:5:"Login";s:19:"login_secondary_btn";s:1:"1";s:24:"login_secondary_btn_word";s:8:"Register";s:23:"login_secondary_btn_url";s:0:"";s:22:"login_forgot_pass_link";s:1:"1";s:21:"login_show_rememberme";s:1:"1";s:14:"enable_timebot";s:1:"1";s:14:"disable_minify";s:1:"0";s:12:"disable_menu";s:1:"0";s:19:"js_css_exlcude_home";s:1:"0";s:17:"enable_custom_css";s:1:"0";s:14:"allow_tracking";s:1:"0";s:24:"addon_bp_avatar_transfer";s:0:"";s:19:"access_exclude_uris";a:0:{}s:14:"js_css_exclude";a:0:{}s:14:"js_css_include";a:0:{}s:25:"profile_show_social_links";i:0;}', 'yes'),
(448, 'um_options-transients', 'a:2:{s:14:"changed_values";a:1:{s:22:"notification_review_on";s:1:"0";}s:9:"last_save";i:1426505939;}', 'yes'),
(456, 'um_tracking_notice', '1', 'yes'),
(488, 'um_existing_rows_61', 'a:1:{i:0;s:9:"_um_row_1";}', 'yes'),
(489, 'um_form_rowdata_61', 'a:1:{s:9:"_um_row_1";a:5:{s:4:"type";s:3:"row";s:2:"id";s:9:"_um_row_1";s:8:"sub_rows";s:1:"1";s:4:"cols";s:1:"1";s:6:"origin";s:9:"_um_row_1";}}', 'yes'),
(493, 'um_existing_rows_62', 'a:1:{i:0;s:9:"_um_row_1";}', 'yes'),
(494, 'um_form_rowdata_62', 'a:1:{s:9:"_um_row_1";a:5:{s:4:"type";s:3:"row";s:2:"id";s:9:"_um_row_1";s:8:"sub_rows";s:1:"1";s:4:"cols";s:1:"1";s:6:"origin";s:9:"_um_row_1";}}', 'yes'),
(956, 'um_existing_rows_63', 'a:1:{i:0;s:9:"_um_row_1";}', 'yes'),
(957, 'um_form_rowdata_63', 'a:1:{s:9:"_um_row_1";a:5:{s:4:"type";s:3:"row";s:2:"id";s:9:"_um_row_1";s:8:"sub_rows";s:1:"1";s:4:"cols";s:1:"1";s:6:"origin";s:9:"_um_row_1";}}', 'yes'),
(1527, 'auto_core_update_notified', 'a:4:{s:4:"type";s:4:"fail";s:5:"email";s:23:"contact@tiar-florian.fr";s:7:"version";s:5:"4.2.2";s:9:"timestamp";i:1432878690;}', 'yes'),
(1788, 'wysija_post_type_updated', '1426946050', 'yes'),
(1790, 'wysija_post_type_created', '1426946050', 'yes'),
(1792, 'installation_step', '16', 'yes'),
(1793, 'wysija', 'YTo4Njp7czo5OiJmcm9tX25hbWUiO3M6MTI6IkNvcmJlaWxBc3NvYyI7czoxMjoicmVwbHl0b19uYW1lIjtzOjIyOiJXZWJtYXN0ZXIgQ29yYmVpbEFzc29jIjtzOjE1OiJlbWFpbHNfbm90aWZpZWQiO3M6MjY6Im5ld3NsZXR0ZXJAdGlhci1mbG9yaWFuLmZyIjtzOjEwOiJmcm9tX2VtYWlsIjtzOjE0OiJpbmZvQDEyNy4wLjAuMSI7czoxMzoicmVwbHl0b19lbWFpbCI7czoxNDoiaW5mb0AxMjcuMC4wLjEiO3M6MTU6ImRlZmF1bHRfbGlzdF9pZCI7aToxO3M6MTc6InRvdGFsX3N1YnNjcmliZXJzIjtzOjE6IjQiO3M6MTY6ImltcG9ydHdwX2xpc3RfaWQiO2k6MjtzOjE4OiJjb25maXJtX2VtYWlsX2xpbmsiO2k6MTI2O3M6MTI6InVwbG9hZGZvbGRlciI7czo1NToiQzpceGFtcHBcaHRkb2NzXGNvcmJlaWxhc3NvYy93cC1jb250ZW50L3VwbG9hZHNcd3lzaWphXCI7czo5OiJ1cGxvYWR1cmwiO3M6NTY6Imh0dHA6Ly8xMjcuMC4wLjEvY29yYmVpbGFzc29jL3dwLWNvbnRlbnQvdXBsb2Fkcy93eXNpamEvIjtzOjE2OiJjb25maXJtX2VtYWlsX2lkIjtpOjI7czo5OiJpbnN0YWxsZWQiO2I6MTtzOjIwOiJtYW5hZ2Vfc3Vic2NyaXB0aW9ucyI7aToxO3M6MTQ6Imluc3RhbGxlZF90aW1lIjtpOjE0MjY5NDYwNTU7czoxNzoid3lzaWphX2RiX3ZlcnNpb24iO3M6NjoiMi42LjE1IjtzOjExOiJka2ltX2RvbWFpbiI7czo5OiIxMjcuMC4wLjEiO3M6MTY6Ind5c2lqYV93aGF0c19uZXciO3M6NjoiMi42LjE1IjtzOjE1OiJjb21wYW55X2FkZHJlc3MiO3M6MDoiIjtzOjI0OiJlbWFpbHNfbm90aWZpZWRfd2hlbl9zdWIiO2k6MTtzOjE2OiJ1bnN1YnNjcmliZV9wYWdlIjtzOjM6IjEyNiI7czoxNzoiY29uZmlybWF0aW9uX3BhZ2UiO3M6MzoiMTI2IjtzOjk6InNtdHBfaG9zdCI7czowOiIiO3M6MTA6InNtdHBfbG9naW4iO3M6MjE6InRmbG9yaWFuOTFAaG90bWFpbC5mciI7czoxMzoic210cF9wYXNzd29yZCI7czo3OiJzYWxpZjk0IjtzOjk6InNtdHBfcG9ydCI7czoyOiIyNSI7czoxMToic210cF9zZWN1cmUiO3M6MToiMCI7czoxMDoidGVzdF9tYWlscyI7czoyMToidGZsb3JpYW45MUBob3RtYWlsLmZyIjtzOjEyOiJib3VuY2VfZW1haWwiO3M6MjY6Im5ld3NsZXR0ZXJAdGlhci1mbG9yaWFuLmZyIjtzOjI3OiJtYW5hZ2Vfc3Vic2NyaXB0aW9uc19saXN0c1siO3M6MToiMSI7czoxODoic3Vic2NyaXB0aW9uc19wYWdlIjtzOjM6IjEyNiI7czoxMToiaHRtbF9zb3VyY2UiO3M6MToiMSI7czo4OiJpbmR1c3RyeSI7czo1OiJhdXRyZSI7czoxNjoiYXJjaGl2ZV9saW5rbmFtZSI7czoxNjoiW3d5c2lqYV9hcmNoaXZlXSI7czoyNjoic3Vic2NyaWJlcnNfY291bnRfbGlua25hbWUiO3M6MjY6Ilt3eXNpamFfc3Vic2NyaWJlcnNfY291bnRdIjtzOjg6Il93cG5vbmNlIjtzOjEwOiIzZjMyNmVlOTlhIjtzOjE2OiJfd3BfaHR0cF9yZWZlcmVyIjtzOjUxOiIvY29yYmVpbGFzc29jL3dwLWFkbWluL2FkbWluLnBocD9wYWdlPXd5c2lqYV9jb25maWciO3M6NjoiYWN0aW9uIjtzOjEyOiJ1cGRhdGVfb3JkZXIiO3M6MTE6InJlZGlyZWN0dGFiIjtzOjA6IiI7czo3OiJmb3JtX2lkIjtzOjA6IiI7czoxNzoic2VuZGluZ19lbWFpbHNfb2siO2I6MTtzOjI3OiJlbWFpbHNfbm90aWZpZWRfd2hlbl9ib3VuY2UiO2I6MDtzOjMzOiJlbWFpbHNfbm90aWZpZWRfd2hlbl9kYWlseXN1bW1hcnkiO2I6MDtzOjE5OiJib3VuY2VfcHJvY2Vzc19hdXRvIjtiOjA7czoyMjoibXNfYm91bmNlX3Byb2Nlc3NfYXV0byI7YjowO3M6OToic2hhcmVkYXRhIjtiOjA7czoxMToiZGtpbV9hY3RpdmUiO2I6MDtzOjk6InNtdHBfcmVzdCI7YjowO3M6MTI6Im1zX3NtdHBfcmVzdCI7YjowO3M6MTQ6ImRlYnVnX2xvZ19jcm9uIjtiOjA7czoyMDoiZGVidWdfbG9nX3Bvc3Rfbm90aWYiO2I6MDtzOjIyOiJkZWJ1Z19sb2dfcXVlcnlfZXJyb3JzIjtiOjA7czoyMzoiZGVidWdfbG9nX3F1ZXVlX3Byb2Nlc3MiO2I6MDtzOjE2OiJkZWJ1Z19sb2dfbWFudWFsIjtiOjA7czoyNjoibWFuYWdlX3N1YnNjcmlwdGlvbnNfbGlzdHMiO2E6MTp7aTowO3M6MToiMSI7fXM6MTM6ImFyY2hpdmVfbGlzdHMiO2E6MTp7aToxO2I6MDt9czozODoicm9sZXNjYXAtLS1hZG1pbmlzdHJhdG9yLS0tbmV3c2xldHRlcnMiO2I6MDtzOjMxOiJyb2xlc2NhcC0tLWVkaXRvci0tLW5ld3NsZXR0ZXJzIjtiOjA7czozMToicm9sZXNjYXAtLS1hdXRob3ItLS1uZXdzbGV0dGVycyI7YjowO3M6MzY6InJvbGVzY2FwLS0tY29udHJpYnV0b3ItLS1uZXdzbGV0dGVycyI7YjowO3M6MzU6InJvbGVzY2FwLS0tc3Vic2NyaWJlci0tLW5ld3NsZXR0ZXJzIjtiOjA7czoyOToicm9sZXNjYXAtLS1jb29sLS0tbmV3c2xldHRlcnMiO2I6MDtzOjM4OiJyb2xlc2NhcC0tLWFkbWluaXN0cmF0b3ItLS1zdWJzY3JpYmVycyI7YjowO3M6MzE6InJvbGVzY2FwLS0tZWRpdG9yLS0tc3Vic2NyaWJlcnMiO2I6MDtzOjMxOiJyb2xlc2NhcC0tLWF1dGhvci0tLXN1YnNjcmliZXJzIjtiOjA7czozNjoicm9sZXNjYXAtLS1jb250cmlidXRvci0tLXN1YnNjcmliZXJzIjtiOjA7czozNToicm9sZXNjYXAtLS1zdWJzY3JpYmVyLS0tc3Vic2NyaWJlcnMiO2I6MDtzOjI5OiJyb2xlc2NhcC0tLWNvb2wtLS1zdWJzY3JpYmVycyI7YjowO3M6MzM6InJvbGVzY2FwLS0tYWRtaW5pc3RyYXRvci0tLWNvbmZpZyI7YjowO3M6MjY6InJvbGVzY2FwLS0tZWRpdG9yLS0tY29uZmlnIjtiOjA7czoyNjoicm9sZXNjYXAtLS1hdXRob3ItLS1jb25maWciO2I6MDtzOjMxOiJyb2xlc2NhcC0tLWNvbnRyaWJ1dG9yLS0tY29uZmlnIjtiOjA7czozMDoicm9sZXNjYXAtLS1zdWJzY3JpYmVyLS0tY29uZmlnIjtiOjA7czoyNDoicm9sZXNjYXAtLS1jb29sLS0tY29uZmlnIjtiOjA7czozNjoicm9sZXNjYXAtLS1hZG1pbmlzdHJhdG9yLS0tdGhlbWVfdGFiIjtiOjA7czoyOToicm9sZXNjYXAtLS1lZGl0b3ItLS10aGVtZV90YWIiO2I6MDtzOjI5OiJyb2xlc2NhcC0tLWF1dGhvci0tLXRoZW1lX3RhYiI7YjowO3M6MzQ6InJvbGVzY2FwLS0tY29udHJpYnV0b3ItLS10aGVtZV90YWIiO2I6MDtzOjMzOiJyb2xlc2NhcC0tLXN1YnNjcmliZXItLS10aGVtZV90YWIiO2I6MDtzOjI3OiJyb2xlc2NhcC0tLWNvb2wtLS10aGVtZV90YWIiO2I6MDtzOjM2OiJyb2xlc2NhcC0tLWFkbWluaXN0cmF0b3ItLS1zdHlsZV90YWIiO2I6MDtzOjI5OiJyb2xlc2NhcC0tLWVkaXRvci0tLXN0eWxlX3RhYiI7YjowO3M6Mjk6InJvbGVzY2FwLS0tYXV0aG9yLS0tc3R5bGVfdGFiIjtiOjA7czozNDoicm9sZXNjYXAtLS1jb250cmlidXRvci0tLXN0eWxlX3RhYiI7YjowO3M6MzM6InJvbGVzY2FwLS0tc3Vic2NyaWJlci0tLXN0eWxlX3RhYiI7YjowO3M6Mjc6InJvbGVzY2FwLS0tY29vbC0tLXN0eWxlX3RhYiI7YjowO30=', 'yes'),
(1795, 'wysija_reinstall', '0', 'no'),
(1796, 'wysija_schedules', 'a:5:{s:5:"queue";a:3:{s:13:"next_schedule";i:1432981169;s:13:"prev_schedule";b:0;s:7:"running";b:0;}s:6:"bounce";a:3:{s:13:"next_schedule";i:1427032457;s:13:"prev_schedule";i:0;s:7:"running";b:0;}s:5:"daily";a:3:{s:13:"next_schedule";i:1434001495;s:13:"prev_schedule";b:0;s:7:"running";b:0;}s:6:"weekly";a:3:{s:13:"next_schedule";i:1434392660;s:13:"prev_schedule";b:0;s:7:"running";b:0;}s:7:"monthly";a:3:{s:13:"next_schedule";i:1435244805;s:13:"prev_schedule";b:0;s:7:"running";b:0;}}', 'yes'),
(1808, 'wysija_msg', '', 'no'),
(1809, 'wysija_queries', '', 'no'),
(1810, 'wysija_queries_errors', '', 'no'),
(1838, 'widget_wysija', 'a:2:{i:2;a:2:{s:5:"title";s:32:"Abonnez-vous à notre newsletter";s:4:"form";s:1:"1";}s:12:"_multiwidget";i:1;}', 'yes'),
(1852, 'widget_widget-corbeilinfos', 'a:2:{i:2;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(1905, 'wysija_check_pn', '1433915091.6752', 'yes'),
(1906, 'wysija_last_scheduled_check', '1433915092', 'yes'),
(1907, 'wysija_last_php_cron_call', '1433915087', 'yes'),
(2505, 'um_flush_rules', '1', 'yes'),
(2831, 'db_upgraded', '', 'yes'),
(2842, '_site_transient_timeout_browser_837ac6e56c8bf9987e4fbaa5079e156e', '1431079547', 'yes'),
(2843, '_site_transient_browser_837ac6e56c8bf9987e4fbaa5079e156e', 'a:9:{s:8:"platform";s:7:"Windows";s:4:"name";s:6:"Chrome";s:7:"version";s:12:"42.0.2311.90";s:10:"update_url";s:28:"http://www.google.com/chrome";s:7:"img_src";s:49:"http://s.wordpress.org/images/browsers/chrome.png";s:11:"img_src_ssl";s:48:"https://wordpress.org/images/browsers/chrome.png";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'yes'),
(2906, 'can_compress_scripts', '1', 'yes'),
(2915, '_site_transient_timeout_popular_importers_fr_FR', '1430653201', 'yes'),
(2916, '_site_transient_popular_importers_fr_FR', 'a:2:{s:9:"importers";a:8:{s:7:"blogger";a:4:{s:4:"name";s:7:"Blogger";s:11:"description";s:86:"Install the Blogger importer to import posts, comments, and users from a Blogger blog.";s:11:"plugin-slug";s:16:"blogger-importer";s:11:"importer-id";s:7:"blogger";}s:9:"wpcat2tag";a:4:{s:4:"name";s:29:"Categories and Tags Converter";s:11:"description";s:109:"Install the category/tag converter to convert existing categories to tags or tags to categories, selectively.";s:11:"plugin-slug";s:18:"wpcat2tag-importer";s:11:"importer-id";s:9:"wpcat2tag";}s:11:"livejournal";a:4:{s:4:"name";s:11:"LiveJournal";s:11:"description";s:82:"Install the LiveJournal importer to import posts from LiveJournal using their API.";s:11:"plugin-slug";s:20:"livejournal-importer";s:11:"importer-id";s:11:"livejournal";}s:11:"movabletype";a:4:{s:4:"name";s:24:"Movable Type and TypePad";s:11:"description";s:99:"Install the Movable Type importer to import posts and comments from a Movable Type or TypePad blog.";s:11:"plugin-slug";s:20:"movabletype-importer";s:11:"importer-id";s:2:"mt";}s:4:"opml";a:4:{s:4:"name";s:8:"Blogroll";s:11:"description";s:61:"Install the blogroll importer to import links in OPML format.";s:11:"plugin-slug";s:13:"opml-importer";s:11:"importer-id";s:4:"opml";}s:3:"rss";a:4:{s:4:"name";s:3:"RSS";s:11:"description";s:58:"Install the RSS importer to import posts from an RSS feed.";s:11:"plugin-slug";s:12:"rss-importer";s:11:"importer-id";s:3:"rss";}s:6:"tumblr";a:4:{s:4:"name";s:6:"Tumblr";s:11:"description";s:84:"Install the Tumblr importer to import posts &amp; media from Tumblr using their API.";s:11:"plugin-slug";s:15:"tumblr-importer";s:11:"importer-id";s:6:"tumblr";}s:9:"wordpress";a:4:{s:4:"name";s:9:"WordPress";s:11:"description";s:130:"Install the WordPress importer to import posts, pages, comments, custom fields, categories, and tags from a WordPress export file.";s:11:"plugin-slug";s:18:"wordpress-importer";s:11:"importer-id";s:9:"wordpress";}}s:10:"translated";b:0;}', 'yes'),
(2952, 'widget_calendar', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(2953, 'widget_eo-event-categories', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(2955, 'widget_eo-event-venues', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(2956, 'widget_nav_menu', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(2957, 'widget_tag_cloud', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(2958, 'widget_pages', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(2961, 'theme_mods_sequentialbis', 'a:3:{i:0;b:0;s:18:"nav_menu_locations";a:2:{s:7:"primary";i:3;s:6:"footer";i:3;}s:16:"sidebars_widgets";a:2:{s:4:"time";i:1430857222;s:4:"data";a:3:{s:19:"wp_inactive_widgets";a:3:{i:0;s:8:"wysija-2";i:1;s:21:"widget-corbeilinfos-2";i:2;s:22:"eo_event_list_widget-2";}s:9:"sidebar-1";a:3:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:6:"meta-2";}s:9:"sidebar-2";a:0:{}}}}', 'yes'),
(2962, 'theme_switched_via_customizer', '', 'yes'),
(3058, '_site_transient_timeout_available_translations', '1430684843', 'yes'),
(3059, '_site_transient_available_translations', 'a:55:{s:2:"ar";a:8:{s:8:"language";s:2:"ar";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-23 23:59:50";s:12:"english_name";s:6:"Arabic";s:11:"native_name";s:14:"العربية";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.1/ar.zip";s:3:"iso";a:2:{i:1;s:2:"ar";i:2;s:3:"ara";}s:7:"strings";a:1:{s:8:"continue";s:16:"المتابعة";}}s:2:"az";a:8:{s:8:"language";s:2:"az";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-05-02 03:57:09";s:12:"english_name";s:11:"Azerbaijani";s:11:"native_name";s:16:"Azərbaycan dili";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.1/az.zip";s:3:"iso";a:2:{i:1;s:2:"az";i:2;s:3:"aze";}s:7:"strings";a:1:{s:8:"continue";s:5:"Davam";}}s:5:"bg_BG";a:8:{s:8:"language";s:5:"bg_BG";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-23 15:23:08";s:12:"english_name";s:9:"Bulgarian";s:11:"native_name";s:18:"Български";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.1/bg_BG.zip";s:3:"iso";a:2:{i:1;s:2:"bg";i:2;s:3:"bul";}s:7:"strings";a:1:{s:8:"continue";s:22:"Продължение";}}s:5:"bs_BA";a:8:{s:8:"language";s:5:"bs_BA";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-25 18:55:51";s:12:"english_name";s:7:"Bosnian";s:11:"native_name";s:8:"Bosanski";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.1/bs_BA.zip";s:3:"iso";a:2:{i:1;s:2:"bs";i:2;s:3:"bos";}s:7:"strings";a:1:{s:8:"continue";s:7:"Nastavi";}}s:2:"ca";a:8:{s:8:"language";s:2:"ca";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-27 03:15:54";s:12:"english_name";s:7:"Catalan";s:11:"native_name";s:7:"Català";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.1/ca.zip";s:3:"iso";a:2:{i:1;s:2:"ca";i:2;s:3:"cat";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continua";}}s:2:"cy";a:8:{s:8:"language";s:2:"cy";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-23 15:23:08";s:12:"english_name";s:5:"Welsh";s:11:"native_name";s:7:"Cymraeg";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.1/cy.zip";s:3:"iso";a:2:{i:1;s:2:"cy";i:2;s:3:"cym";}s:7:"strings";a:1:{s:8:"continue";s:6:"Parhau";}}s:5:"da_DK";a:8:{s:8:"language";s:5:"da_DK";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-23 15:23:09";s:12:"english_name";s:6:"Danish";s:11:"native_name";s:5:"Dansk";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.1/da_DK.zip";s:3:"iso";a:2:{i:1;s:2:"da";i:2;s:3:"dan";}s:7:"strings";a:1:{s:8:"continue";s:12:"Forts&#230;t";}}s:5:"de_CH";a:8:{s:8:"language";s:5:"de_CH";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-23 15:23:08";s:12:"english_name";s:20:"German (Switzerland)";s:11:"native_name";s:17:"Deutsch (Schweiz)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.1/de_CH.zip";s:3:"iso";a:1:{i:1;s:2:"de";}s:7:"strings";a:1:{s:8:"continue";s:10:"Fortfahren";}}s:5:"de_DE";a:8:{s:8:"language";s:5:"de_DE";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-05-02 22:25:58";s:12:"english_name";s:6:"German";s:11:"native_name";s:7:"Deutsch";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.1/de_DE.zip";s:3:"iso";a:1:{i:1;s:2:"de";}s:7:"strings";a:1:{s:8:"continue";s:10:"Fortfahren";}}s:2:"el";a:8:{s:8:"language";s:2:"el";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-05-02 07:47:28";s:12:"english_name";s:5:"Greek";s:11:"native_name";s:16:"Ελληνικά";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.1/el.zip";s:3:"iso";a:2:{i:1;s:2:"el";i:2;s:3:"ell";}s:7:"strings";a:1:{s:8:"continue";s:16:"Συνέχεια";}}s:5:"en_AU";a:8:{s:8:"language";s:5:"en_AU";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-23 15:23:09";s:12:"english_name";s:19:"English (Australia)";s:11:"native_name";s:19:"English (Australia)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.1/en_AU.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:5:"en_CA";a:8:{s:8:"language";s:5:"en_CA";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-23 15:23:08";s:12:"english_name";s:16:"English (Canada)";s:11:"native_name";s:16:"English (Canada)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.1/en_CA.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:5:"en_GB";a:8:{s:8:"language";s:5:"en_GB";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-23 15:23:09";s:12:"english_name";s:12:"English (UK)";s:11:"native_name";s:12:"English (UK)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.1/en_GB.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:2:"eo";a:8:{s:8:"language";s:2:"eo";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-23 15:23:09";s:12:"english_name";s:9:"Esperanto";s:11:"native_name";s:9:"Esperanto";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.1/eo.zip";s:3:"iso";a:2:{i:1;s:2:"eo";i:2;s:3:"epo";}s:7:"strings";a:1:{s:8:"continue";s:8:"Daŭrigi";}}s:5:"es_PE";a:8:{s:8:"language";s:5:"es_PE";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-25 13:39:01";s:12:"english_name";s:14:"Spanish (Peru)";s:11:"native_name";s:17:"Español de Perú";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.1/es_PE.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_MX";a:8:{s:8:"language";s:5:"es_MX";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-24 16:03:38";s:12:"english_name";s:16:"Spanish (Mexico)";s:11:"native_name";s:19:"Español de México";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.1/es_MX.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_ES";a:8:{s:8:"language";s:5:"es_ES";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-28 16:15:15";s:12:"english_name";s:15:"Spanish (Spain)";s:11:"native_name";s:8:"Español";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.1/es_ES.zip";s:3:"iso";a:1:{i:1;s:2:"es";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_CL";a:8:{s:8:"language";s:5:"es_CL";s:7:"version";s:3:"4.0";s:7:"updated";s:19:"2014-09-04 19:47:01";s:12:"english_name";s:15:"Spanish (Chile)";s:11:"native_name";s:17:"Español de Chile";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.0/es_CL.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:2:"eu";a:8:{s:8:"language";s:2:"eu";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-23 15:23:09";s:12:"english_name";s:6:"Basque";s:11:"native_name";s:7:"Euskara";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.1/eu.zip";s:3:"iso";a:2:{i:1;s:2:"eu";i:2;s:3:"eus";}s:7:"strings";a:1:{s:8:"continue";s:8:"Jarraitu";}}s:5:"fa_IR";a:8:{s:8:"language";s:5:"fa_IR";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-24 12:47:01";s:12:"english_name";s:7:"Persian";s:11:"native_name";s:10:"فارسی";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.1/fa_IR.zip";s:3:"iso";a:2:{i:1;s:2:"fa";i:2;s:3:"fas";}s:7:"strings";a:1:{s:8:"continue";s:10:"ادامه";}}s:2:"fi";a:8:{s:8:"language";s:2:"fi";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-23 15:23:09";s:12:"english_name";s:7:"Finnish";s:11:"native_name";s:5:"Suomi";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.1/fi.zip";s:3:"iso";a:2:{i:1;s:2:"fi";i:2;s:3:"fin";}s:7:"strings";a:1:{s:8:"continue";s:5:"Jatka";}}s:5:"fr_FR";a:8:{s:8:"language";s:5:"fr_FR";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-29 17:08:38";s:12:"english_name";s:15:"French (France)";s:11:"native_name";s:9:"Français";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.1/fr_FR.zip";s:3:"iso";a:1:{i:1;s:2:"fr";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuer";}}s:2:"gd";a:8:{s:8:"language";s:2:"gd";s:7:"version";s:3:"4.0";s:7:"updated";s:19:"2014-09-05 17:37:43";s:12:"english_name";s:15:"Scottish Gaelic";s:11:"native_name";s:9:"Gàidhlig";s:7:"package";s:59:"https://downloads.wordpress.org/translation/core/4.0/gd.zip";s:3:"iso";a:3:{i:1;s:2:"gd";i:2;s:3:"gla";i:3;s:3:"gla";}s:7:"strings";a:1:{s:8:"continue";s:15:"Lean air adhart";}}s:5:"gl_ES";a:8:{s:8:"language";s:5:"gl_ES";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-23 15:23:08";s:12:"english_name";s:8:"Galician";s:11:"native_name";s:6:"Galego";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.1/gl_ES.zip";s:3:"iso";a:2:{i:1;s:2:"gl";i:2;s:3:"glg";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:3:"haz";a:8:{s:8:"language";s:3:"haz";s:7:"version";s:5:"4.1.3";s:7:"updated";s:19:"2015-03-26 15:20:27";s:12:"english_name";s:8:"Hazaragi";s:11:"native_name";s:15:"هزاره گی";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.1.3/haz.zip";s:3:"iso";a:1:{i:2;s:3:"haz";}s:7:"strings";a:1:{s:8:"continue";s:10:"ادامه";}}s:5:"he_IL";a:8:{s:8:"language";s:5:"he_IL";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-23 15:23:09";s:12:"english_name";s:6:"Hebrew";s:11:"native_name";s:16:"עִבְרִית";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.1/he_IL.zip";s:3:"iso";a:1:{i:1;s:2:"he";}s:7:"strings";a:1:{s:8:"continue";s:12:"להמשיך";}}s:2:"hr";a:8:{s:8:"language";s:2:"hr";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-26 14:13:46";s:12:"english_name";s:8:"Croatian";s:11:"native_name";s:8:"Hrvatski";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.1/hr.zip";s:3:"iso";a:2:{i:1;s:2:"hr";i:2;s:3:"hrv";}s:7:"strings";a:1:{s:8:"continue";s:7:"Nastavi";}}s:5:"hu_HU";a:8:{s:8:"language";s:5:"hu_HU";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-23 21:33:36";s:12:"english_name";s:9:"Hungarian";s:11:"native_name";s:6:"Magyar";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.1/hu_HU.zip";s:3:"iso";a:2:{i:1;s:2:"hu";i:2;s:3:"hun";}s:7:"strings";a:1:{s:8:"continue";s:7:"Tovább";}}s:5:"id_ID";a:8:{s:8:"language";s:5:"id_ID";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-23 15:23:08";s:12:"english_name";s:10:"Indonesian";s:11:"native_name";s:16:"Bahasa Indonesia";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.1/id_ID.zip";s:3:"iso";a:2:{i:1;s:2:"id";i:2;s:3:"ind";}s:7:"strings";a:1:{s:8:"continue";s:9:"Lanjutkan";}}s:5:"is_IS";a:8:{s:8:"language";s:5:"is_IS";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-30 11:54:24";s:12:"english_name";s:9:"Icelandic";s:11:"native_name";s:9:"Íslenska";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.1/is_IS.zip";s:3:"iso";a:2:{i:1;s:2:"is";i:2;s:3:"isl";}s:7:"strings";a:1:{s:8:"continue";s:6:"Áfram";}}s:5:"it_IT";a:8:{s:8:"language";s:5:"it_IT";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-05-02 10:34:18";s:12:"english_name";s:7:"Italian";s:11:"native_name";s:8:"Italiano";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.1/it_IT.zip";s:3:"iso";a:2:{i:1;s:2:"it";i:2;s:3:"ita";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continua";}}s:2:"ja";a:8:{s:8:"language";s:2:"ja";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-30 06:14:18";s:12:"english_name";s:8:"Japanese";s:11:"native_name";s:9:"日本語";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.1/ja.zip";s:3:"iso";a:1:{i:1;s:2:"ja";}s:7:"strings";a:1:{s:8:"continue";s:9:"続ける";}}s:5:"ko_KR";a:8:{s:8:"language";s:5:"ko_KR";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-23 22:23:52";s:12:"english_name";s:6:"Korean";s:11:"native_name";s:9:"한국어";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.1/ko_KR.zip";s:3:"iso";a:2:{i:1;s:2:"ko";i:2;s:3:"kor";}s:7:"strings";a:1:{s:8:"continue";s:6:"계속";}}s:5:"lt_LT";a:8:{s:8:"language";s:5:"lt_LT";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-23 15:23:08";s:12:"english_name";s:10:"Lithuanian";s:11:"native_name";s:15:"Lietuvių kalba";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.1/lt_LT.zip";s:3:"iso";a:2:{i:1;s:2:"lt";i:2;s:3:"lit";}s:7:"strings";a:1:{s:8:"continue";s:6:"Tęsti";}}s:5:"my_MM";a:8:{s:8:"language";s:5:"my_MM";s:7:"version";s:5:"4.1.3";s:7:"updated";s:19:"2015-03-26 15:57:42";s:12:"english_name";s:17:"Myanmar (Burmese)";s:11:"native_name";s:15:"ဗမာစာ";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.1.3/my_MM.zip";s:3:"iso";a:2:{i:1;s:2:"my";i:2;s:3:"mya";}s:7:"strings";a:1:{s:8:"continue";s:54:"ဆက်လက်လုပ်ေဆာင်ပါ။";}}s:5:"nb_NO";a:8:{s:8:"language";s:5:"nb_NO";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-27 20:16:30";s:12:"english_name";s:19:"Norwegian (Bokmål)";s:11:"native_name";s:13:"Norsk bokmål";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.1/nb_NO.zip";s:3:"iso";a:2:{i:1;s:2:"nb";i:2;s:3:"nob";}s:7:"strings";a:1:{s:8:"continue";s:8:"Fortsett";}}s:5:"nl_NL";a:8:{s:8:"language";s:5:"nl_NL";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-28 10:26:32";s:12:"english_name";s:5:"Dutch";s:11:"native_name";s:10:"Nederlands";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.1/nl_NL.zip";s:3:"iso";a:2:{i:1;s:2:"nl";i:2;s:3:"nld";}s:7:"strings";a:1:{s:8:"continue";s:8:"Doorgaan";}}s:5:"nn_NO";a:8:{s:8:"language";s:5:"nn_NO";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-23 15:23:09";s:12:"english_name";s:19:"Norwegian (Nynorsk)";s:11:"native_name";s:13:"Norsk nynorsk";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.1/nn_NO.zip";s:3:"iso";a:2:{i:1;s:2:"nn";i:2;s:3:"nno";}s:7:"strings";a:1:{s:8:"continue";s:9:"Hald fram";}}s:5:"pl_PL";a:8:{s:8:"language";s:5:"pl_PL";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-05-01 13:58:25";s:12:"english_name";s:6:"Polish";s:11:"native_name";s:6:"Polski";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.1/pl_PL.zip";s:3:"iso";a:2:{i:1;s:2:"pl";i:2;s:3:"pol";}s:7:"strings";a:1:{s:8:"continue";s:9:"Kontynuuj";}}s:2:"ps";a:8:{s:8:"language";s:2:"ps";s:7:"version";s:5:"4.1.3";s:7:"updated";s:19:"2015-03-29 22:19:48";s:12:"english_name";s:6:"Pashto";s:11:"native_name";s:8:"پښتو";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.1.3/ps.zip";s:3:"iso";a:1:{i:1;s:2:"ps";}s:7:"strings";a:1:{s:8:"continue";s:8:"دوام";}}s:5:"pt_BR";a:8:{s:8:"language";s:5:"pt_BR";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-27 14:23:25";s:12:"english_name";s:19:"Portuguese (Brazil)";s:11:"native_name";s:20:"Português do Brasil";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.1/pt_BR.zip";s:3:"iso";a:2:{i:1;s:2:"pt";i:2;s:3:"por";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"pt_PT";a:8:{s:8:"language";s:5:"pt_PT";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-27 09:25:14";s:12:"english_name";s:21:"Portuguese (Portugal)";s:11:"native_name";s:10:"Português";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.1/pt_PT.zip";s:3:"iso";a:1:{i:1;s:2:"pt";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"ro_RO";a:8:{s:8:"language";s:5:"ro_RO";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-05-01 09:18:04";s:12:"english_name";s:8:"Romanian";s:11:"native_name";s:8:"Română";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.1/ro_RO.zip";s:3:"iso";a:2:{i:1;s:2:"ro";i:2;s:3:"ron";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuă";}}s:5:"ru_RU";a:8:{s:8:"language";s:5:"ru_RU";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-23 15:23:08";s:12:"english_name";s:7:"Russian";s:11:"native_name";s:14:"Русский";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.1/ru_RU.zip";s:3:"iso";a:2:{i:1;s:2:"ru";i:2;s:3:"rus";}s:7:"strings";a:1:{s:8:"continue";s:20:"Продолжить";}}s:5:"sk_SK";a:8:{s:8:"language";s:5:"sk_SK";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-23 15:23:08";s:12:"english_name";s:6:"Slovak";s:11:"native_name";s:11:"Slovenčina";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.1/sk_SK.zip";s:3:"iso";a:2:{i:1;s:2:"sk";i:2;s:3:"slk";}s:7:"strings";a:1:{s:8:"continue";s:12:"Pokračovať";}}s:5:"sl_SI";a:8:{s:8:"language";s:5:"sl_SI";s:7:"version";s:5:"4.1.3";s:7:"updated";s:19:"2015-03-26 16:25:46";s:12:"english_name";s:9:"Slovenian";s:11:"native_name";s:13:"Slovenščina";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.1.3/sl_SI.zip";s:3:"iso";a:2:{i:1;s:2:"sl";i:2;s:3:"slv";}s:7:"strings";a:1:{s:8:"continue";s:10:"Nadaljujte";}}s:2:"sq";a:8:{s:8:"language";s:2:"sq";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-30 20:40:26";s:12:"english_name";s:8:"Albanian";s:11:"native_name";s:5:"Shqip";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.1/sq.zip";s:3:"iso";a:2:{i:1;s:2:"sq";i:2;s:3:"sqi";}s:7:"strings";a:1:{s:8:"continue";s:6:"Vazhdo";}}s:5:"sr_RS";a:8:{s:8:"language";s:5:"sr_RS";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-23 16:55:54";s:12:"english_name";s:7:"Serbian";s:11:"native_name";s:23:"Српски језик";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.1/sr_RS.zip";s:3:"iso";a:2:{i:1;s:2:"sr";i:2;s:3:"srp";}s:7:"strings";a:1:{s:8:"continue";s:14:"Настави";}}s:5:"sv_SE";a:8:{s:8:"language";s:5:"sv_SE";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-23 15:23:09";s:12:"english_name";s:7:"Swedish";s:11:"native_name";s:7:"Svenska";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.1/sv_SE.zip";s:3:"iso";a:2:{i:1;s:2:"sv";i:2;s:3:"swe";}s:7:"strings";a:1:{s:8:"continue";s:9:"Fortsätt";}}s:2:"th";a:8:{s:8:"language";s:2:"th";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-29 07:03:07";s:12:"english_name";s:4:"Thai";s:11:"native_name";s:9:"ไทย";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.1/th.zip";s:3:"iso";a:2:{i:1;s:2:"th";i:2;s:3:"tha";}s:7:"strings";a:1:{s:8:"continue";s:15:"ต่อไป";}}s:5:"tr_TR";a:8:{s:8:"language";s:5:"tr_TR";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-23 15:23:08";s:12:"english_name";s:7:"Turkish";s:11:"native_name";s:8:"Türkçe";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.1/tr_TR.zip";s:3:"iso";a:2:{i:1;s:2:"tr";i:2;s:3:"tur";}s:7:"strings";a:1:{s:8:"continue";s:5:"Devam";}}s:5:"ug_CN";a:8:{s:8:"language";s:5:"ug_CN";s:7:"version";s:5:"4.1.3";s:7:"updated";s:19:"2015-03-26 16:45:38";s:12:"english_name";s:6:"Uighur";s:11:"native_name";s:9:"Uyƣurqə";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.1.3/ug_CN.zip";s:3:"iso";a:2:{i:1;s:2:"ug";i:2;s:3:"uig";}s:7:"strings";a:1:{s:8:"continue";s:26:"داۋاملاشتۇرۇش";}}s:2:"uk";a:8:{s:8:"language";s:2:"uk";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-28 16:44:31";s:12:"english_name";s:9:"Ukrainian";s:11:"native_name";s:20:"Українська";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.2.1/uk.zip";s:3:"iso";a:2:{i:1;s:2:"uk";i:2;s:3:"ukr";}s:7:"strings";a:1:{s:8:"continue";s:20:"Продовжити";}}s:5:"zh_CN";a:8:{s:8:"language";s:5:"zh_CN";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-23 15:23:08";s:12:"english_name";s:15:"Chinese (China)";s:11:"native_name";s:12:"简体中文";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.1/zh_CN.zip";s:3:"iso";a:2:{i:1;s:2:"zh";i:2;s:3:"zho";}s:7:"strings";a:1:{s:8:"continue";s:6:"继续";}}s:5:"zh_TW";a:8:{s:8:"language";s:5:"zh_TW";s:7:"version";s:5:"4.2.1";s:7:"updated";s:19:"2015-04-29 06:37:03";s:12:"english_name";s:16:"Chinese (Taiwan)";s:11:"native_name";s:12:"繁體中文";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.2.1/zh_TW.zip";s:3:"iso";a:2:{i:1;s:2:"zh";i:2;s:3:"zho";}s:7:"strings";a:1:{s:8:"continue";s:6:"繼續";}}}', 'yes'),
(3085, '_transient_timeout_eventorganiser_add_ons', '1430761205', 'no'),
(3086, '_transient_eventorganiser_add_ons', '[\n    {\n        "title": "Event Organiser Pro",\n        "thumbnail": "http://dixcquypdhb9e.cloudfront.net/wp-content/uploads/edd/2013/08/pro-250x250.png",\n        "url": "http://wp-event-organiser.com/pricing",\n        "description": "Event booking & registration add-on, with additional shortcodes, and support for advanced event-venue queries.Event booking & registration add-on, with additional shortcodes, and support for advanced event-venue queries.",\n	"status":"available",\n	"price": false\n    },\n    {\n        "title": "Event Organiser Frontend Submissions",\n        "thumbnail": "http://dixcquypdhb9e.cloudfront.net/wp-content/uploads/2014/07/fes-thumbnail-250x250.png",\n        "url": "http://wp-event-organiser.com/extensions/event-organiser-frontend-submissions/",\n        "description": "Easily create forms to allow users to submit events from the frontend. Purchase this extension or get it <strong>free</strong> when you purchase Event Organiser Pro (Developer license)",\n	"status":"available",\n	"price": false\n    },\n    {\n        "title": "Discount Codes",\n        "thumbnail": "http://dixcquypdhb9e.cloudfront.net/wp-content/uploads/edd/2013/12/discount-code-thumbnail.png",\n        "url": "http://wp-event-organiser.com/extensions/event-organiser-discount-codes/",\n        "description": "Create fixed amount or percentage based discount codes for your tickets. Purchase this extension or get it <strong>free</strong> when you purchase Event Organiser Pro (Business or Developer license)",\n	"status":"available",\n	"price": false\n    },\n    {\n        "title": "Event Board",\n        "thumbnail": "http://dixcquypdhb9e.cloudfront.net/wp-content/uploads/2013/08/event-board.png",\n        "url": "http://wordpress.org/plugins/event-organiser-posterboard/",\n	"price": "free",\n        "description": "(<strong>Free</strong>) Display events on a responsive posterboard.",\n	"status":"available"\n    },\n    {\n        "title": "Stripe Payment Gateway",\n        "thumbnail": "http://dixcquypdhb9e.cloudfront.net/wp-content/uploads/2013/08/stripe.png",\n        "url": "http://wp-event-organiser.com/extensions/event-organiser-stripe/",\n        "description": "Accept credit card payments on your site for bookings via Stripe. This add-on requires Pro. Purchase this extension or get it <strong>free</strong> when you purchase Event Organiser Pro (Developer license)",\n	"status":"available",\n	"price": false\n    },\n    {\n        "title": "VAT",\n        "thumbnail": "http://dixcquypdhb9e.cloudfront.net/wp-content/uploads/2014/01/eo-vat.png",\n        "url": "http://wordpress.org/plugins/event-organiser-vat",\n        "description": "(<strong>Free</strong>) Adds VAT to booking checkout. Requires Event Organiser & Event Organiser Pro.",\n	"status":"available",\n	"price": "free"\n    },\n    {\n        "title": "CSV Import / Export",\n        "thumbnail": "http://dixcquypdhb9e.cloudfront.net/wp-content/uploads/2014/01/eo-csv.png",\n        "url": "http://wordpress.org/plugins/event-organiser-csv",\n        "description": "(<strong>Free</strong>) This plug-in allows you to import events from a CSV file into Event Organiser. You can also export events from Event Organiser into a CSV file.",\n	"status":"available",\n	"price": "free"\n    },\n    {\n        "title": "Theme: Sigma",\n        "thumbnail": "http://dixcquypdhb9e.cloudfront.net/wp-content/uploads/edd/2014/01/sigma-thumbnail.png",\n        "url": "http://wp-event-organiser.com/extensions/sigma-theme/",\n        "description": "(<strong>Free</strong>) A lightweight and responsive multi-purpose theme which is ''Event Organiser ready''.",\n	"status":"available",\n	"price": "free"\n    },\n    {\n        "title": "Venue Map Markers",\n        "thumbnail": "http://dixcquypdhb9e.cloudfront.net/wp-content/uploads/2013/07/venue-map-markers.png",\n        "url": "http://wp-event-organiser.com/extensions/event-organiser-venue-markers/",\n        "description": "Assign venues custom markers for Google maps. Choose from over 150 provided markers, or upload your own.",\n	"status":"available",\n	"price": false\n    },\n    {\n        "title": "iCal Sync",\n        "thumbnail": "http://dixcquypdhb9e.cloudfront.net/wp-content/uploads/edd/2013/07/ical-sync.png",\n        "url": "http://wp-event-organiser.com/extensions/event-organiser-ical-sync/",\n        "description": "Automatically import events from an iCal feed. Events will be automatically updated to reflect changes in the feed. This add-on allows you to manage your events from Google.",\n	"status":"available",\n	"price": false\n    }\n]\n', 'no'),
(3094, 'eo-event-category_5', 'a:1:{s:6:"colour";s:0:"";}', 'yes'),
(3099, 'category_children', 'a:0:{}', 'yes'),
(3108, '_site_transient_timeout_poptags_40cd750bba9870f18aada2478b24840a', '1430688893', 'yes');
INSERT INTO `crbass_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(3109, '_site_transient_poptags_40cd750bba9870f18aada2478b24840a', 'a:40:{s:6:"widget";a:3:{s:4:"name";s:6:"widget";s:4:"slug";s:6:"widget";s:5:"count";s:4:"5152";}s:4:"post";a:3:{s:4:"name";s:4:"Post";s:4:"slug";s:4:"post";s:5:"count";s:4:"3225";}s:6:"plugin";a:3:{s:4:"name";s:6:"plugin";s:4:"slug";s:6:"plugin";s:5:"count";s:4:"3156";}s:5:"admin";a:3:{s:4:"name";s:5:"admin";s:4:"slug";s:5:"admin";s:5:"count";s:4:"2683";}s:5:"posts";a:3:{s:4:"name";s:5:"posts";s:4:"slug";s:5:"posts";s:5:"count";s:4:"2467";}s:7:"sidebar";a:3:{s:4:"name";s:7:"sidebar";s:4:"slug";s:7:"sidebar";s:5:"count";s:4:"1979";}s:9:"shortcode";a:3:{s:4:"name";s:9:"shortcode";s:4:"slug";s:9:"shortcode";s:5:"count";s:4:"1858";}s:6:"google";a:3:{s:4:"name";s:6:"google";s:4:"slug";s:6:"google";s:5:"count";s:4:"1818";}s:7:"twitter";a:3:{s:4:"name";s:7:"twitter";s:4:"slug";s:7:"twitter";s:5:"count";s:4:"1763";}s:6:"images";a:3:{s:4:"name";s:6:"images";s:4:"slug";s:6:"images";s:5:"count";s:4:"1743";}s:4:"page";a:3:{s:4:"name";s:4:"page";s:4:"slug";s:4:"page";s:5:"count";s:4:"1705";}s:8:"comments";a:3:{s:4:"name";s:8:"comments";s:4:"slug";s:8:"comments";s:5:"count";s:4:"1699";}s:5:"image";a:3:{s:4:"name";s:5:"image";s:4:"slug";s:5:"image";s:5:"count";s:4:"1589";}s:8:"facebook";a:3:{s:4:"name";s:8:"Facebook";s:4:"slug";s:8:"facebook";s:5:"count";s:4:"1395";}s:3:"seo";a:3:{s:4:"name";s:3:"seo";s:4:"slug";s:3:"seo";s:5:"count";s:4:"1345";}s:9:"wordpress";a:3:{s:4:"name";s:9:"wordpress";s:4:"slug";s:9:"wordpress";s:5:"count";s:4:"1271";}s:5:"links";a:3:{s:4:"name";s:5:"links";s:4:"slug";s:5:"links";s:5:"count";s:4:"1199";}s:6:"social";a:3:{s:4:"name";s:6:"social";s:4:"slug";s:6:"social";s:5:"count";s:4:"1147";}s:7:"gallery";a:3:{s:4:"name";s:7:"gallery";s:4:"slug";s:7:"gallery";s:5:"count";s:4:"1129";}s:5:"email";a:3:{s:4:"name";s:5:"email";s:4:"slug";s:5:"email";s:5:"count";s:3:"993";}s:7:"widgets";a:3:{s:4:"name";s:7:"widgets";s:4:"slug";s:7:"widgets";s:5:"count";s:3:"965";}s:5:"pages";a:3:{s:4:"name";s:5:"pages";s:4:"slug";s:5:"pages";s:5:"count";s:3:"917";}s:11:"woocommerce";a:3:{s:4:"name";s:11:"woocommerce";s:4:"slug";s:11:"woocommerce";s:5:"count";s:3:"884";}s:6:"jquery";a:3:{s:4:"name";s:6:"jquery";s:4:"slug";s:6:"jquery";s:5:"count";s:3:"877";}s:3:"rss";a:3:{s:4:"name";s:3:"rss";s:4:"slug";s:3:"rss";s:5:"count";s:3:"855";}s:5:"media";a:3:{s:4:"name";s:5:"media";s:4:"slug";s:5:"media";s:5:"count";s:3:"839";}s:5:"video";a:3:{s:4:"name";s:5:"video";s:4:"slug";s:5:"video";s:5:"count";s:3:"798";}s:4:"ajax";a:3:{s:4:"name";s:4:"AJAX";s:4:"slug";s:4:"ajax";s:5:"count";s:3:"774";}s:7:"content";a:3:{s:4:"name";s:7:"content";s:4:"slug";s:7:"content";s:5:"count";s:3:"747";}s:10:"javascript";a:3:{s:4:"name";s:10:"javascript";s:4:"slug";s:10:"javascript";s:5:"count";s:3:"728";}s:5:"login";a:3:{s:4:"name";s:5:"login";s:4:"slug";s:5:"login";s:5:"count";s:3:"721";}s:9:"ecommerce";a:3:{s:4:"name";s:9:"ecommerce";s:4:"slug";s:9:"ecommerce";s:5:"count";s:3:"715";}s:10:"buddypress";a:3:{s:4:"name";s:10:"buddypress";s:4:"slug";s:10:"buddypress";s:5:"count";s:3:"684";}s:5:"photo";a:3:{s:4:"name";s:5:"photo";s:4:"slug";s:5:"photo";s:5:"count";s:3:"676";}s:4:"feed";a:3:{s:4:"name";s:4:"feed";s:4:"slug";s:4:"feed";s:5:"count";s:3:"672";}s:4:"link";a:3:{s:4:"name";s:4:"link";s:4:"slug";s:4:"link";s:5:"count";s:3:"662";}s:7:"youtube";a:3:{s:4:"name";s:7:"youtube";s:4:"slug";s:7:"youtube";s:5:"count";s:3:"638";}s:6:"photos";a:3:{s:4:"name";s:6:"photos";s:4:"slug";s:6:"photos";s:5:"count";s:3:"628";}s:4:"spam";a:3:{s:4:"name";s:4:"spam";s:4:"slug";s:4:"spam";s:5:"count";s:3:"627";}s:8:"security";a:3:{s:4:"name";s:8:"security";s:4:"slug";s:8:"security";s:5:"count";s:3:"627";}}', 'yes'),
(3215, 'theme_mods_sequential_old', 'a:2:{i:0;b:0;s:16:"sidebars_widgets";a:2:{s:4:"time";i:1430857360;s:4:"data";a:3:{s:19:"wp_inactive_widgets";a:3:{i:0;s:8:"wysija-2";i:1;s:21:"widget-corbeilinfos-2";i:2;s:22:"eo_event_list_widget-2";}s:9:"sidebar-1";a:3:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:6:"meta-2";}s:9:"sidebar-2";N;}}}', 'yes'),
(3218, '_site_transient_timeout_browser_694cb9383723bf110978ee4fd654ef7a', '1431463302', 'yes'),
(3219, '_site_transient_browser_694cb9383723bf110978ee4fd654ef7a', 'a:9:{s:8:"platform";s:7:"Windows";s:4:"name";s:6:"Chrome";s:7:"version";s:12:"42.0.2311.90";s:10:"update_url";s:28:"http://www.google.com/chrome";s:7:"img_src";s:49:"http://s.wordpress.org/images/browsers/chrome.png";s:11:"img_src_ssl";s:48:"https://wordpress.org/images/browsers/chrome.png";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'yes'),
(3248, '_site_transient_update_core', 'O:8:"stdClass":4:{s:7:"updates";a:3:{i:0;O:8:"stdClass":10:{s:8:"response";s:7:"upgrade";s:8:"download";s:65:"https://downloads.wordpress.org/release/fr_FR/wordpress-4.2.2.zip";s:6:"locale";s:5:"fr_FR";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:65:"https://downloads.wordpress.org/release/fr_FR/wordpress-4.2.2.zip";s:10:"no_content";b:0;s:11:"new_bundled";b:0;s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:5:"4.2.2";s:7:"version";s:5:"4.2.2";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.1";s:15:"partial_version";s:0:"";}i:1;O:8:"stdClass":10:{s:8:"response";s:7:"upgrade";s:8:"download";s:59:"https://downloads.wordpress.org/release/wordpress-4.2.2.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:59:"https://downloads.wordpress.org/release/wordpress-4.2.2.zip";s:10:"no_content";s:70:"https://downloads.wordpress.org/release/wordpress-4.2.2-no-content.zip";s:11:"new_bundled";s:71:"https://downloads.wordpress.org/release/wordpress-4.2.2-new-bundled.zip";s:7:"partial";s:69:"https://downloads.wordpress.org/release/wordpress-4.2.2-partial-1.zip";s:8:"rollback";b:0;}s:7:"current";s:5:"4.2.2";s:7:"version";s:5:"4.2.2";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.1";s:15:"partial_version";s:5:"4.2.1";}i:2;O:8:"stdClass":12:{s:8:"response";s:10:"autoupdate";s:8:"download";s:65:"https://downloads.wordpress.org/release/fr_FR/wordpress-4.2.2.zip";s:6:"locale";s:5:"fr_FR";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:65:"https://downloads.wordpress.org/release/fr_FR/wordpress-4.2.2.zip";s:10:"no_content";b:0;s:11:"new_bundled";b:0;s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:5:"4.2.2";s:7:"version";s:5:"4.2.2";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.1";s:15:"partial_version";s:0:"";s:12:"notify_email";s:1:"1";s:9:"new_files";s:1:"1";}}s:12:"last_checked";i:1433915147;s:15:"version_checked";s:5:"4.2.1";s:12:"translations";a:0:{}}', 'yes'),
(3269, '_site_transient_timeout_browser_a344c581ee2eff9c9c4fcdf9997f17a9', '1431782031', 'yes'),
(3270, '_site_transient_browser_a344c581ee2eff9c9c4fcdf9997f17a9', 'a:9:{s:8:"platform";s:7:"Windows";s:4:"name";s:6:"Chrome";s:7:"version";s:13:"42.0.2311.135";s:10:"update_url";s:28:"http://www.google.com/chrome";s:7:"img_src";s:49:"http://s.wordpress.org/images/browsers/chrome.png";s:11:"img_src_ssl";s:48:"https://wordpress.org/images/browsers/chrome.png";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'yes'),
(3298, 'auto_core_update_failed', 'a:6:{s:9:"attempted";s:5:"4.2.2";s:7:"current";s:5:"4.2.1";s:10:"error_code";s:15:"download_failed";s:10:"error_data";s:47:"Could not resolve host: downloads.wordpress.org";s:9:"timestamp";i:1432878702;s:5:"retry";b:1;}', 'yes'),
(3311, 'auto_updater.lock', '1433915145', 'no'),
(3321, 'event-category_children', 'a:0:{}', 'yes'),
(3328, '_transient_eo_is_multi_event_organiser', '0', 'yes'),
(3341, 'theme_mods_sequential-child', 'a:2:{i:0;b:0;s:18:"nav_menu_locations";a:2:{s:7:"primary";i:3;s:6:"footer";i:3;}}', 'yes'),
(3342, '_site_transient_timeout_browser_b22dc2264b397479d0d4c79bb3f5bcf2', '1433527736', 'yes'),
(3343, '_site_transient_browser_b22dc2264b397479d0d4c79bb3f5bcf2', 'a:9:{s:8:"platform";s:7:"Windows";s:4:"name";s:6:"Chrome";s:7:"version";s:12:"43.0.2357.65";s:10:"update_url";s:28:"http://www.google.com/chrome";s:7:"img_src";s:49:"http://s.wordpress.org/images/browsers/chrome.png";s:11:"img_src_ssl";s:48:"https://wordpress.org/images/browsers/chrome.png";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'yes'),
(3381, '_site_transient_timeout_wporg_theme_feature_list', '1432937939', 'yes'),
(3382, '_site_transient_wporg_theme_feature_list', 'a:4:{s:6:"Colors";a:15:{i:0;s:5:"black";i:1;s:4:"blue";i:2;s:5:"brown";i:3;s:4:"gray";i:4;s:5:"green";i:5;s:6:"orange";i:6;s:4:"pink";i:7;s:6:"purple";i:8;s:3:"red";i:9;s:6:"silver";i:10;s:3:"tan";i:11;s:5:"white";i:12;s:6:"yellow";i:13;s:4:"dark";i:14;s:5:"light";}s:6:"Layout";a:9:{i:0;s:12:"fixed-layout";i:1;s:12:"fluid-layout";i:2;s:17:"responsive-layout";i:3;s:10:"one-column";i:4;s:11:"two-columns";i:5;s:13:"three-columns";i:6;s:12:"four-columns";i:7;s:12:"left-sidebar";i:8;s:13:"right-sidebar";}s:8:"Features";a:20:{i:0;s:19:"accessibility-ready";i:1;s:8:"blavatar";i:2;s:10:"buddypress";i:3;s:17:"custom-background";i:4;s:13:"custom-colors";i:5;s:13:"custom-header";i:6;s:11:"custom-menu";i:7;s:12:"editor-style";i:8;s:21:"featured-image-header";i:9;s:15:"featured-images";i:10;s:15:"flexible-header";i:11;s:20:"front-page-post-form";i:12;s:19:"full-width-template";i:13;s:12:"microformats";i:14;s:12:"post-formats";i:15;s:20:"rtl-language-support";i:16;s:11:"sticky-post";i:17;s:13:"theme-options";i:18;s:17:"threaded-comments";i:19;s:17:"translation-ready";}s:7:"Subject";a:3:{i:0;s:7:"holiday";i:1;s:13:"photoblogging";i:2;s:8:"seasonal";}}', 'yes'),
(3383, 'theme_mods_twentyfifteen', 'a:1:{s:16:"sidebars_widgets";a:2:{s:4:"time";i:1432927184;s:4:"data";a:2:{s:19:"wp_inactive_widgets";a:3:{i:0;s:8:"wysija-2";i:1;s:21:"widget-corbeilinfos-2";i:2;s:22:"eo_event_list_widget-2";}s:9:"sidebar-1";a:3:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:6:"meta-2";}}}}', 'yes'),
(3398, 'rewrite_rules', 'a:124:{s:15:"associations/?$";s:31:"index.php?post_type=association";s:45:"associations/feed/(feed|rdf|rss|rss2|atom)/?$";s:48:"index.php?post_type=association&feed=$matches[1]";s:40:"associations/(feed|rdf|rss|rss2|atom)/?$";s:48:"index.php?post_type=association&feed=$matches[1]";s:32:"associations/page/([0-9]{1,})/?$";s:49:"index.php?post_type=association&paged=$matches[1]";s:13:"evenements/?$";s:25:"index.php?post_type=event";s:43:"evenements/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?post_type=event&feed=$matches[1]";s:38:"evenements/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?post_type=event&feed=$matches[1]";s:30:"evenements/page/([0-9]{1,})/?$";s:43:"index.php?post_type=event&paged=$matches[1]";s:57:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:52:"category/(.+?)/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:54:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:49:"tag/([^/]+)/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:14:"tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:55:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:50:"type/([^/]+)/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:66:"evenements/lieu/([^/]+)/feed/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:50:"index.php?event-venue=$matches[1]&feed=$matches[2]";s:61:"evenements/lieu/([^/]+)/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:50:"index.php?event-venue=$matches[1]&feed=$matches[2]";s:44:"evenements/lieu/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?event-venue=$matches[1]&paged=$matches[2]";s:26:"evenements/lieu/([^/]+)/?$";s:33:"index.php?event-venue=$matches[1]";s:71:"evenements/categorie/([^/]+)/feed/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:53:"index.php?event-category=$matches[1]&feed=$matches[2]";s:66:"evenements/categorie/([^/]+)/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:53:"index.php?event-category=$matches[1]&feed=$matches[2]";s:49:"evenements/categorie/([^/]+)/page/?([0-9]{1,})/?$";s:54:"index.php?event-category=$matches[1]&paged=$matches[2]";s:31:"evenements/categorie/([^/]+)/?$";s:36:"index.php?event-category=$matches[1]";s:70:"evenements/mot-clef/([^/]+)/feed/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:48:"index.php?event-tag=$matches[1]&feed=$matches[2]";s:65:"evenements/mot-clef/([^/]+)/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:48:"index.php?event-tag=$matches[1]&feed=$matches[2]";s:48:"evenements/mot-clef/([^/]+)/page/?([0-9]{1,})/?$";s:49:"index.php?event-tag=$matches[1]&paged=$matches[2]";s:30:"evenements/mot-clef/([^/]+)/?$";s:31:"index.php?event-tag=$matches[1]";s:40:"associations/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:50:"associations/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:80:"associations/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:75:"associations/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:65:"associations/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:"associations/([^/]+)/trackback/?$";s:39:"index.php?associations=$matches[1]&tb=1";s:63:"associations/([^/]+)/feed/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:51:"index.php?associations=$matches[1]&feed=$matches[2]";s:58:"associations/([^/]+)/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:51:"index.php?associations=$matches[1]&feed=$matches[2]";s:41:"associations/([^/]+)/page/?([0-9]{1,})/?$";s:52:"index.php?associations=$matches[1]&paged=$matches[2]";s:48:"associations/([^/]+)/comment-page-([0-9]{1,})/?$";s:52:"index.php?associations=$matches[1]&cpage=$matches[2]";s:33:"associations/([^/]+)(/[0-9]+)?/?$";s:51:"index.php?associations=$matches[1]&page=$matches[2]";s:29:"associations/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:39:"associations/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:69:"associations/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:64:"associations/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:54:"associations/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:95:"evenements/on/([0-9]{4}(?:/[0-9]{2}(?:/[0-9]{2})?)?)/feed/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:61:"index.php?post_type=event&ondate=$matches[1]&feed=$matches[2]";s:90:"evenements/on/([0-9]{4}(?:/[0-9]{2}(?:/[0-9]{2})?)?)/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:61:"index.php?post_type=event&ondate=$matches[1]&feed=$matches[2]";s:73:"evenements/on/([0-9]{4}(?:/[0-9]{2}(?:/[0-9]{2})?)?)/page/?([0-9]{1,})/?$";s:62:"index.php?post_type=event&ondate=$matches[1]&paged=$matches[2]";s:55:"evenements/on/([0-9]{4}(?:/[0-9]{2}(?:/[0-9]{2})?)?)/?$";s:44:"index.php?post_type=event&ondate=$matches[1]";s:37:"evenement/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:47:"evenement/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:77:"evenement/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:72:"evenement/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:62:"evenement/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:30:"evenement/([^/]+)/trackback/?$";s:32:"index.php?event=$matches[1]&tb=1";s:60:"evenement/([^/]+)/feed/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:44:"index.php?event=$matches[1]&feed=$matches[2]";s:55:"evenement/([^/]+)/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:44:"index.php?event=$matches[1]&feed=$matches[2]";s:38:"evenement/([^/]+)/page/?([0-9]{1,})/?$";s:45:"index.php?event=$matches[1]&paged=$matches[2]";s:45:"evenement/([^/]+)/comment-page-([0-9]{1,})/?$";s:45:"index.php?event=$matches[1]&cpage=$matches[2]";s:30:"evenement/([^/]+)(/[0-9]+)?/?$";s:44:"index.php?event=$matches[1]&page=$matches[2]";s:26:"evenement/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:36:"evenement/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:66:"evenement/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:61:"evenement/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:51:"evenement/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:42:"feed/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:27:"index.php?&feed=$matches[1]";s:37:"(feed|rdf|rss|rss2|atom|eo-events)/?$";s:27:"index.php?&feed=$matches[1]";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:27:"comment-page-([0-9]{1,})/?$";s:38:"index.php?&page_id=8&cpage=$matches[1]";s:51:"comments/feed/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:46:"comments/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:54:"search/(.+)/feed/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:49:"search/(.+)/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:57:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:52:"author/([^/]+)/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:79:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:74:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:66:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:61:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:53:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:48:"([0-9]{4})/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:67:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:62:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:50:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:45:"(.?.+?)/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:20:"(.?.+?)(/[0-9]+)?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";s:27:"[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:"[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:67:"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:62:"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:20:"([^/]+)/trackback/?$";s:31:"index.php?name=$matches[1]&tb=1";s:50:"([^/]+)/feed/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:45:"([^/]+)/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:28:"([^/]+)/page/?([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&paged=$matches[2]";s:35:"([^/]+)/comment-page-([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&cpage=$matches[2]";s:20:"([^/]+)(/[0-9]+)?/?$";s:43:"index.php?name=$matches[1]&page=$matches[2]";s:16:"[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:26:"[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:56:"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:51:"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom|eo-events)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";}', 'yes'),
(3413, '_site_transient_update_plugins', 'O:8:"stdClass":1:{s:12:"last_checked";i:1433915242;}', 'yes'),
(3414, '_site_transient_timeout_theme_roots', '1433916909', 'yes'),
(3415, '_site_transient_theme_roots', 'a:3:{s:16:"sequential-child";s:7:"/themes";s:10:"sequential";s:7:"/themes";s:13:"twentyfifteen";s:7:"/themes";}', 'yes'),
(3416, '_site_transient_update_themes', 'O:8:"stdClass":1:{s:12:"last_checked";i:1433915149;}', 'yes'),
(3420, '_transient_timeout_acf_pro_get_remote_info', '1433922392', 'no'),
(3421, '_transient_acf_pro_get_remote_info', '0', 'no'),
(3422, '_transient_timeout_eo_full_calendar_public_priv', '1434001593', 'no'),
(3423, '_transient_eo_full_calendar_public_priv', 'a:1:{s:38:"eo_fc_1601225f8ddf00dbf795244e7a0cc3d2";a:1:{i:0;a:11:{s:5:"title";s:29:"L’Univers au fil de l’eau";s:3:"url";s:64:"http://127.0.0.1/corbeilassoc/evenement/lunivers-au-fil-de-leau/";s:6:"allDay";b:0;s:5:"start";s:19:"2015-06-06T20:00:00";s:3:"end";s:19:"2015-06-06T22:00:00";s:11:"description";s:436:"juin 6, 2015 20:00 - 22:00</br></br>Benoît Reeves à Corbeil-Essonnes, le fils de l’astrophysicien Hubert Reeves sera à Corbeil-Essonnes le 6 juin prochain à 20h pour nous présenter son film : « L’Univers au fil de l’eau » Spécialisé dans les domaines de l’astronomie et de l’environnement, Benoît Reeves (son site personnel), fils du célèbre astrophysicien franco-canadien Hubert Reeves, viendra le 6 juin à...";s:9:"className";a:13:{i:0;s:27:"eo-event-venue-cinema-arcel";i:1;s:26:"eo-event-cat-culture-event";i:2;s:19:"eo-event-tag-cinema";i:3;s:16:"eo-event-tag-eau";i:4;s:26:"eo-event-tag-environnement";i:5;s:13:"eo-event-past";i:6;s:8:"eo-event";i:7;s:13:"eo-past-event";i:8;s:18:"venue-cinema-arcel";i:9;s:22:"category-culture-event";i:10;s:10:"tag-cinema";i:11;s:7:"tag-eau";i:12;s:17:"tag-environnement";}s:5:"venue";i:16;s:8:"category";a:1:{i:0;s:13:"culture-event";}s:4:"tags";a:3:{i:0;s:6:"cinema";i:1;s:3:"eau";i:2;s:13:"environnement";}s:9:"textColor";s:7:"#ffffff";}}}', 'no'),
(3424, '_transient_timeout_plugin_slugs', '1434001642', 'no'),
(3425, '_transient_plugin_slugs', 'a:11:{i:0;s:34:"advanced-custom-fields-pro/acf.php";i:1;s:37:"association-type/association-type.php";i:2;s:41:"CorbeilinfosPlugin/CorbeilinfosPlugin.php";i:3;s:35:"event-organiser/event-organiser.php";i:4;s:43:"event-organiser-fes/event-organiser-fes.php";i:5;s:19:"if-menu/if-menu.php";i:6;s:51:"juiz-social-post-sharer/juiz-social-post-sharer.php";i:7;s:28:"wysija-newsletters/index.php";i:8;s:43:"simple-custom-types/simple-custom-types.php";i:9;s:27:"user-system/user-system.php";i:10;s:41:"wordpress-importer/wordpress-importer.php";}', 'no'),
(3426, '_transient_timeout_dash_4077549d03da2e451c8b5f002294ff51', '1433958418', 'no'),
(3427, '_transient_dash_4077549d03da2e451c8b5f002294ff51', '<div class="rss-widget"><p><strong>Erreur RSS&nbsp;:</strong> WP HTTP Error: Could not resolve host: feeds.feedburner.com</p></div><div class="rss-widget"><p><strong>Erreur RSS&nbsp;:</strong> WP HTTP Error: Could not resolve host: feeds.feedburner.com</p></div><div class="rss-widget"><ul></ul></div>', 'no');

-- --------------------------------------------------------

--
-- Structure de la table `crbass_postmeta`
--

CREATE TABLE IF NOT EXISTS `crbass_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=929 ;

--
-- Contenu de la table `crbass_postmeta`
--

INSERT INTO `crbass_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 4, '_edit_last', '1'),
(3, 4, '_edit_lock', '1427292963:1'),
(4, 4, '_eventorganiser_event_schedule', 'a:8:{s:7:"all_day";i:0;s:8:"schedule";s:4:"once";s:13:"schedule_meta";s:0:"";s:9:"frequency";i:1;s:7:"exclude";a:0:{}s:7:"include";a:0:{}s:12:"duration_str";s:55:"+0 year +0 month +0 days +4 hours +0 minutes +0 seconds";s:12:"_occurrences";a:1:{i:1;s:19:"2014-12-28 08:00:00";}}'),
(5, 4, '_eventorganiser_schedule_start_start', '2014-12-28 08:00:00'),
(6, 4, '_eventorganiser_schedule_start_finish', '2014-12-28 12:00:00'),
(7, 4, '_eventorganiser_schedule_last_start', '2014-12-28 08:00:00'),
(8, 4, '_eventorganiser_schedule_last_finish', '2014-12-28 12:00:00'),
(26, 7, '_menu_item_type', 'post_type_archive'),
(27, 7, '_menu_item_menu_item_parent', '0'),
(28, 7, '_menu_item_object_id', '0'),
(29, 7, '_menu_item_object', 'event'),
(30, 7, '_menu_item_target', ''),
(31, 7, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(32, 7, '_menu_item_xfn', ''),
(33, 7, '_menu_item_url', 'http://127.0.0.1/corbeilassoc/evenements/'),
(34, 8, '_edit_last', '1'),
(35, 8, '_wp_page_template', 'default'),
(36, 8, '_edit_lock', '1419588743:1'),
(37, 10, '_edit_last', '1'),
(38, 10, '_edit_lock', '1419588264:1'),
(39, 10, '_wp_page_template', 'default'),
(40, 12, '_menu_item_type', 'post_type'),
(41, 12, '_menu_item_menu_item_parent', '0'),
(42, 12, '_menu_item_object_id', '10'),
(43, 12, '_menu_item_object', 'page'),
(44, 12, '_menu_item_target', ''),
(45, 12, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(46, 12, '_menu_item_xfn', ''),
(47, 12, '_menu_item_url', ''),
(56, 36, '_edit_last', '1'),
(57, 36, '_edit_lock', '1426594656:1'),
(58, 43, '_edit_last', '1'),
(59, 43, '_edit_lock', '1427293404:1'),
(64, 43, 'president', 'Mohamed GUENDOUZI'),
(65, 43, '_president', 'field_549c57f8ee819'),
(66, 43, 'adresse', ''),
(67, 43, '_adresse', 'field_549c5815ee81a'),
(68, 43, 'tel', '0755664477'),
(69, 43, '_tel', 'field_549c584fee81b'),
(70, 43, 'email', 'contact@aem91.fr'),
(71, 43, '_email', 'field_549c58baee81d'),
(72, 43, 'site_internet', 'http://aem91.fr'),
(73, 43, '_site_internet', 'field_549c58cfee81e'),
(76, 47, '_edit_last', '1'),
(77, 47, '_edit_lock', '1427295023:1'),
(78, 47, '_wp_page_template', 'page-templates/associations.php'),
(79, 50, '_edit_last', '1'),
(80, 50, '_edit_lock', '1419587341:1'),
(81, 2, '_edit_lock', '1421234926:1'),
(82, 2, 'nom', 'Florian TIAR'),
(83, 2, '_edit_last', '1'),
(87, 61, '_um_custom_fields', 'a:6:{s:10:"user_login";a:17:{s:6:"in_row";s:9:"_um_row_1";s:10:"in_sub_row";s:1:"0";s:9:"in_column";s:1:"1";s:4:"type";s:4:"text";s:7:"metakey";s:10:"user_login";s:8:"position";s:1:"1";s:5:"title";s:10:"Pseudonyme";s:9:"min_chars";s:1:"3";s:10:"visibility";s:3:"all";s:5:"label";s:10:"Pseudonyme";s:6:"public";s:1:"1";s:8:"validate";s:15:"unique_username";s:9:"max_chars";s:2:"24";s:8:"required";s:1:"1";s:8:"editable";s:1:"0";s:17:"conditional_value";s:1:"0";s:8:"in_group";s:0:"";}s:10:"user_email";a:15:{s:6:"in_row";s:9:"_um_row_1";s:10:"in_sub_row";s:1:"0";s:9:"in_column";s:1:"1";s:4:"type";s:4:"text";s:7:"metakey";s:10:"user_email";s:8:"position";s:1:"4";s:5:"title";s:14:"Adresse E-mail";s:10:"visibility";s:3:"all";s:5:"label";s:14:"Adresse E-mail";s:6:"public";s:1:"1";s:8:"validate";s:12:"unique_email";s:8:"required";s:1:"0";s:8:"editable";s:1:"1";s:17:"conditional_value";s:1:"0";s:8:"in_group";s:0:"";}s:13:"user_password";a:18:{s:6:"in_row";s:9:"_um_row_1";s:10:"in_sub_row";s:1:"0";s:9:"in_column";s:1:"1";s:4:"type";s:8:"password";s:7:"metakey";s:13:"user_password";s:8:"position";s:1:"5";s:5:"title";s:12:"Mot de passe";s:9:"min_chars";s:1:"6";s:9:"max_chars";s:2:"30";s:10:"visibility";s:3:"all";s:5:"label";s:12:"Mot de passe";s:6:"public";s:1:"1";s:15:"force_good_pass";s:1:"0";s:18:"force_confirm_pass";s:1:"0";s:8:"required";s:1:"1";s:8:"editable";s:1:"1";s:17:"conditional_value";s:1:"0";s:8:"in_group";s:0:"";}s:10:"first_name";a:14:{s:6:"in_row";s:9:"_um_row_1";s:10:"in_sub_row";s:1:"0";s:9:"in_column";s:1:"1";s:4:"type";s:4:"text";s:7:"metakey";s:10:"first_name";s:8:"position";s:1:"2";s:5:"title";s:7:"Prénom";s:10:"visibility";s:3:"all";s:5:"label";s:7:"Prénom";s:6:"public";s:1:"1";s:8:"required";s:1:"0";s:8:"editable";s:1:"1";s:17:"conditional_value";s:1:"0";s:8:"in_group";s:0:"";}s:9:"last_name";a:14:{s:6:"in_row";s:9:"_um_row_1";s:10:"in_sub_row";s:1:"0";s:9:"in_column";s:1:"1";s:4:"type";s:4:"text";s:7:"metakey";s:9:"last_name";s:8:"position";s:1:"3";s:5:"title";s:3:"Nom";s:10:"visibility";s:3:"all";s:5:"label";s:3:"Nom";s:6:"public";s:1:"1";s:8:"required";s:1:"0";s:8:"editable";s:1:"1";s:17:"conditional_value";s:1:"0";s:8:"in_group";s:0:"";}s:9:"_um_row_1";a:5:{s:4:"type";s:3:"row";s:2:"id";s:9:"_um_row_1";s:8:"sub_rows";s:1:"1";s:4:"cols";s:1:"1";s:6:"origin";s:9:"_um_row_1";}}'),
(88, 61, '_um_mode', 'register'),
(89, 61, '_um_core', 'register'),
(90, 61, '_um_register_use_globals', '0'),
(91, 62, '_um_custom_fields', 'a:3:{s:8:"username";a:15:{s:6:"in_row";s:9:"_um_row_1";s:10:"in_sub_row";s:1:"0";s:9:"in_column";s:1:"1";s:4:"type";s:4:"text";s:7:"metakey";s:8:"username";s:8:"position";s:1:"1";s:5:"title";s:20:"Pseudonyme ou E-mail";s:10:"visibility";s:3:"all";s:5:"label";s:20:"Pseudonyme ou E-mail";s:6:"public";s:1:"1";s:8:"validate";s:24:"unique_username_or_email";s:8:"required";s:1:"1";s:8:"editable";s:1:"0";s:17:"conditional_value";s:1:"0";s:8:"in_group";s:0:"";}s:13:"user_password";a:18:{s:6:"in_row";s:9:"_um_row_1";s:10:"in_sub_row";s:1:"0";s:9:"in_column";s:1:"1";s:4:"type";s:8:"password";s:7:"metakey";s:13:"user_password";s:8:"position";s:1:"2";s:5:"title";s:12:"Mot de passe";s:9:"min_chars";s:1:"6";s:9:"max_chars";s:2:"30";s:10:"visibility";s:3:"all";s:5:"label";s:12:"Mot de passe";s:6:"public";s:1:"1";s:15:"force_good_pass";s:1:"0";s:18:"force_confirm_pass";s:1:"1";s:8:"required";s:1:"1";s:8:"editable";s:1:"1";s:17:"conditional_value";s:1:"0";s:8:"in_group";s:0:"";}s:9:"_um_row_1";a:5:{s:4:"type";s:3:"row";s:2:"id";s:9:"_um_row_1";s:8:"sub_rows";s:1:"1";s:4:"cols";s:1:"1";s:6:"origin";s:9:"_um_row_1";}}'),
(92, 62, '_um_mode', 'login'),
(93, 62, '_um_core', 'login'),
(94, 62, '_um_login_use_globals', '0'),
(95, 63, '_um_custom_fields', 'a:1:{s:9:"_um_row_1";a:5:{s:4:"type";s:3:"row";s:2:"id";s:9:"_um_row_1";s:8:"sub_rows";s:1:"1";s:4:"cols";s:1:"1";s:6:"origin";s:9:"_um_row_1";}}'),
(96, 63, '_um_mode', 'profile'),
(97, 63, '_um_core', 'profile'),
(98, 63, '_um_profile_use_globals', '0'),
(99, 64, '_um_template', 'members'),
(100, 64, '_um_mode', 'directory'),
(101, 64, '_um_has_profile_photo', '0'),
(102, 64, '_um_has_cover_photo', '0'),
(103, 64, '_um_show_social', '0'),
(104, 64, '_um_show_userinfo', '0'),
(105, 64, '_um_show_tagline', '0'),
(106, 64, '_um_search', '0'),
(107, 64, '_um_userinfo_animate', '1'),
(108, 64, '_um_sortby', 'user_registered_desc'),
(109, 64, '_um_profile_photo', '1'),
(110, 64, '_um_cover_photos', '1'),
(111, 64, '_um_show_name', '1'),
(112, 64, '_um_directory_header', '{total_users} Members'),
(113, 64, '_um_directory_header_single', '{total_users} Member'),
(114, 64, '_um_directory_no_users', 'We are sorry. We cannot find any users who match your search criteria.'),
(115, 64, '_um_profiles_per_page', '12'),
(116, 64, '_um_profiles_per_page_mobile', '6'),
(117, 64, '_um_core', 'members'),
(119, 65, '_um_core', 'user'),
(121, 67, '_um_core', 'login'),
(123, 69, '_um_core', 'register'),
(132, 79, '_um_core', 'admin'),
(133, 79, '_um_can_access_wpadmin', '1'),
(134, 79, '_um_can_not_see_adminbar', '0'),
(135, 79, '_um_can_edit_everyone', '1'),
(136, 79, '_um_can_delete_everyone', '1'),
(137, 79, '_um_can_edit_profile', '1'),
(138, 79, '_um_can_delete_profile', '1'),
(139, 79, '_um_can_view_all', '1'),
(140, 79, '_um_can_make_private_profile', '1'),
(141, 79, '_um_can_access_private_profile', '1'),
(142, 79, '_um_default_homepage', '1'),
(143, 79, '_um_status', 'approved'),
(144, 79, '_um_auto_approve_act', 'redirect_profile'),
(145, 79, '_um_after_login', 'redirect_admin'),
(146, 79, '_um_after_logout', 'redirect_home'),
(147, 80, '_um_core', 'member'),
(148, 80, '_um_can_access_wpadmin', '0'),
(149, 80, '_um_can_not_see_adminbar', '1'),
(150, 80, '_um_can_edit_everyone', '0'),
(151, 80, '_um_can_delete_everyone', '0'),
(152, 80, '_um_can_edit_profile', '1'),
(153, 80, '_um_can_delete_profile', '1'),
(154, 80, '_um_can_view_all', '1'),
(155, 80, '_um_can_make_private_profile', '0'),
(156, 80, '_um_can_access_private_profile', '0'),
(157, 80, '_um_default_homepage', '1'),
(158, 80, '_um_status', 'approved'),
(159, 80, '_um_auto_approve_act', 'redirect_profile'),
(160, 80, '_um_after_login', 'redirect_profile'),
(161, 80, '_um_after_logout', 'redirect_home'),
(162, 61, '_edit_lock', '1426345130:1'),
(163, 62, '_edit_lock', '1426345345:1'),
(164, 61, '_edit_last', '1'),
(165, 61, '_um_register_role', '0'),
(166, 61, '_um_register_template', 'register'),
(167, 61, '_um_register_max_width', '450px'),
(168, 61, '_um_register_align', 'center'),
(169, 61, '_um_register_icons', 'label'),
(170, 61, '_um_register_primary_btn_word', 'Inscription'),
(171, 61, '_um_register_primary_btn_color', '#3ba1da'),
(172, 61, '_um_register_primary_btn_hover', '#44b0ec'),
(173, 61, '_um_register_primary_btn_text', '#ffffff'),
(174, 61, '_um_register_secondary_btn', '1'),
(175, 61, '_um_register_secondary_btn_word', 'Connexion'),
(176, 61, '_um_register_secondary_btn_color', '#eeeeee'),
(177, 61, '_um_register_secondary_btn_hover', '#e5e5e5'),
(178, 61, '_um_register_secondary_btn_text', '#666666'),
(179, 61, '_um_register_custom_css', ''),
(180, 61, '_um_profile_use_globals', '1'),
(181, 61, '_um_profile_role', '0'),
(182, 61, '_um_profile_template', 'profile'),
(183, 61, '_um_profile_max_width', '1000px'),
(184, 61, '_um_profile_area_max_width', '600px'),
(185, 61, '_um_profile_align', 'center'),
(186, 61, '_um_profile_icons', 'label'),
(187, 61, '_um_profile_primary_btn_word', 'Update Profile'),
(188, 61, '_um_profile_primary_btn_color', '#3ba1da'),
(189, 61, '_um_profile_primary_btn_hover', '#44b0ec'),
(190, 61, '_um_profile_primary_btn_text', '#ffffff'),
(191, 61, '_um_profile_secondary_btn', '1'),
(192, 61, '_um_profile_secondary_btn_word', 'Cancel'),
(193, 61, '_um_profile_secondary_btn_color', '#eeeeee'),
(194, 61, '_um_profile_secondary_btn_hover', '#e5e5e5'),
(195, 61, '_um_profile_secondary_btn_text', '#666666'),
(196, 61, '_um_profile_main_bg', '0'),
(197, 61, '_um_profile_cover_enabled', '1'),
(198, 61, '_um_profile_cover_ratio', '2.7:1'),
(199, 61, '_um_profile_photosize', '190px'),
(200, 61, '_um_profile_photocorner', '1'),
(201, 61, '_um_profile_header_bg', '0'),
(202, 61, '_um_profile_header_text', '#999999'),
(203, 61, '_um_profile_header_link_color', '#555555'),
(204, 61, '_um_profile_header_link_hcolor', '#444444'),
(205, 61, '_um_profile_header_icon_color', '#aaaaaa'),
(206, 61, '_um_profile_header_icon_hcolor', '#3ba1da'),
(207, 61, '_um_profile_show_name', '1'),
(208, 61, '_um_profile_show_bio', '1'),
(210, 61, '_um_profile_custom_css', ''),
(211, 61, '_um_login_use_globals', '1'),
(212, 61, '_um_login_template', 'login'),
(213, 61, '_um_login_max_width', '450px'),
(214, 61, '_um_login_align', 'center'),
(215, 61, '_um_login_icons', 'label'),
(216, 61, '_um_login_primary_btn_word', 'Login'),
(217, 61, '_um_login_primary_btn_color', '#3ba1da'),
(218, 61, '_um_login_primary_btn_hover', '#44b0ec'),
(219, 61, '_um_login_primary_btn_text', '#ffffff'),
(220, 61, '_um_login_secondary_btn', '1'),
(221, 61, '_um_login_secondary_btn_word', 'Register'),
(222, 61, '_um_login_secondary_btn_color', '#eeeeee'),
(223, 61, '_um_login_secondary_btn_hover', '#e5e5e5'),
(224, 61, '_um_login_secondary_btn_text', '#666666'),
(225, 61, '_um_login_forgot_pass_link', '1'),
(226, 61, '_um_login_show_rememberme', '1'),
(227, 61, '_um_login_after_login', '0'),
(228, 61, '_um_login_redirect_url', ''),
(229, 61, '_um_login_custom_css', ''),
(230, 62, '_edit_last', '1'),
(231, 62, '_um_register_use_globals', '1'),
(232, 62, '_um_register_role', '0'),
(233, 62, '_um_register_template', 'register'),
(234, 62, '_um_register_max_width', '450px'),
(235, 62, '_um_register_align', 'center'),
(236, 62, '_um_register_icons', 'label'),
(237, 62, '_um_register_primary_btn_word', 'Register'),
(238, 62, '_um_register_primary_btn_color', '#3ba1da'),
(239, 62, '_um_register_primary_btn_hover', '#44b0ec'),
(240, 62, '_um_register_primary_btn_text', '#ffffff'),
(241, 62, '_um_register_secondary_btn', '1'),
(242, 62, '_um_register_secondary_btn_word', 'Login'),
(243, 62, '_um_register_secondary_btn_color', '#eeeeee'),
(244, 62, '_um_register_secondary_btn_hover', '#e5e5e5'),
(245, 62, '_um_register_secondary_btn_text', '#666666'),
(246, 62, '_um_register_custom_css', ''),
(247, 62, '_um_profile_use_globals', '1'),
(248, 62, '_um_profile_role', '0'),
(249, 62, '_um_profile_template', 'profile'),
(250, 62, '_um_profile_max_width', '1000px'),
(251, 62, '_um_profile_area_max_width', '600px'),
(252, 62, '_um_profile_align', 'center'),
(253, 62, '_um_profile_icons', 'label'),
(254, 62, '_um_profile_primary_btn_word', 'Update Profile'),
(255, 62, '_um_profile_primary_btn_color', '#3ba1da'),
(256, 62, '_um_profile_primary_btn_hover', '#44b0ec'),
(257, 62, '_um_profile_primary_btn_text', '#ffffff'),
(258, 62, '_um_profile_secondary_btn', '1'),
(259, 62, '_um_profile_secondary_btn_word', 'Cancel'),
(260, 62, '_um_profile_secondary_btn_color', '#eeeeee'),
(261, 62, '_um_profile_secondary_btn_hover', '#e5e5e5'),
(262, 62, '_um_profile_secondary_btn_text', '#666666'),
(263, 62, '_um_profile_main_bg', '0'),
(264, 62, '_um_profile_cover_enabled', '1'),
(265, 62, '_um_profile_cover_ratio', '2.7:1'),
(266, 62, '_um_profile_photosize', '190px'),
(267, 62, '_um_profile_photocorner', '1'),
(268, 62, '_um_profile_header_bg', '0'),
(269, 62, '_um_profile_header_text', '#999999'),
(270, 62, '_um_profile_header_link_color', '#555555'),
(271, 62, '_um_profile_header_link_hcolor', '#444444'),
(272, 62, '_um_profile_header_icon_color', '#aaaaaa'),
(273, 62, '_um_profile_header_icon_hcolor', '#3ba1da'),
(274, 62, '_um_profile_show_name', '1'),
(275, 62, '_um_profile_show_bio', '1'),
(277, 62, '_um_profile_custom_css', ''),
(278, 62, '_um_login_template', 'login'),
(279, 62, '_um_login_max_width', '450px'),
(280, 62, '_um_login_align', 'center'),
(281, 62, '_um_login_icons', 'label'),
(282, 62, '_um_login_primary_btn_word', 'Connexion'),
(283, 62, '_um_login_primary_btn_color', '#3ba1da'),
(284, 62, '_um_login_primary_btn_hover', '#44b0ec'),
(285, 62, '_um_login_primary_btn_text', '#ffffff'),
(286, 62, '_um_login_secondary_btn', '1'),
(287, 62, '_um_login_secondary_btn_word', 'Inscription'),
(288, 62, '_um_login_secondary_btn_color', '#eeeeee'),
(289, 62, '_um_login_secondary_btn_hover', '#e5e5e5'),
(290, 62, '_um_login_secondary_btn_text', '#666666'),
(291, 62, '_um_login_forgot_pass_link', '1'),
(292, 62, '_um_login_show_rememberme', '1'),
(293, 62, '_um_login_after_login', '0'),
(294, 62, '_um_login_redirect_url', ''),
(295, 62, '_um_login_custom_css', ''),
(296, 69, '_edit_lock', '1430601922:1'),
(297, 67, '_edit_lock', '1430601944:1'),
(298, 65, '_edit_lock', '1432968265:1'),
(299, 69, '_edit_last', '1'),
(300, 69, '_wp_page_template', 'page-templates/full-width-page.php'),
(301, 69, '_um_custom_access_settings', '0'),
(302, 69, '_um_accessible', '0'),
(303, 69, '_um_access_redirect2', ''),
(304, 69, '_um_access_redirect', ''),
(309, 67, '_edit_last', '1'),
(310, 67, '_wp_page_template', 'page-templates/full-width-page.php'),
(311, 67, '_um_custom_access_settings', '0'),
(312, 67, '_um_accessible', '0'),
(313, 67, '_um_access_redirect2', ''),
(314, 67, '_um_access_redirect', ''),
(315, 65, '_edit_last', '1'),
(316, 65, '_wp_page_template', 'page-templates/profile-page.php'),
(317, 65, '_um_custom_access_settings', '0'),
(318, 65, '_um_accessible', '0'),
(319, 65, '_um_access_redirect2', ''),
(320, 65, '_um_access_redirect', ''),
(331, 91, '_menu_item_type', 'post_type'),
(332, 91, '_menu_item_menu_item_parent', '0'),
(333, 91, '_menu_item_object_id', '67'),
(334, 91, '_menu_item_object', 'page'),
(335, 91, '_menu_item_target', ''),
(336, 91, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(337, 91, '_menu_item_xfn', ''),
(338, 91, '_menu_item_url', ''),
(358, 64, '_edit_lock', '1426342672:1'),
(359, 64, '_edit_last', '1'),
(360, 64, '_um_directory_template', 'members'),
(361, 64, '_um_css_profile_card_bg', ''),
(362, 64, '_um_css_profile_card_text', ''),
(363, 64, '_um_css_card_bordercolor', ''),
(364, 64, '_um_css_img_bordercolor', ''),
(365, 64, '_um_css_card_thickness', '1px'),
(366, 64, '_um_sortby_custom', ''),
(367, 64, '_um_tagline_fields', 'a:1:{i:0;s:1:"0";}'),
(368, 64, '_um_reveal_fields', 'a:1:{i:0;s:1:"0";}'),
(369, 64, '_um_search_fields', 'a:1:{i:0;s:1:"0";}'),
(370, 64, '_um_max_users', ''),
(371, 61, '_um_profile_metafields', 'a:1:{i:0;s:1:"0";}'),
(372, 63, '_edit_lock', '1426345203:1'),
(373, 63, '_edit_last', '1'),
(374, 63, '_um_register_use_globals', '1'),
(375, 63, '_um_register_role', '0'),
(376, 63, '_um_register_template', 'register'),
(377, 63, '_um_register_max_width', '450px'),
(378, 63, '_um_register_align', 'center'),
(379, 63, '_um_register_icons', 'label'),
(380, 63, '_um_register_primary_btn_word', 'Register'),
(381, 63, '_um_register_primary_btn_color', '#3ba1da'),
(382, 63, '_um_register_primary_btn_hover', '#44b0ec'),
(383, 63, '_um_register_primary_btn_text', '#ffffff'),
(384, 63, '_um_register_secondary_btn', '1'),
(385, 63, '_um_register_secondary_btn_word', 'Login'),
(386, 63, '_um_register_secondary_btn_color', '#eeeeee'),
(387, 63, '_um_register_secondary_btn_hover', '#e5e5e5'),
(388, 63, '_um_register_secondary_btn_text', '#666666'),
(389, 63, '_um_register_custom_css', ''),
(390, 63, '_um_profile_role', '0'),
(391, 63, '_um_profile_template', 'profile'),
(392, 63, '_um_profile_max_width', '1000px'),
(393, 63, '_um_profile_area_max_width', '600px'),
(394, 63, '_um_profile_align', 'center'),
(395, 63, '_um_profile_icons', 'label'),
(396, 63, '_um_profile_primary_btn_word', 'Metre à jour le Profil'),
(397, 63, '_um_profile_primary_btn_color', '#3ba1da'),
(398, 63, '_um_profile_primary_btn_hover', '#44b0ec'),
(399, 63, '_um_profile_primary_btn_text', '#ffffff'),
(400, 63, '_um_profile_secondary_btn', '1'),
(401, 63, '_um_profile_secondary_btn_word', 'Annuler'),
(402, 63, '_um_profile_secondary_btn_color', '#eeeeee'),
(403, 63, '_um_profile_secondary_btn_hover', '#e5e5e5'),
(404, 63, '_um_profile_secondary_btn_text', '#666666'),
(405, 63, '_um_profile_main_bg', '0'),
(406, 63, '_um_profile_cover_enabled', '1'),
(407, 63, '_um_profile_cover_ratio', '2.7:1'),
(408, 63, '_um_profile_photosize', '190px'),
(409, 63, '_um_profile_photocorner', '1'),
(410, 63, '_um_profile_header_bg', '0'),
(411, 63, '_um_profile_header_text', '#999999'),
(412, 63, '_um_profile_header_link_color', '#555555'),
(413, 63, '_um_profile_header_link_hcolor', '#444444'),
(414, 63, '_um_profile_header_icon_color', '#aaaaaa'),
(415, 63, '_um_profile_header_icon_hcolor', '#3ba1da'),
(416, 63, '_um_profile_show_name', '1'),
(417, 63, '_um_profile_show_bio', '1'),
(418, 63, '_um_profile_metafields', 'a:1:{i:0;s:1:"0";}'),
(419, 63, '_um_profile_custom_css', ''),
(420, 63, '_um_login_use_globals', '1'),
(421, 63, '_um_login_template', 'login'),
(422, 63, '_um_login_max_width', '450px'),
(423, 63, '_um_login_align', 'center'),
(424, 63, '_um_login_icons', 'label'),
(425, 63, '_um_login_primary_btn_word', 'Login'),
(426, 63, '_um_login_primary_btn_color', '#3ba1da'),
(427, 63, '_um_login_primary_btn_hover', '#44b0ec'),
(428, 63, '_um_login_primary_btn_text', '#ffffff'),
(429, 63, '_um_login_secondary_btn', '1'),
(430, 63, '_um_login_secondary_btn_word', 'Register'),
(431, 63, '_um_login_secondary_btn_color', '#eeeeee'),
(432, 63, '_um_login_secondary_btn_hover', '#e5e5e5'),
(433, 63, '_um_login_secondary_btn_text', '#666666'),
(434, 63, '_um_login_forgot_pass_link', '1'),
(435, 63, '_um_login_show_rememberme', '1'),
(436, 63, '_um_login_after_login', '0'),
(437, 63, '_um_login_redirect_url', ''),
(438, 63, '_um_login_custom_css', ''),
(439, 62, '_um_profile_metafields', 'a:1:{i:0;s:1:"0";}'),
(440, 47, '_um_custom_access_settings', '0'),
(441, 47, '_um_accessible', '0'),
(442, 47, '_um_access_redirect2', ''),
(443, 47, '_um_access_redirect', ''),
(445, 97, '_menu_item_type', 'post_type'),
(446, 97, '_menu_item_menu_item_parent', '0'),
(447, 97, '_menu_item_object_id', '47'),
(448, 97, '_menu_item_object', 'page'),
(449, 97, '_menu_item_target', ''),
(450, 97, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(451, 97, '_menu_item_xfn', ''),
(452, 97, '_menu_item_url', ''),
(457, 100, '_wp_attached_file', '2014/12/180x180.png'),
(458, 100, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:180;s:6:"height";i:180;s:4:"file";s:19:"2014/12/180x180.png";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"180x180-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(459, 101, '_wp_attached_file', '2014/12/logo.png'),
(460, 101, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1151;s:6:"height";i:445;s:4:"file";s:16:"2014/12/logo.png";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:16:"logo-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:16:"logo-300x116.png";s:5:"width";i:300;s:6:"height";i:116;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:17:"logo-1024x396.png";s:5:"width";i:1024;s:6:"height";i:396;s:9:"mime-type";s:9:"image/png";}s:25:"sequential-featured-image";a:4:{s:4:"file";s:16:"logo-772x298.png";s:5:"width";i:772;s:6:"height";i:298;s:9:"mime-type";s:9:"image/png";}s:15:"sequential-logo";a:4:{s:4:"file";s:16:"logo-624x241.png";s:5:"width";i:624;s:6:"height";i:241;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(461, 101, '_wp_attachment_image_alt', 'Logo'),
(462, 43, '_thumbnail_id', '102'),
(463, 43, '_um_custom_access_settings', '0'),
(464, 43, '_um_accessible', '0'),
(465, 43, '_um_access_redirect2', ''),
(466, 43, '_um_access_redirect', ''),
(467, 102, '_wp_attached_file', '2014/12/Cover-twitter-PP-2.jpg'),
(468, 102, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1079;s:6:"height";i:1079;s:4:"file";s:30:"2014/12/Cover-twitter-PP-2.jpg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:30:"Cover-twitter-PP-2-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:30:"Cover-twitter-PP-2-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:32:"Cover-twitter-PP-2-1024x1024.jpg";s:5:"width";i:1024;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}s:25:"sequential-featured-image";a:4:{s:4:"file";s:30:"Cover-twitter-PP-2-772x772.jpg";s:5:"width";i:772;s:6:"height";i:772;s:9:"mime-type";s:10:"image/jpeg";}s:15:"sequential-logo";a:4:{s:4:"file";s:30:"Cover-twitter-PP-2-624x624.jpg";s:5:"width";i:624;s:6:"height";i:624;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:13:"myriam fartas";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:1414451119;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(469, 103, '_edit_lock', '1430686309:1'),
(470, 103, '_edit_last', '1'),
(471, 103, '_wp_page_template', 'page-templates/add-association.php'),
(472, 105, 'president', 'Dany Meyer'),
(473, 105, '_president', 'field_549c57f8ee819'),
(474, 105, 'email', 'renaissanceetculture@orange.fr'),
(475, 105, '_email', 'field_549c58baee81d'),
(476, 105, 'adresse', 'Président Alliende, 91100 Corbeil-Essonnes'),
(477, 105, '_adresse', 'field_549c5815ee81a'),
(478, 105, 'tel', '01 60 00 00 00'),
(479, 105, '_tel', 'field_549c584fee81b'),
(480, 105, 'site_internet', ''),
(481, 105, '_site_internet', 'field_549c58cfee81e'),
(482, 105, '_edit_lock', '1432970614:1'),
(483, 106, '_menu_item_type', 'post_type'),
(484, 106, '_menu_item_menu_item_parent', '97'),
(485, 106, '_menu_item_object_id', '103'),
(486, 106, '_menu_item_object', 'page'),
(487, 106, '_menu_item_target', ''),
(488, 106, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(489, 106, '_menu_item_xfn', ''),
(490, 106, '_menu_item_url', ''),
(510, 109, '_menu_item_type', 'post_type'),
(511, 109, '_menu_item_menu_item_parent', '0'),
(512, 109, '_menu_item_object_id', '65'),
(513, 109, '_menu_item_object', 'page'),
(514, 109, '_menu_item_target', ''),
(515, 109, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(516, 109, '_menu_item_xfn', ''),
(517, 109, '_menu_item_url', ''),
(519, 106, 'menu-item-um_nav_public', '2'),
(520, 106, 'menu-item-um_nav_roles', 'a:2:{i:0;s:6:"member";i:1;s:5:"admin";}'),
(521, 109, 'menu-item-um_nav_public', '2'),
(522, 91, 'menu-item-um_nav_public', '1'),
(551, 113, '_edit_lock', '1426507490:1'),
(552, 113, '_edit_last', '1'),
(553, 113, '_wp_page_template', 'default'),
(554, 113, '_um_custom_access_settings', '0'),
(555, 113, '_um_accessible', '0'),
(556, 113, '_um_access_redirect2', ''),
(557, 113, '_um_access_redirect', ''),
(674, 128, '_edit_lock', '1430815970:1'),
(675, 128, '_edit_last', '1'),
(676, 128, '_wp_page_template', 'default'),
(677, 128, '_um_custom_access_settings', '0'),
(678, 128, '_um_accessible', '0'),
(679, 128, '_um_access_redirect2', ''),
(680, 128, '_um_access_redirect', ''),
(681, 130, '_eventorganiser_event_schedule', 'a:8:{s:7:"all_day";i:1;s:8:"schedule";s:4:"once";s:13:"schedule_meta";s:0:"";s:9:"frequency";i:1;s:7:"exclude";a:0:{}s:7:"include";a:0:{}s:12:"duration_str";s:57:"+0 year +0 month +0 days +23 hours +59 minutes +0 seconds";s:12:"_occurrences";a:1:{i:3;s:19:"2015-03-23 00:00:00";}}'),
(682, 130, '_eventorganiser_schedule_start_start', '2015-03-23 00:00:00'),
(683, 130, '_eventorganiser_schedule_start_finish', '2015-03-23 23:59:00'),
(684, 130, '_eventorganiser_schedule_last_start', '2015-03-23 00:00:00'),
(685, 130, '_eventorganiser_schedule_last_finish', '2015-03-23 23:59:00'),
(686, 130, '_edit_lock', '1432921669:1'),
(687, 130, '_edit_last', '1'),
(688, 130, '_um_custom_access_settings', '0'),
(689, 130, '_um_accessible', '0'),
(690, 130, '_um_access_redirect2', ''),
(691, 130, '_um_access_redirect', ''),
(692, 131, '_menu_item_type', 'post_type'),
(693, 131, '_menu_item_menu_item_parent', '7'),
(694, 131, '_menu_item_object_id', '128'),
(695, 131, '_menu_item_object', 'page'),
(696, 131, '_menu_item_target', ''),
(697, 131, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(698, 131, '_menu_item_xfn', ''),
(699, 131, '_menu_item_url', ''),
(701, 131, 'menu-item-um_nav_public', '2'),
(702, 131, 'menu-item-um_nav_roles', 'a:2:{i:0;s:6:"member";i:1;s:5:"admin";}'),
(703, 132, '_eventorganiser_event_schedule', 'a:8:{s:7:"all_day";i:1;s:8:"schedule";s:4:"once";s:13:"schedule_meta";s:0:"";s:9:"frequency";i:1;s:7:"exclude";a:0:{}s:7:"include";a:0:{}s:12:"duration_str";s:57:"+0 year +0 month +0 days +23 hours +59 minutes +0 seconds";s:12:"_occurrences";a:1:{i:4;s:19:"2015-03-23 00:00:00";}}'),
(704, 132, '_eventorganiser_schedule_start_start', '2015-03-23 00:00:00'),
(705, 132, '_eventorganiser_schedule_start_finish', '2015-03-23 23:59:00'),
(706, 132, '_eventorganiser_schedule_last_start', '2015-03-23 00:00:00'),
(707, 132, '_eventorganiser_schedule_last_finish', '2015-03-23 23:59:00'),
(708, 132, '_edit_lock', '1432921688:1'),
(709, 132, '_edit_last', '1'),
(710, 132, '_um_custom_access_settings', '0'),
(711, 132, '_um_accessible', '0'),
(712, 132, '_um_access_redirect2', ''),
(713, 132, '_um_access_redirect', ''),
(714, 133, '_edit_lock', '1427045228:1'),
(715, 133, '_edit_last', '1'),
(716, 134, '_edit_lock', '1430858141:1'),
(717, 134, '_edit_last', '1'),
(718, 134, '_wp_page_template', 'page-templates/add-post.php'),
(719, 134, '_um_custom_access_settings', '0'),
(720, 134, '_um_accessible', '0'),
(721, 134, '_um_access_redirect2', ''),
(722, 134, '_um_access_redirect', ''),
(723, 103, '_um_custom_access_settings', '0'),
(724, 103, '_um_accessible', '0'),
(725, 103, '_um_access_redirect2', ''),
(726, 103, '_um_access_redirect', ''),
(727, 139, '_edit_lock', '1430677182:1'),
(728, 140, '_menu_item_type', 'post_type'),
(729, 140, '_menu_item_menu_item_parent', '12'),
(730, 140, '_menu_item_object_id', '134'),
(731, 140, '_menu_item_object', 'page'),
(732, 140, '_menu_item_target', ''),
(733, 140, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(734, 140, '_menu_item_xfn', ''),
(735, 140, '_menu_item_url', ''),
(737, 140, 'menu-item-um_nav_public', '2'),
(738, 140, 'menu-item-um_nav_roles', 'a:2:{i:0;s:6:"member";i:1;s:5:"admin";}'),
(740, 143, 'president', 'Truc MUCHE'),
(741, 143, '_president', 'field_549c57f8ee819'),
(742, 143, 'email', 'jeunesseespoir@xxx.fr'),
(743, 143, '_email', 'field_549c58baee81d'),
(744, 143, 'adresse', 'Tartezoo'),
(745, 143, '_adresse', 'field_549c5815ee81a'),
(746, 143, 'tel', ''),
(747, 143, '_tel', 'field_549c584fee81b'),
(748, 143, 'site_internet', ''),
(749, 143, '_site_internet', 'field_549c58cfee81e'),
(750, 143, 'type_association', 'a:1:{i:0;s:2:"11";}'),
(751, 143, '_type_association', 'field_5506c28c94586'),
(752, 143, '_edit_lock', '1432968286:1'),
(753, 143, '_edit_last', '1'),
(754, 143, '_um_custom_access_settings', '0'),
(755, 143, '_um_accessible', '0'),
(756, 143, '_um_access_redirect2', ''),
(757, 143, '_um_access_redirect', ''),
(758, 4, '_eventorganiser_schedule_until', '2014-12-28 08:00:00'),
(760, 130, '_eventorganiser_schedule_until', '2015-03-23 00:00:00'),
(761, 132, '_eventorganiser_schedule_until', '2015-03-23 00:00:00'),
(780, 12, 'if_menu_enable', '0'),
(781, 140, 'if_menu_enable', '0'),
(782, 7, 'if_menu_enable', '0'),
(783, 131, 'if_menu_enable', '0'),
(784, 97, 'if_menu_enable', '0'),
(785, 106, 'if_menu_enable', '0'),
(786, 91, 'if_menu_enable', '1'),
(787, 109, 'if_menu_enable', '1'),
(788, 109, 'if_menu_condition_type', 'show'),
(789, 109, 'if_menu_condition', 'User is logged in'),
(792, 91, 'if_menu_condition_type', 'hide'),
(793, 91, 'if_menu_condition', 'User is logged in'),
(798, 4, '_eventorganiser_uid', '20150503T1732Z-1430674326.6805-EO-4-1@127.0.0.1'),
(800, 130, '_eventorganiser_uid', '20150503T1732Z-1430674326.9632-EO-130-1@127.0.0.1'),
(801, 132, '_eventorganiser_uid', '20150503T1732Z-1430674327.0367-EO-132-1@127.0.0.1'),
(802, 151, 'president', 'Moio'),
(803, 151, '_president', 'field_549c57f8ee819'),
(804, 151, 'email', 'bijour@fr.fr'),
(805, 151, '_email', 'field_549c58baee81d'),
(806, 151, 'adresse', '109 bv'),
(807, 151, '_adresse', 'field_549c5815ee81a'),
(808, 151, 'tel', ''),
(809, 151, '_tel', 'field_549c584fee81b'),
(810, 151, 'site_internet', ''),
(811, 151, '_site_internet', 'field_549c58cfee81e'),
(812, 151, 'type_association', ''),
(813, 151, '_type_association', 'field_5506c28c94586'),
(816, 155, '_edit_lock', '1432921851:1'),
(817, 155, '_edit_last', '7'),
(819, 157, '_edit_last', '7'),
(820, 157, 'president', 'sa'),
(821, 157, '_president', 'field_549c57f8ee819'),
(822, 157, 'email', 'za@f.fr'),
(823, 157, '_email', 'field_549c58baee81d'),
(824, 157, 'adresse', ''),
(825, 157, '_adresse', 'field_549c5815ee81a'),
(826, 157, 'tel', ''),
(827, 157, '_tel', 'field_549c584fee81b'),
(828, 157, 'site_internet', ''),
(829, 157, '_site_internet', 'field_549c58cfee81e'),
(830, 157, 'type_association', ''),
(831, 157, '_type_association', 'field_5506c28c94586'),
(832, 157, '_edit_lock', '1432968176:1'),
(833, 158, '_edit_lock', '1432968164:1'),
(834, 158, '_edit_last', '7'),
(835, 158, 'president', 'ssez'),
(836, 158, '_president', 'field_549c57f8ee819'),
(837, 158, 'email', 'saz@rfr.rf'),
(838, 158, '_email', 'field_549c58baee81d'),
(839, 158, 'adresse', ''),
(840, 158, '_adresse', 'field_549c5815ee81a'),
(841, 158, 'tel', ''),
(842, 158, '_tel', 'field_549c584fee81b'),
(843, 158, 'site_internet', ''),
(844, 158, '_site_internet', 'field_549c58cfee81e'),
(845, 158, 'type_association', ''),
(846, 158, '_type_association', 'field_5506c28c94586'),
(851, 160, '_edit_lock', '1430685316:1'),
(852, 160, '_edit_last', '1'),
(853, 162, '_edit_lock', '1430686047:1'),
(854, 162, '_edit_last', '1'),
(855, 162, '_wp_page_template', 'page-templates/edit-association.php'),
(856, 164, 'president', 'Michel'),
(857, 164, '_president', 'field_549c57f8ee819'),
(858, 164, 'email', 'michou@mcc.fr'),
(859, 164, '_email', 'field_549c58baee81d'),
(860, 164, 'adresse', ''),
(861, 164, '_adresse', 'field_549c5815ee81a'),
(862, 164, 'tel', ''),
(863, 164, '_tel', 'field_549c584fee81b'),
(864, 164, 'site_internet', ''),
(865, 164, '_site_internet', 'field_549c58cfee81e'),
(866, 164, 'type_association', ''),
(867, 164, '_type_association', 'field_5506c28c94586'),
(868, 151, '_edit_last', '1'),
(869, 151, '_edit_lock', '1432968187:1'),
(880, 168, '_edit_lock', '1430812933:1'),
(881, 168, '_edit_last', '1'),
(882, 169, '_edit_lock', '1430815866:1'),
(883, 169, '_edit_last', '1'),
(884, 169, '_wp_page_template', 'page-templates/edit-post.php'),
(886, 172, '_edit_lock', '1430815859:1'),
(887, 172, '_edit_last', '1'),
(888, 172, '_wp_page_template', 'page-templates/edit-event.php'),
(889, 43, 'type_association', ''),
(890, 43, '_type_association', 'field_5506c28c94586'),
(891, 174, '_edit_lock', '1430858465:1'),
(892, 176, '_eventorganiser_schedule_until', '2015-06-06 20:00:00'),
(893, 176, '_edit_lock', '1432921107:1'),
(894, 176, '_edit_last', '1'),
(895, 176, '_eventorganiser_event_schedule', 'a:8:{s:7:"all_day";i:0;s:8:"schedule";s:4:"once";s:13:"schedule_meta";s:0:"";s:9:"frequency";i:1;s:7:"exclude";a:0:{}s:7:"include";a:0:{}s:12:"duration_str";s:55:"+0 year +0 month +0 days +2 hours +0 minutes +0 seconds";s:12:"_occurrences";a:1:{i:5;s:19:"2015-06-06 20:00:00";}}'),
(896, 176, '_eventorganiser_schedule_start_start', '2015-06-06 20:00:00'),
(897, 176, '_eventorganiser_schedule_start_finish', '2015-06-06 22:00:00'),
(898, 176, '_eventorganiser_schedule_last_start', '2015-06-06 20:00:00'),
(899, 176, '_eventorganiser_schedule_last_finish', '2015-06-06 22:00:00'),
(900, 177, '_eventorganiser_schedule_until', '2015-05-30 10:00:00'),
(901, 177, '_edit_lock', '1432921632:1'),
(902, 177, '_edit_last', '1'),
(903, 177, '_eventorganiser_event_schedule', 'a:8:{s:7:"all_day";i:0;s:8:"schedule";s:4:"once";s:13:"schedule_meta";s:0:"";s:9:"frequency";i:1;s:7:"exclude";a:0:{}s:7:"include";a:0:{}s:12:"duration_str";s:55:"+0 year +0 month +0 days +7 hours +0 minutes +0 seconds";s:12:"_occurrences";a:1:{i:6;s:19:"2015-05-30 10:00:00";}}'),
(904, 177, '_eventorganiser_schedule_start_start', '2015-05-30 10:00:00'),
(905, 177, '_eventorganiser_schedule_start_finish', '2015-05-30 17:00:00'),
(906, 177, '_eventorganiser_schedule_last_start', '2015-05-30 10:00:00'),
(907, 177, '_eventorganiser_schedule_last_finish', '2015-05-30 17:00:00'),
(908, 130, '_wp_trash_meta_status', 'publish'),
(909, 130, '_wp_trash_meta_time', '1432921811'),
(910, 132, '_wp_trash_meta_status', 'publish'),
(911, 132, '_wp_trash_meta_time', '1432921830'),
(912, 155, '_wp_trash_meta_status', 'publish'),
(913, 155, '_wp_trash_meta_time', '1432921994'),
(914, 174, '_wp_trash_meta_status', 'draft'),
(915, 174, '_wp_trash_meta_time', '1432922003'),
(916, 158, '_wp_trash_meta_status', 'publish'),
(917, 158, '_wp_trash_meta_time', '1432968307'),
(918, 157, '_wp_trash_meta_status', 'publish'),
(919, 157, '_wp_trash_meta_time', '1432968318'),
(920, 151, '_wp_trash_meta_status', 'publish'),
(921, 151, '_wp_trash_meta_time', '1432968330'),
(922, 164, '_wp_trash_meta_status', 'draft'),
(923, 164, '_wp_trash_meta_time', '1432968338'),
(924, 105, '_edit_last', '1'),
(925, 105, 'type_association', ''),
(926, 105, '_type_association', 'field_5506c28c94586'),
(927, 143, '_wp_trash_meta_status', 'publish'),
(928, 143, '_wp_trash_meta_time', '1432968429');

-- --------------------------------------------------------

--
-- Structure de la table `crbass_posts`
--

CREATE TABLE IF NOT EXISTS `crbass_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`),
  KEY `post_name` (`post_name`(191))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=181 ;

--
-- Contenu de la table `crbass_posts`
--

INSERT INTO `crbass_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2014-12-24 18:35:51', '2014-12-24 18:35:51', 'Bienvenue dans WordPress. Ceci est votre premier article. Modifiez-le ou supprimez-le, puis lancez-vous ! N''hésitez-pas à laisser un commentaire pour rester en contact.', 'Bonjour tout le monde !', '', 'publish', 'open', 'open', '', 'bonjour-tout-le-monde', '', '', '2015-05-05 10:09:05', '2015-05-05 08:09:05', '', 0, 'http://127.0.0.1/corbeilassoc/?p=1', 0, 'post', '', 1),
(2, 1, '2014-12-24 18:35:51', '2014-12-24 18:35:51', 'Voici un exemple de page. Elle est différente d''un article de blog, en cela qu''elle restera à la même place, et s''affichera dans le menu de navigation de votre site (en fonction de votre thème). La plupart des gens commencent par écrire une page « À Propos » qui les présente aux visiteurs potentiels du site. Vous pourriez y écrire quelque chose de ce tenant :\r\n<blockquote>Bonjour ! Je suis un mécanicien qui aspire à devenir un acteur, et voici mon blog. J''habite à Bordeaux, j''ai un super chien baptisé Russell, et j''aime la vodka-ananas (ainsi que regarder la pluie tomber).</blockquote>\r\n...ou bien quelque chose comme ça :\r\n<blockquote>La société 123 Machin Truc a été créée en 1971, et n''a cessé de proposer au public des machins-trucs de qualité depuis lors. Située à Saint-Remy-en-Bouzemont-Saint-Genest-et-Isson, 123 Machin Truc emploie 2 000 personnes, et fabrique toutes sortes de bidules super pour la communauté bouzemontoise.</blockquote>\r\nÉtant donné que vous êtes un nouvel utilisateur de WordPress, vous devriez vous rendre sur votre <a href="http://127.0.0.1/corbeilassoc/wp-admin/">tableau de bord</a> pour effacer la présente page, et créer de nouvelles pages avec votre propre contenu. Amusez-vous bien !', 'Page d&rsquo;exemple', '', 'publish', 'closed', 'open', '', 'page-d-exemple', '', '', '2015-05-03 22:23:39', '2015-05-03 20:23:39', '', 0, 'http://127.0.0.1/corbeilassoc/?page_id=2', 0, 'page', '', 0),
(4, 1, '2014-12-24 21:06:53', '2014-12-24 21:06:53', 'Le marché de Corbeil-Essonnes regroupe plus de 140 commerçants, sur l’un des plus grands marchés du département.', 'Marché de Corbeil-Essonnes', '', 'publish', 'open', 'open', '', 'marche-de-corbeil-essonnes', '', '', '2014-12-24 21:06:53', '2014-12-24 21:06:53', '', 0, 'http://127.0.0.1/corbeilassoc/?post_type=event&#038;p=4', 0, 'event', '', 0),
(7, 1, '2014-12-24 21:32:56', '2014-12-24 21:32:56', '', 'Événements', '', 'publish', 'open', 'open', '', 'evenements', '', '', '2015-05-08 14:32:49', '2015-05-08 12:32:49', '', 0, 'http://127.0.0.1/corbeilassoc/evenements/', 3, 'nav_menu_item', '', 0),
(8, 1, '2014-12-25 14:17:27', '2014-12-25 14:17:27', 'Voici une liste des différents événements des associations de Corbeil-Essonnes.\r\n\r\n[eo_events]\r\n[eo_subscribe title="Subscribe with Google" type="google"]Google agenda[/eo_subscribe]\r\n[eo_fullcalendar timeformat=''H:i'']', 'Accueil', '', 'publish', 'closed', 'open', '', 'accueil', '', '', '2015-05-03 22:23:38', '2015-05-03 20:23:38', '', 0, 'http://127.0.0.1/corbeilassoc/?page_id=8', 0, 'page', '', 0),
(9, 1, '2014-12-25 14:17:27', '2014-12-25 14:17:27', 'Voici une liste des différents événements des associations de Corbeil-Essonnes.\r\n\r\n[eo_events]\r\n\r\n[eo_fullcalendar]\r\n\r\n[eo_subscribe]', 'Accueil', '', 'inherit', 'open', 'open', '', '8-revision-v1', '', '', '2014-12-25 14:17:27', '2014-12-25 14:17:27', '', 8, 'http://127.0.0.1/corbeilassoc/8-revision-v1/', 0, 'revision', '', 0),
(10, 1, '2014-12-25 14:20:01', '2014-12-25 14:20:01', '', 'Actualité', '', 'publish', 'closed', 'open', '', 'actualite', '', '', '2015-05-03 22:23:38', '2015-05-03 20:23:38', '', 0, 'http://127.0.0.1/corbeilassoc/?page_id=10', 0, 'page', '', 0),
(11, 1, '2014-12-25 14:20:01', '2014-12-25 14:20:01', '', 'Actualité', '', 'inherit', 'open', 'open', '', '10-revision-v1', '', '', '2014-12-25 14:20:01', '2014-12-25 14:20:01', '', 10, 'http://127.0.0.1/corbeilassoc/10-revision-v1/', 0, 'revision', '', 0),
(12, 1, '2014-12-25 15:23:06', '2014-12-25 14:23:06', ' ', '', '', 'publish', 'open', 'open', '', '12', '', '', '2015-05-08 14:32:48', '2015-05-08 12:32:48', '', 0, 'http://127.0.0.1/corbeilassoc/?p=12', 1, 'nav_menu_item', '', 0),
(14, 1, '2014-12-25 16:34:02', '2014-12-25 15:34:02', 'Voici une liste des différents événements des associations de Corbeil-Essonnes.\r\n\r\n[eo_events]\r\n\r\n[eo_subscribe]\r\n\r\n[eo_fullcalendar]', 'Accueil', '', 'inherit', 'open', 'open', '', '8-revision-v1', '', '', '2014-12-25 16:34:02', '2014-12-25 15:34:02', '', 8, 'http://127.0.0.1/corbeilassoc/8-revision-v1/', 0, 'revision', '', 0),
(15, 1, '2014-12-25 16:36:13', '2014-12-25 15:36:13', 'Voici une liste des différents événements des associations de Corbeil-Essonnes.\r\n\r\n[eo_events]\r\n<pre class="prettyprint prettyprinted"><code><span class="pun">[</span><span class="pln">eo_subscribe title</span><span class="pun">=</span><span class="str">"Subscribe with Google"</span><span class="pln"> type</span><span class="pun">=</span><span class="str">"google"</span><span class="pun">]</span> <span class="pun">&lt;</span><span class="pln">img src</span><span class="pun">=</span><span class="str">"//www.google.com/calendar/images/ext/gc_button1.gif?2adb9a"</span><span class="pln"> alt</span><span class="pun">=</span><span class="str">"0"</span><span class="pln"> border</span><span class="pun">=</span><span class="str">"0"</span><span class="pun">&gt;</span> <span class="pun">[/</span><span class="pln">eo_subscribe</span><span class="pun">]</span></code></pre>\r\n[eo_fullcalendar]', 'Accueil', '', 'inherit', 'open', 'open', '', '8-revision-v1', '', '', '2014-12-25 16:36:13', '2014-12-25 15:36:13', '', 8, 'http://127.0.0.1/corbeilassoc/8-revision-v1/', 0, 'revision', '', 0),
(16, 1, '2014-12-25 16:36:34', '2014-12-25 15:36:34', 'Voici une liste des différents événements des associations de Corbeil-Essonnes.\r\n\r\n[eo_events]\r\n[eo_subscribe title="Subscribe with Google" type="google"] <img src="//www.google.com/calendar/images/ext/gc_button1.gif?2adb9a" alt="0" border="0"> [/eo_subscribe]\r\n[eo_fullcalendar]', 'Accueil', '', 'inherit', 'open', 'open', '', '8-revision-v1', '', '', '2014-12-25 16:36:34', '2014-12-25 15:36:34', '', 8, 'http://127.0.0.1/corbeilassoc/8-revision-v1/', 0, 'revision', '', 0),
(17, 1, '2014-12-25 16:37:25', '2014-12-25 15:37:25', 'Voici une liste des différents événements des associations de Corbeil-Essonnes.\r\n\r\n[eo_events]\r\n[eo_subscribe title="Subscribe with Google" type="google"]Google agenda[/eo_subscribe]\r\n[eo_fullcalendar]', 'Accueil', '', 'inherit', 'open', 'open', '', '8-revision-v1', '', '', '2014-12-25 16:37:25', '2014-12-25 15:37:25', '', 8, 'http://127.0.0.1/corbeilassoc/8-revision-v1/', 0, 'revision', '', 0),
(18, 1, '2014-12-25 16:45:23', '2014-12-25 15:45:23', 'Voici une liste des différents événements des associations de Corbeil-Essonnes.\r\n\r\n[eo_events]\r\n[eo_subscribe title="Subscribe with Google" type="google"]Google agenda[/eo_subscribe]\r\n[eo_fullcalendar]\r\n[eo_fullcalendar headerLeft=''prev,next today'' headerCenter=''title'' headerRight=''month,agendaWeek'']', 'Accueil', '', 'inherit', 'open', 'open', '', '8-revision-v1', '', '', '2014-12-25 16:45:23', '2014-12-25 15:45:23', '', 8, 'http://127.0.0.1/corbeilassoc/8-revision-v1/', 0, 'revision', '', 0),
(19, 1, '2014-12-25 16:47:05', '2014-12-25 15:47:05', 'Voici une liste des différents événements des associations de Corbeil-Essonnes.\r\n\r\n[eo_events]\r\n[eo_subscribe title="Subscribe with Google" type="google"]Google agenda[/eo_subscribe]\r\n[eo_fullcalendar]\r\n[eo_fullcalendar headerLeft=''prev,next today'' headerCenter=''salut'' headerRight=''month,agendaWeek'']', 'Accueil', '', 'inherit', 'open', 'open', '', '8-revision-v1', '', '', '2014-12-25 16:47:05', '2014-12-25 15:47:05', '', 8, 'http://127.0.0.1/corbeilassoc/8-revision-v1/', 0, 'revision', '', 0),
(20, 1, '2014-12-25 17:41:33', '2014-12-25 16:41:33', 'Voici une liste des différents événements des associations de Corbeil-Essonnes.\n\n[eo_events]\n[eo_subscribe title="Subscribe with Google" type="google"]Google agenda[/eo_subscribe]\n[eo_fullcalendar]\n[eo_fullcalendar headerLeft=''prev,next today'' headerCenter=''title'' headerRight=''month,agendaWeek'' timeformat=''H\\hi'' titleformatweek=''j M Y'']', 'Accueil', '', 'inherit', 'open', 'open', '', '8-autosave-v1', '', '', '2014-12-25 17:41:33', '2014-12-25 16:41:33', '', 8, 'http://127.0.0.1/corbeilassoc/8-autosave-v1/', 0, 'revision', '', 0),
(21, 1, '2014-12-25 16:54:05', '2014-12-25 15:54:05', 'Voici une liste des différents événements des associations de Corbeil-Essonnes.\r\n\r\n[eo_events]\r\n[eo_subscribe title="Subscribe with Google" type="google"]Google agenda[/eo_subscribe]\r\n[eo_fullcalendar]\r\n[eo_fullcalendar headerLeft=''prev,next today'' headerCenter=''title'' headerRight=''month,agendaWeek'' timeformat=''H\\hi'']', 'Accueil', '', 'inherit', 'open', 'open', '', '8-revision-v1', '', '', '2014-12-25 16:54:05', '2014-12-25 15:54:05', '', 8, 'http://127.0.0.1/corbeilassoc/8-revision-v1/', 0, 'revision', '', 0),
(22, 1, '2014-12-25 16:54:36', '2014-12-25 15:54:36', 'Voici une liste des différents événements des associations de Corbeil-Essonnes.\r\n\r\n[eo_events]\r\n[eo_subscribe title="Subscribe with Google" type="google"]Google agenda[/eo_subscribe]\r\n[eo_fullcalendar]\r\n[eo_fullcalendar headerLeft=''prev,next today'' headerCenter=''title'' headerRight=''month,agendaWeek'' timeformat=''H:i'']', 'Accueil', '', 'inherit', 'open', 'open', '', '8-revision-v1', '', '', '2014-12-25 16:54:36', '2014-12-25 15:54:36', '', 8, 'http://127.0.0.1/corbeilassoc/8-revision-v1/', 0, 'revision', '', 0),
(23, 1, '2014-12-25 16:57:53', '2014-12-25 15:57:53', 'Voici une liste des différents événements des associations de Corbeil-Essonnes.\r\n\r\n[eo_events]\r\n[eo_subscribe title="Subscribe with Google" type="google"]Google agenda[/eo_subscribe]\r\n[eo_fullcalendar]\r\n[eo_fullcalendar headerLeft=''prev,next today'' headerCenter=''title'' headerRight=''month,agendaWeek'' timeformat=''H:i'' titleformatweek=''j m Y'']', 'Accueil', '', 'inherit', 'open', 'open', '', '8-revision-v1', '', '', '2014-12-25 16:57:53', '2014-12-25 15:57:53', '', 8, 'http://127.0.0.1/corbeilassoc/8-revision-v1/', 0, 'revision', '', 0),
(24, 1, '2014-12-25 17:41:55', '2014-12-25 16:41:55', 'Voici une liste des différents événements des associations de Corbeil-Essonnes.\r\n\r\n[eo_events]\r\n[eo_subscribe title="Subscribe with Google" type="google"]Google agenda[/eo_subscribe]\r\n[eo_fullcalendar]\r\n[eo_fullcalendar headerLeft=''prev,next today'' headerCenter=''title'' headerRight=''month,agendaWeek'' timeformat=''H\\hi'' titleformatweek=''j M Y'']', 'Accueil', '', 'inherit', 'open', 'open', '', '8-revision-v1', '', '', '2014-12-25 17:41:55', '2014-12-25 16:41:55', '', 8, 'http://127.0.0.1/corbeilassoc/8-revision-v1/', 0, 'revision', '', 0),
(25, 1, '2014-12-25 17:42:29', '2014-12-25 16:42:29', 'Voici une liste des différents événements des associations de Corbeil-Essonnes.\r\n\r\n[eo_events]\r\n[eo_subscribe title="Subscribe with Google" type="google"]Google agenda[/eo_subscribe]\r\n[eo_fullcalendar]\r\n[eo_fullcalendar headerLeft=''prev,next today'' headerCenter=''title'' headerRight=''month,agendaWeek'' timeformat=''H\\h\\i'' titleformatweek=''j M Y'']', 'Accueil', '', 'inherit', 'open', 'open', '', '8-revision-v1', '', '', '2014-12-25 17:42:29', '2014-12-25 16:42:29', '', 8, 'http://127.0.0.1/corbeilassoc/8-revision-v1/', 0, 'revision', '', 0),
(26, 1, '2014-12-25 17:42:55', '2014-12-25 16:42:55', 'Voici une liste des différents événements des associations de Corbeil-Essonnes.\r\n\r\n[eo_events]\r\n[eo_subscribe title="Subscribe with Google" type="google"]Google agenda[/eo_subscribe]\r\n[eo_fullcalendar]\r\n[eo_fullcalendar headerLeft=''prev,next today'' headerCenter=''title'' headerRight=''month,agendaWeek'' timeformat=''H:i'' titleformatweek=''j M Y'']', 'Accueil', '', 'inherit', 'open', 'open', '', '8-revision-v1', '', '', '2014-12-25 17:42:55', '2014-12-25 16:42:55', '', 8, 'http://127.0.0.1/corbeilassoc/8-revision-v1/', 0, 'revision', '', 0),
(27, 1, '2014-12-25 17:43:14', '2014-12-25 16:43:14', 'Voici une liste des différents événements des associations de Corbeil-Essonnes.\r\n\r\n[eo_events]\r\n[eo_subscribe title="Subscribe with Google" type="google"]Google agenda[/eo_subscribe]\r\n[eo_fullcalendar]\r\n[eo_fullcalendar headerLeft=''prev,next today'' headerCenter=''title'' headerRight=''month,agendaWeek'' timeformat=''H:i'']', 'Accueil', '', 'inherit', 'open', 'open', '', '8-revision-v1', '', '', '2014-12-25 17:43:14', '2014-12-25 16:43:14', '', 8, 'http://127.0.0.1/corbeilassoc/8-revision-v1/', 0, 'revision', '', 0),
(28, 1, '2014-12-25 17:43:40', '2014-12-25 16:43:40', 'Voici une liste des différents événements des associations de Corbeil-Essonnes.\r\n\r\n[eo_events]\r\n[eo_subscribe title="Subscribe with Google" type="google"]Google agenda[/eo_subscribe]\r\n[eo_fullcalendar timeformat=''H:i'']', 'Accueil', '', 'inherit', 'open', 'open', '', '8-revision-v1', '', '', '2014-12-25 17:43:40', '2014-12-25 16:43:40', '', 8, 'http://127.0.0.1/corbeilassoc/8-revision-v1/', 0, 'revision', '', 0),
(29, 1, '2014-12-25 18:26:05', '2014-12-25 17:26:05', 'Actualité', 'Actualité', '', 'inherit', 'open', 'open', '', '10-revision-v1', '', '', '2014-12-25 18:26:05', '2014-12-25 17:26:05', '', 10, 'http://127.0.0.1/corbeilassoc/10-revision-v1/', 0, 'revision', '', 0),
(30, 1, '2014-12-25 18:26:40', '2014-12-25 17:26:40', '', 'Actualité', '', 'inherit', 'open', 'open', '', '10-revision-v1', '', '', '2014-12-25 18:26:40', '2014-12-25 17:26:40', '', 10, 'http://127.0.0.1/corbeilassoc/10-revision-v1/', 0, 'revision', '', 0),
(36, 1, '2014-12-25 19:31:06', '2014-12-25 18:31:06', 'a:6:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:9:"post_type";s:8:"operator";s:2:"==";s:5:"value";s:11:"association";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";s:0:"";}', 'Champs Association', 'champs-association', 'publish', 'closed', 'closed', '', 'group_549c576c510bc', '', '', '2015-03-16 12:47:55', '2015-03-16 11:47:55', '', 0, 'http://127.0.0.1/corbeilassoc/?post_type=acf-field-group&#038;p=36', 0, 'acf-field-group', '', 0),
(37, 1, '2014-12-25 19:35:12', '2014-12-25 18:35:12', 'a:11:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:1;s:17:"conditional_logic";i:0;s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";s:8:"readonly";i:0;s:8:"disabled";i:0;}', 'Président', 'president', 'publish', 'open', 'open', '', 'field_549c57f8ee819', '', '', '2014-12-25 19:35:12', '2014-12-25 18:35:12', '', 36, 'http://127.0.0.1/corbeilassoc/?post_type=acf-field&p=37', 0, 'acf-field', '', 0),
(38, 1, '2014-12-25 19:35:12', '2014-12-25 18:35:12', 'a:11:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";s:8:"readonly";i:0;s:8:"disabled";i:0;}', 'Adresse', 'adresse', 'publish', 'open', 'open', '', 'field_549c5815ee81a', '', '', '2015-03-16 12:10:30', '2015-03-16 11:10:30', '', 36, 'http://127.0.0.1/corbeilassoc/?post_type=acf-field&#038;p=38', 2, 'acf-field', '', 0),
(39, 1, '2014-12-25 19:35:12', '2014-12-25 18:35:12', 'a:11:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";s:8:"readonly";i:0;s:8:"disabled";i:0;}', 'Tel', 'tel', 'publish', 'open', 'open', '', 'field_549c584fee81b', '', '', '2015-03-16 12:10:30', '2015-03-16 11:10:30', '', 36, 'http://127.0.0.1/corbeilassoc/?post_type=acf-field&#038;p=39', 3, 'acf-field', '', 0),
(40, 1, '2014-12-25 19:35:12', '2014-12-25 18:35:12', 'a:8:{s:4:"type";s:5:"email";s:12:"instructions";s:0:"";s:8:"required";i:1;s:17:"conditional_logic";i:0;s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";}', 'Adresse mail', 'email', 'publish', 'open', 'open', '', 'field_549c58baee81d', '', '', '2015-03-16 12:10:30', '2015-03-16 11:10:30', '', 36, 'http://127.0.0.1/corbeilassoc/?post_type=acf-field&#038;p=40', 1, 'acf-field', '', 0),
(41, 1, '2014-12-25 19:35:12', '2014-12-25 18:35:12', 'a:6:{s:4:"type";s:3:"url";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";}', 'Site internet', 'site_internet', 'publish', 'open', 'open', '', 'field_549c58cfee81e', '', '', '2014-12-25 19:35:12', '2014-12-25 18:35:12', '', 36, 'http://127.0.0.1/corbeilassoc/?post_type=acf-field&p=41', 4, 'acf-field', '', 0),
(43, 1, '2014-12-25 19:55:28', '2014-12-25 18:55:28', 'L’association des étoiles montantes a pour but d’aider les jeunes prometteurs de la ville de Corbeil-Essonnes ayant des difficultés scolaires et relationnelles. Nous souhaitons leur offrir un cadre d’éducation stable, avec des éducateurs expérimentés pour une prise en charge au cas par cas.\r\n\r\nAinsi, la réussite scolaire de nos jeunes espoirs aboutira à de meilleures possibilités d’avenir pour eux et cela constituera un grand pas vers la réussite éducative de la ville de Corbeil-Essonnes.', 'Association des Etoiles Montantes de Corbeil-Essonnes', '', 'publish', 'closed', 'closed', '', 'association-des-etoiles-montantes-de-corbeil-essonnes', '', '', '2015-05-05 21:33:53', '2015-05-05 19:33:53', '', 0, 'http://127.0.0.1/corbeilassoc/?post_type=association&#038;p=43', 0, 'association', '', 0),
(47, 1, '2014-12-25 20:38:17', '2014-12-25 19:38:17', '', 'Les Associations', '', 'publish', 'closed', 'open', '', 'les-associations', '', '', '2015-05-03 22:23:38', '2015-05-03 20:23:38', '', 0, 'http://127.0.0.1/corbeilassoc/?page_id=47', 0, 'page', '', 0),
(48, 1, '2014-12-25 20:38:17', '2014-12-25 19:38:17', '', 'Associations', '', 'inherit', 'open', 'open', '', '47-revision-v1', '', '', '2014-12-25 20:38:17', '2014-12-25 19:38:17', '', 47, 'http://127.0.0.1/corbeilassoc/47-revision-v1/', 0, 'revision', '', 0),
(49, 1, '2014-12-25 20:40:34', '2014-12-25 19:40:34', '', 'Les Associations', '', 'inherit', 'open', 'open', '', '47-revision-v1', '', '', '2014-12-25 20:40:34', '2014-12-25 19:40:34', '', 47, 'http://127.0.0.1/corbeilassoc/47-revision-v1/', 0, 'revision', '', 0),
(50, 1, '2014-12-26 10:51:13', '2014-12-26 09:51:13', 'Trop cool mec', 'Aussi cool que ton front', '', 'publish', 'closed', 'closed', '', 'aussi-cool-que-ton-front', '', '', '2014-12-26 10:51:13', '2014-12-26 09:51:13', '', 0, 'http://127.0.0.1/corbeilassoc/?post_type=cool&#038;p=50', 0, 'cool', '', 0),
(53, 1, '2015-01-14 11:41:31', '2015-01-14 10:41:31', 'Voici un exemple de page. Elle est différente d''un article de blog, en cela qu''elle restera à la même place, et s''affichera dans le menu de navigation de votre site (en fonction de votre thème). La plupart des gens commencent par écrire une page « À Propos » qui les présente aux visiteurs potentiels du site. Vous pourriez y écrire quelque chose de ce tenant :\n<blockquote>Bonjour ! Je suis un mécanicien qui aspire à devenir un acteur, et voici mon blog. J''habite à Bordeaux, j''ai un super chien baptisé Russell, et j''aime la vodka-ananas (ainsi que regarder la pluie tomber).</blockquote>\n...ou bien quelque chose comme ça :\n<blockquote>La société 123 Machin Truc a été créée en 1971, et n''a cessé de proposer au public des machins-trucs de qualité depuis lors. Située à Saint-Remy-en-Bouzemont-Saint-Genest-et-Isson, 123 Machin Truc emploie 2 000 personnes, et fabrique toutes sortes de bidules super pour la communauté bouzemontoise.</blockquote>\nÉtant donné que vous êtes un nouvel utilisateur de WordPress, vous devriez vous rendre sur votre <a href="http://127.0.0.1/corbeilassoc/wp-admin/">tableau de bord</a> pour effacer la présente page, et créer de nouvelles pages avec votre propre contenu. Amusez-vous bien !', 'Page d&rsquo;exemple', '', 'inherit', 'open', 'open', '', '2-autosave-v1', '', '', '2015-01-14 11:41:31', '2015-01-14 10:41:31', '', 2, 'http://127.0.0.1/corbeilassoc/2-autosave-v1/', 0, 'revision', '', 0),
(56, 1, '2015-01-14 11:47:24', '2015-01-14 10:47:24', 'Voici un exemple de page. Elle est différente d''un article de blog, en cela qu''elle restera à la même place, et s''affichera dans le menu de navigation de votre site (en fonction de votre thème). La plupart des gens commencent par écrire une page « À Propos » qui les présente aux visiteurs potentiels du site. Vous pourriez y écrire quelque chose de ce tenant :\r\n<blockquote>Bonjour ! Je suis un mécanicien qui aspire à devenir un acteur, et voici mon blog. J''habite à Bordeaux, j''ai un super chien baptisé Russell, et j''aime la vodka-ananas (ainsi que regarder la pluie tomber).</blockquote>\r\n...ou bien quelque chose comme ça :\r\n<blockquote>La société 123 Machin Truc a été créée en 1971, et n''a cessé de proposer au public des machins-trucs de qualité depuis lors. Située à Saint-Remy-en-Bouzemont-Saint-Genest-et-Isson, 123 Machin Truc emploie 2 000 personnes, et fabrique toutes sortes de bidules super pour la communauté bouzemontoise.</blockquote>\r\nÉtant donné que vous êtes un nouvel utilisateur de WordPress, vous devriez vous rendre sur votre <a href="http://127.0.0.1/corbeilassoc/wp-admin/">tableau de bord</a> pour effacer la présente page, et créer de nouvelles pages avec votre propre contenu. Amusez-vous bien !', 'Page d&rsquo;exemple', '', 'inherit', 'open', 'open', '', '2-revision-v1', '', '', '2015-01-14 11:47:24', '2015-01-14 10:47:24', '', 2, 'http://127.0.0.1/corbeilassoc/2-revision-v1/', 0, 'revision', '', 0),
(61, 1, '2015-03-14 14:07:04', '2015-03-14 13:07:04', '', 'Inscription', '', 'publish', 'closed', 'closed', '', 'default-registration', '', '', '2015-03-14 16:00:26', '2015-03-14 15:00:26', '', 0, 'http://127.0.0.1/corbeilassoc/um_form/default-registration/', 0, 'um_form', '', 0),
(62, 1, '2015-03-14 14:07:04', '2015-03-14 13:07:04', '', 'Connexion', '', 'publish', 'closed', 'closed', '', 'default-login', '', '', '2015-03-14 16:02:54', '2015-03-14 15:02:54', '', 0, 'http://127.0.0.1/corbeilassoc/um_form/default-login/', 0, 'um_form', '', 0),
(63, 1, '2015-03-14 14:07:04', '2015-03-14 13:07:04', '', 'Profil', '', 'publish', 'closed', 'closed', '', 'default-profile', '', '', '2015-03-14 16:02:12', '2015-03-14 15:02:12', '', 0, 'http://127.0.0.1/corbeilassoc/um_form/default-profile/', 0, 'um_form', '', 0),
(64, 1, '2015-03-14 14:07:06', '2015-03-14 13:07:06', '', 'Membres', '', 'publish', 'closed', 'closed', '', 'members', '', '', '2015-03-14 15:18:13', '2015-03-14 14:18:13', '', 0, 'http://127.0.0.1/corbeilassoc/um_directory/members/', 0, 'um_directory', '', 0),
(65, 1, '2015-03-14 14:07:07', '2015-03-14 13:07:07', '', 'Profil', '', 'publish', 'closed', 'open', '', 'profil', '', '', '2015-05-05 08:18:15', '2015-05-05 06:18:15', '', 0, 'http://127.0.0.1/corbeilassoc/user/', 0, 'page', '', 0),
(66, 1, '2015-03-14 14:07:08', '2015-03-14 13:07:08', '[ultimatemember form_id=63]', 'User', '', 'inherit', 'open', 'open', '', '65-revision-v1', '', '', '2015-03-14 14:07:08', '2015-03-14 13:07:08', '', 65, 'http://127.0.0.1/corbeilassoc/65-revision-v1/', 0, 'revision', '', 0),
(67, 1, '2015-03-14 14:07:08', '2015-03-14 13:07:08', '[loginform]', 'Connexion', '', 'publish', 'closed', 'open', '', 'connexion', '', '', '2015-05-03 22:23:38', '2015-05-03 20:23:38', '', 0, 'http://127.0.0.1/corbeilassoc/login/', 0, 'page', '', 0),
(68, 1, '2015-03-14 14:07:09', '2015-03-14 13:07:09', '[ultimatemember form_id=62]', 'Login', '', 'inherit', 'open', 'open', '', '67-revision-v1', '', '', '2015-03-14 14:07:09', '2015-03-14 13:07:09', '', 67, 'http://127.0.0.1/corbeilassoc/67-revision-v1/', 0, 'revision', '', 0),
(69, 1, '2015-03-14 14:07:09', '2015-03-14 13:07:09', 'Remplissez le formulaire suivante afin de vous inscrire sur le site.\r\n[registerform user_role=''association_owner'']', 'Inscription', '', 'publish', 'closed', 'open', '', 'inscription', '', '', '2015-05-03 22:23:38', '2015-05-03 20:23:38', '', 0, 'http://127.0.0.1/corbeilassoc/register/', 0, 'page', '', 0),
(70, 1, '2015-03-14 14:07:10', '2015-03-14 13:07:10', '[ultimatemember form_id=61]', 'Register', '', 'inherit', 'open', 'open', '', '69-revision-v1', '', '', '2015-03-14 14:07:10', '2015-03-14 13:07:10', '', 69, 'http://127.0.0.1/corbeilassoc/69-revision-v1/', 0, 'revision', '', 0),
(79, 1, '2015-03-14 14:07:15', '2015-03-14 13:07:15', '', 'Admin', '', 'publish', 'open', 'open', '', 'admin', '', '', '2015-03-14 14:07:15', '2015-03-14 13:07:15', '', 0, 'http://127.0.0.1/corbeilassoc/um_role/admin/', 0, 'um_role', '', 0),
(80, 1, '2015-03-14 14:07:17', '2015-03-14 13:07:17', '', 'Member', '', 'publish', 'open', 'open', '', 'member', '', '', '2015-03-14 14:07:17', '2015-03-14 13:07:17', '', 0, 'http://127.0.0.1/corbeilassoc/um_role/member/', 0, 'um_role', '', 0),
(81, 1, '2015-03-14 14:28:21', '2015-03-14 13:28:21', '', 'Inscription', '', 'inherit', 'open', 'open', '', '61-autosave-v1', '', '', '2015-03-14 14:28:21', '2015-03-14 13:28:21', '', 61, 'http://127.0.0.1/corbeilassoc/61-autosave-v1/', 0, 'revision', '', 0),
(82, 1, '2015-03-14 14:32:32', '2015-03-14 13:32:32', '', 'Connexion', '', 'inherit', 'open', 'open', '', '62-autosave-v1', '', '', '2015-03-14 14:32:32', '2015-03-14 13:32:32', '', 62, 'http://127.0.0.1/corbeilassoc/62-autosave-v1/', 0, 'revision', '', 0),
(83, 1, '2015-03-14 14:34:48', '2015-03-14 13:34:48', '[ultimatemember form_id=61]', 'Inscription', '', 'inherit', 'open', 'open', '', '69-revision-v1', '', '', '2015-03-14 14:34:48', '2015-03-14 13:34:48', '', 69, 'http://127.0.0.1/corbeilassoc/69-revision-v1/', 0, 'revision', '', 0),
(85, 1, '2015-03-14 14:38:54', '2015-03-14 13:38:54', '[ultimatemember form_id=62]', 'Connexion', '', 'inherit', 'open', 'open', '', '67-revision-v1', '', '', '2015-03-14 14:38:54', '2015-03-14 13:38:54', '', 67, 'http://127.0.0.1/corbeilassoc/67-revision-v1/', 0, 'revision', '', 0),
(87, 1, '2015-03-14 14:45:32', '2015-03-14 13:45:32', '[ultimatemember form_id=63]', 'Utilisateur', '', 'inherit', 'open', 'open', '', '65-revision-v1', '', '', '2015-03-14 14:45:32', '2015-03-14 13:45:32', '', 65, 'http://127.0.0.1/corbeilassoc/65-revision-v1/', 0, 'revision', '', 0),
(91, 1, '2015-03-14 14:55:57', '2015-03-14 13:55:57', ' ', '', '', 'publish', 'open', 'open', '', '91', '', '', '2015-05-08 14:32:50', '2015-05-08 12:32:50', '', 0, 'http://127.0.0.1/corbeilassoc/?p=91', 7, 'nav_menu_item', '', 0),
(97, 1, '2015-03-14 16:07:34', '2015-03-14 15:07:34', '', 'Associations', '', 'publish', 'open', 'open', '', 'associations', '', '', '2015-05-08 14:32:50', '2015-05-08 12:32:50', '', 0, 'http://127.0.0.1/corbeilassoc/?p=97', 5, 'nav_menu_item', '', 0),
(100, 1, '2015-03-16 11:04:22', '2015-03-16 10:04:22', '', '180x180', '', 'inherit', 'open', 'open', '', '180x180', '', '', '2015-03-16 11:04:22', '2015-03-16 10:04:22', '', 43, 'http://127.0.0.1/corbeilassoc/wp-content/uploads/2014/12/180x180.png', 0, 'attachment', 'image/png', 0),
(101, 1, '2015-03-16 11:05:34', '2015-03-16 10:05:34', '', 'logo', '', 'inherit', 'open', 'open', '', 'logo', '', '', '2015-03-16 11:05:49', '2015-03-16 10:05:49', '', 43, 'http://127.0.0.1/corbeilassoc/wp-content/uploads/2014/12/logo.png', 0, 'attachment', 'image/png', 0),
(102, 1, '2015-03-16 11:08:01', '2015-03-16 10:08:01', '', 'Cover twitter PP 2', '', 'inherit', 'open', 'open', '', 'cover-twitter-pp-2', '', '', '2015-03-16 11:08:01', '2015-03-16 10:08:01', '', 43, 'http://127.0.0.1/corbeilassoc/wp-content/uploads/2014/12/Cover-twitter-PP-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(103, 1, '2015-03-16 11:24:09', '2015-03-16 10:24:09', 'Vous pouvez ajouter votre association au répertoire du site en remplissant le formulaire suivant.', 'Ajouter mon association', '', 'publish', 'open', 'open', '', 'ajouter-mon-association', '', '', '2015-05-03 22:54:07', '2015-05-03 20:54:07', '', 0, 'http://127.0.0.1/corbeilassoc/?page_id=103', 0, 'page', '', 0),
(104, 1, '2015-03-16 11:24:09', '2015-03-16 10:24:09', '', 'Ajouter une association', '', 'inherit', 'open', 'open', '', '103-revision-v1', '', '', '2015-03-16 11:24:09', '2015-03-16 10:24:09', '', 103, 'http://127.0.0.1/corbeilassoc/103-revision-v1/', 0, 'revision', '', 0),
(105, 1, '2015-05-30 08:46:51', '2015-05-30 06:46:51', 'Association qui milite contre l''illettrisme et l''alphabétisation des étrangers en France.\r\n\r\nPrésent dans le quartier de Montconseil et la Nacelle.', 'Renaissance et Culture', '', 'publish', 'closed', 'closed', '', 'renaissance-et-culture', '', '', '2015-05-30 08:46:51', '2015-05-30 06:46:51', '', 0, 'http://127.0.0.1/corbeilassoc/?post_type=association&#038;p=105', 0, 'association', '', 0),
(106, 1, '2015-03-16 12:41:55', '2015-03-16 11:41:55', ' ', '', '', 'publish', 'open', 'open', '', '106', '', '', '2015-05-08 14:32:50', '2015-05-08 12:32:50', '', 0, 'http://127.0.0.1/corbeilassoc/?p=106', 6, 'nav_menu_item', '', 0),
(109, 1, '2015-03-16 12:41:57', '2015-03-16 11:41:57', ' ', '', '', 'publish', 'open', 'open', '', 'profil', '', '', '2015-05-08 14:32:50', '2015-05-08 12:32:50', '', 0, 'http://127.0.0.1/corbeilassoc/?p=109', 8, 'nav_menu_item', '', 0),
(110, 1, '2015-03-16 12:47:55', '2015-03-16 11:47:55', 'a:10:{s:4:"type";s:8:"taxonomy";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:8:"taxonomy";s:8:"category";s:10:"field_type";s:8:"checkbox";s:10:"allow_null";i:0;s:15:"load_save_terms";i:0;s:13:"return_format";s:6:"object";s:8:"multiple";i:0;}', 'Type d''Association', 'type_association', 'publish', 'open', 'open', '', 'field_5506c28c94586', '', '', '2015-03-16 12:47:55', '2015-03-16 11:47:55', '', 36, 'http://127.0.0.1/corbeilassoc/?post_type=acf-field&p=110', 5, 'acf-field', '', 0),
(113, 1, '2015-03-16 13:01:38', '2015-03-16 12:01:38', 'Merci d''avoir ajouté votre association sur le site.\r\n\r\nVeuillez patienter pendant qu''un administrateur valide votre demande.', 'Association à l''état de brouillon', '', 'publish', 'closed', 'open', '', 'association-en-brouillon', '', '', '2015-05-03 22:23:38', '2015-05-03 20:23:38', '', 103, 'http://127.0.0.1/corbeilassoc/?page_id=113', 0, 'page', '', 0),
(114, 1, '2015-03-16 13:01:38', '2015-03-16 12:01:38', 'Merci d''avoir ajouté votre association sur le site.\r\n\r\nVeuillez patienter pendant qu''un administrateur valide votre demande.', 'Association en brouillon', '', 'inherit', 'open', 'open', '', '113-revision-v1', '', '', '2015-03-16 13:01:38', '2015-03-16 12:01:38', '', 113, 'http://127.0.0.1/corbeilassoc/113-revision-v1/', 0, 'revision', '', 0),
(115, 1, '2015-03-16 13:01:57', '2015-03-16 12:01:57', 'Merci d''avoir ajouté votre association sur le site.\r\n\r\nVeuillez patienter pendant qu''un administrateur valide votre demande.', 'Association à l''état de brouillon', '', 'inherit', 'open', 'open', '', '113-revision-v1', '', '', '2015-03-16 13:01:57', '2015-03-16 12:01:57', '', 113, 'http://127.0.0.1/corbeilassoc/113-revision-v1/', 0, 'revision', '', 0),
(126, 1, '2015-03-21 14:54:15', '2015-03-21 13:54:15', '[wysija_page]', 'Confirmation d’inscription', '', 'publish', 'open', 'open', '', 'subscriptions', '', '', '2015-03-21 14:54:15', '2015-03-21 13:54:15', '', 0, 'http://127.0.0.1/corbeilassoc/?wysijap=subscriptions', 0, 'wysijap', '', 0),
(128, 1, '2015-03-22 18:10:54', '2015-03-22 17:10:54', 'Vous pouvez ajouter un événement en remplissant le formulaire suivant.\r\n\r\n[my_event_form]', 'Ajouter un événement', '', 'publish', 'closed', 'open', '', 'ajouter-un-evenement', '', '', '2015-05-03 22:23:38', '2015-05-03 20:23:38', '', 0, 'http://127.0.0.1/corbeilassoc/?page_id=128', 0, 'page', '', 0),
(129, 1, '2015-03-22 18:10:54', '2015-03-22 17:10:54', 'Vous pouvez ajouter un événement en remplissant le formulaire suivant.\r\n\r\n[my_event_form]', 'Ajouter un événement', '', 'inherit', 'open', 'open', '', '128-revision-v1', '', '', '2015-03-22 18:10:54', '2015-03-22 17:10:54', '', 128, 'http://127.0.0.1/corbeilassoc/128-revision-v1/', 0, 'revision', '', 0),
(130, 1, '2015-03-22 18:15:08', '2015-03-22 17:15:08', 'Dernier match de la saison afin de connaitre le futur champion d''Angleterre !!', 'Match de Chelsea', '', 'trash', 'open', 'open', '', 'match-de-chelsea', '', '', '2015-05-29 19:50:11', '2015-05-29 17:50:11', '', 0, 'http://127.0.0.1/corbeilassoc/?p=130', 0, 'event', '', 0),
(131, 1, '2015-03-22 18:16:23', '2015-03-22 17:16:23', ' ', '', '', 'publish', 'open', 'open', '', '131', '', '', '2015-05-08 14:32:49', '2015-05-08 12:32:49', '', 0, 'http://127.0.0.1/corbeilassoc/?p=131', 4, 'nav_menu_item', '', 0),
(132, 1, '2015-03-22 18:18:15', '2015-03-22 17:18:15', 'Match de l''année en Espagne avec la BBC face à la MSN', 'Classico Real Barca', '', 'trash', 'open', 'open', '', 'classico-real-barca', '', '', '2015-05-29 19:50:30', '2015-05-29 17:50:30', '', 0, 'http://127.0.0.1/corbeilassoc/?p=132', 0, 'event', '', 0),
(133, 1, '2015-05-03 22:23:38', '0000-00-00 00:00:00', '', 'Ajouter un article', '', 'draft', 'closed', 'open', '', '', '', '', '2015-05-03 22:23:38', '2015-05-03 20:23:38', '', 0, 'http://127.0.0.1/corbeilassoc/?page_id=133', 0, 'page', '', 0),
(134, 1, '2015-03-22 18:28:32', '2015-03-22 17:28:32', 'Vous avez la possibilité d''ajouter un article afin de nous faire part de votre actualité.', 'Ajouter un article', '', 'publish', 'closed', 'open', '', 'ajouter-un-article', '', '', '2015-05-05 22:37:54', '2015-05-05 20:37:54', '', 0, 'http://127.0.0.1/corbeilassoc/?page_id=134', 0, 'page', '', 0),
(135, 1, '2015-03-22 18:28:32', '2015-03-22 17:28:32', 'Vous avez la possibilité d''ajouter un article afin de nous faire part de votre actualité.', 'Ajouter un article', '', 'inherit', 'open', 'open', '', '134-revision-v1', '', '', '2015-03-22 18:28:32', '2015-03-22 17:28:32', '', 134, 'http://127.0.0.1/corbeilassoc/134-revision-v1/', 0, 'revision', '', 0),
(136, 1, '2015-03-22 18:31:43', '2015-03-22 17:31:43', 'Vous avez la possibilité d''ajouter un article afin de nous faire part de votre actualité.', 'Ajouter un article', '', 'inherit', 'open', 'open', '', '134-autosave-v1', '', '', '2015-03-22 18:31:43', '2015-03-22 17:31:43', '', 134, 'http://127.0.0.1/corbeilassoc/134-autosave-v1/', 0, 'revision', '', 0),
(137, 1, '2015-03-22 18:48:28', '2015-03-22 17:48:28', 'Vous pouveze', 'Ajouter une association', '', 'inherit', 'open', 'open', '', '103-autosave-v1', '', '', '2015-03-22 18:48:28', '2015-03-22 17:48:28', '', 103, 'http://127.0.0.1/corbeilassoc/103-autosave-v1/', 0, 'revision', '', 0),
(138, 1, '2015-03-22 18:49:17', '2015-03-22 17:49:17', 'Vous pouvez ajouter votre association au répertoire du site en remplissant le formulaire suivant.', 'Ajouter une association', '', 'inherit', 'open', 'open', '', '103-revision-v1', '', '', '2015-03-22 18:49:17', '2015-03-22 17:49:17', '', 103, 'http://127.0.0.1/corbeilassoc/103-revision-v1/', 0, 'revision', '', 0),
(139, 1, '2015-03-22 18:50:00', '0000-00-00 00:00:00', 'Lutte contre les trucs pas bien de l''éducation nationale', 'Gros blocus au lycée', '', 'draft', 'open', 'open', '', '', '', '', '2015-03-22 18:50:00', '0000-00-00 00:00:00', '', 0, 'http://127.0.0.1/corbeilassoc/?p=139', 0, 'post', '', 0),
(140, 1, '2015-03-22 18:51:30', '2015-03-22 17:51:30', ' ', '', '', 'publish', 'open', 'open', '', '140', '', '', '2015-05-08 14:32:49', '2015-05-08 12:32:49', '', 0, 'http://127.0.0.1/corbeilassoc/?p=140', 2, 'nav_menu_item', '', 0),
(143, 1, '2015-03-25 15:45:23', '2015-03-25 14:45:23', 'Jeune association issu des tarterêts qui filme régulièrement des actions politiques et associatives sur la ville de Corbeil-Essonnes.\r\n\r\nkjij oko k', 'Jeunesse Espoir', '', 'trash', 'closed', 'closed', '', 'jeunesse-espoir', '', '', '2015-05-30 08:47:09', '2015-05-30 06:47:09', '', 0, 'http://127.0.0.1/corbeilassoc/?post_type=association&#038;p=143', 0, 'association', '', 0),
(146, 1, '2015-05-02 22:27:38', '2015-05-02 21:27:38', 'Remplissez le formulaire suivante afin de vous inscrire sur le site.\r\n[registerform user_role=''association_owner'']', 'Inscription', '', 'inherit', 'open', 'open', '', '69-revision-v1', '', '', '2015-05-02 22:27:38', '2015-05-02 21:27:38', '', 69, 'http://127.0.0.1/corbeilassoc/69-revision-v1/', 0, 'revision', '', 0),
(148, 1, '2015-05-02 22:28:04', '2015-05-02 21:28:04', '[loginform]', 'Connexion', '', 'inherit', 'open', 'open', '', '67-revision-v1', '', '', '2015-05-02 22:28:04', '2015-05-02 21:28:04', '', 67, 'http://127.0.0.1/corbeilassoc/67-revision-v1/', 0, 'revision', '', 0),
(150, 1, '2015-05-02 22:44:47', '2015-05-02 21:44:47', 'Bonjour', 'Profil', '', 'inherit', 'open', 'open', '', '65-revision-v1', '', '', '2015-05-02 22:44:47', '2015-05-02 21:44:47', '', 65, 'http://127.0.0.1/corbeilassoc/65-revision-v1/', 0, 'revision', '', 0),
(151, 5, '2015-05-03 19:39:16', '2015-05-03 17:39:16', 'Bijour bojour', 'Salut', '', 'trash', 'closed', 'closed', '', 'salut', '', '', '2015-05-30 08:45:30', '2015-05-30 06:45:30', '', 0, 'http://127.0.0.1/corbeilassoc/?post_type=association&#038;p=151', 0, 'association', '', 0),
(155, 7, '2015-05-03 20:50:09', '2015-05-03 18:50:09', 'Mon pote', 'Coucou', '', 'trash', 'closed', 'open', '', 'coucou', '', '', '2015-05-29 19:53:14', '2015-05-29 17:53:14', '', 0, 'http://127.0.0.1/corbeilassoc/?p=155', 0, 'post', '', 0),
(156, 7, '2015-05-03 20:50:09', '2015-05-03 18:50:09', 'Mon pote', 'Coucou', '', 'inherit', 'closed', 'open', '', '155-revision-v1', '', '', '2015-05-03 20:50:09', '2015-05-03 18:50:09', '', 155, 'http://127.0.0.1/corbeilassoc/155-revision-v1/', 0, 'revision', '', 0),
(157, 7, '2015-05-03 21:27:04', '2015-05-03 19:27:04', 'Sa', 'Wesh Morray', '', 'trash', 'closed', 'closed', '', 'wesh-morray', '', '', '2015-05-30 08:45:18', '2015-05-30 06:45:18', '', 0, 'http://127.0.0.1/corbeilassoc/?post_type=association&#038;p=157', 0, 'association', '', 0),
(158, 7, '2015-05-03 21:30:39', '2015-05-03 19:30:39', 'Mon frère', 'Lourd', '', 'trash', 'closed', 'closed', '', 'lourd', '', '', '2015-05-30 08:45:07', '2015-05-30 06:45:07', '', 0, 'http://127.0.0.1/corbeilassoc/?post_type=association&#038;p=158', 0, 'association', '', 0),
(159, 1, '2015-05-03 22:23:38', '2015-05-03 20:23:38', '', 'Ajouter un article', '', 'inherit', 'closed', 'open', '', '133-revision-v1', '', '', '2015-05-03 22:23:38', '2015-05-03 20:23:38', '', 133, 'http://127.0.0.1/corbeilassoc/133-revision-v1/', 0, 'revision', '', 0),
(160, 1, '2015-05-03 22:35:16', '0000-00-00 00:00:00', '', 'Modifier mon association', '', 'draft', 'closed', 'open', '', '', '', '', '2015-05-03 22:35:16', '2015-05-03 20:35:16', '', 0, 'http://127.0.0.1/corbeilassoc/?page_id=160', 0, 'page', '', 0),
(161, 1, '2015-05-03 22:34:12', '2015-05-03 20:34:12', 'Vous pouvez ajouter votre association au répertoire du site en remplissant le formulaire suivant.', 'Ajouter mon association', '', 'inherit', 'closed', 'open', '', '103-revision-v1', '', '', '2015-05-03 22:34:12', '2015-05-03 20:34:12', '', 103, 'http://127.0.0.1/corbeilassoc/103-revision-v1/', 0, 'revision', '', 0),
(162, 1, '2015-05-03 22:36:12', '2015-05-03 20:36:12', 'Vous pouvez modifier les informations de votre association avec le formulaire ci-dessous.', 'Modifier mon association', '', 'publish', 'closed', 'open', '', 'modifier-mon-association', '', '', '2015-05-03 22:36:12', '2015-05-03 20:36:12', '', 0, 'http://127.0.0.1/corbeilassoc/?page_id=162', 0, 'page', '', 0),
(163, 1, '2015-05-03 22:36:12', '2015-05-03 20:36:12', 'Vous pouvez modifier les informations de votre association avec le formulaire ci-dessous.', 'Modifier mon association', '', 'inherit', 'closed', 'open', '', '162-revision-v1', '', '', '2015-05-03 22:36:12', '2015-05-03 20:36:12', '', 162, 'http://127.0.0.1/corbeilassoc/162-revision-v1/', 0, 'revision', '', 0),
(164, 1, '2015-05-03 23:03:09', '2015-05-03 21:03:09', 'Salut cousin', 'FloFloAssoc', '', 'trash', 'closed', 'open', '', 'flofloassoc', '', '', '2015-05-30 08:45:39', '2015-05-30 06:45:39', '', 0, 'http://127.0.0.1/corbeilassoc/?post_type=association&#038;p=164', 0, 'association', '', 0),
(167, 1, '2015-05-05 08:18:15', '2015-05-05 06:18:15', '', 'Profil', '', 'inherit', 'closed', 'open', '', '65-revision-v1', '', '', '2015-05-05 08:18:15', '2015-05-05 06:18:15', '', 65, 'http://127.0.0.1/corbeilassoc/65-revision-v1/', 0, 'revision', '', 0),
(168, 1, '2015-05-05 10:01:06', '0000-00-00 00:00:00', '', 'Modifier mon article', '', 'draft', 'closed', 'open', '', '', '', '', '2015-05-05 10:01:06', '2015-05-05 08:01:06', '', 0, 'http://127.0.0.1/corbeilassoc/?page_id=168', 0, 'page', '', 0),
(169, 1, '2015-05-05 10:03:13', '2015-05-05 08:03:13', '', 'Modifier mon article', '', 'publish', 'closed', 'open', '', 'modifier-mon-article', '', '', '2015-05-05 10:03:13', '2015-05-05 08:03:13', '', 0, 'http://127.0.0.1/corbeilassoc/?page_id=169', 0, 'page', '', 0),
(170, 1, '2015-05-05 10:03:13', '2015-05-05 08:03:13', '', 'Modifier mon article', '', 'inherit', 'closed', 'open', '', '169-revision-v1', '', '', '2015-05-05 10:03:13', '2015-05-05 08:03:13', '', 169, 'http://127.0.0.1/corbeilassoc/169-revision-v1/', 0, 'revision', '', 0),
(171, 1, '2015-05-05 10:09:05', '2015-05-05 08:09:05', 'Bienvenue dans WordPress. Ceci est votre premier article. Modifiez-le ou supprimez-le, puis lancez-vous ! N''hésitez-pas à laisser un commentaire pour rester en contact.', 'Bonjour tout le monde !', '', 'inherit', 'closed', 'open', '', '1-revision-v1', '', '', '2015-05-05 10:09:05', '2015-05-05 08:09:05', '', 1, 'http://127.0.0.1/corbeilassoc/1-revision-v1/', 0, 'revision', '', 0),
(172, 1, '2015-05-05 10:39:26', '2015-05-05 08:39:26', '', 'Modifier mon événement', '', 'publish', 'closed', 'open', '', 'modifier-mon-evenement', '', '', '2015-05-05 10:39:26', '2015-05-05 08:39:26', '', 0, 'http://127.0.0.1/corbeilassoc/?page_id=172', 0, 'page', '', 0),
(173, 1, '2015-05-05 10:39:26', '2015-05-05 08:39:26', '', 'Modifier mon événement', '', 'inherit', 'closed', 'open', '', '172-revision-v1', '', '', '2015-05-05 10:39:26', '2015-05-05 08:39:26', '', 172, 'http://127.0.0.1/corbeilassoc/172-revision-v1/', 0, 'revision', '', 0),
(174, 1, '2015-05-05 22:41:15', '2015-05-05 20:41:15', 'Bonjour,\r\n\r\nSuite à notre événement de la semaine dernière, vous pouvez retrouver des photos et une courte vidéo. Un grand merci à tous d''être venu.', 'La fête du slip !!', '', 'trash', 'closed', 'open', '', 'la-fete-du-slip', '', '', '2015-05-29 19:53:23', '2015-05-29 17:53:23', '', 0, 'http://127.0.0.1/corbeilassoc/?p=174', 0, 'post', '', 0),
(176, 1, '2015-05-29 19:38:21', '2015-05-29 17:38:21', '<strong>Benoît Reeves à Corbeil-Essonnes, le fils de l’astrophysicien Hubert Reeves sera à Corbeil-Essonnes le 6 juin prochain à 20h pour nous présenter son film : « L’Univers au fil de l’eau »</strong>\r\n\r\nSpécialisé dans les domaines de l’astronomie et de l’environnement, Benoît Reeves (<a class="spip_out" href="http://benoit-reeves.fr/" rel="external">son site personnel</a>), fils du célèbre astrophysicien franco-canadien Hubert Reeves, viendra le 6 juin à Corbeil-Essonnes pour débattre avec les habitants sur le thème de l’eau, son origine cosmique, ses rapports étroits avec la vie, mais aussi de notre avenir et celui de tous les êtres vivants au regard de nos ressources en eau. Ce débat, qui s’annonce passionnant, se déroulera suite à la projection au <a class="spip_out" href="http://cine-arcel.com/" rel="external">cinéma ARCEL</a>, en avant-première le 6 juin à 20h, du film « <a class="spip_out" href="http://benoit-reeves.fr/realisations-audiovisuelles/lunivers-au-fil-de-leau/" rel="external">L’Univers au fil de l’eau</a> » réalisé par Benoît Reeves, qui est une célébration de l’apparition de l’eau liquide dans l’univers avec une conclusion riche de sens : à l’échelle cosmique, l’eau liquide est plus rare que l’or. Ce film sera ensuite inscrit au programme des séances de la semaine suivante à 2,50 €.\r\n\r\n&nbsp;', 'L’Univers au fil de l’eau', '', 'publish', 'closed', 'closed', '', 'lunivers-au-fil-de-leau', '', '', '2015-05-29 19:38:21', '2015-05-29 17:38:21', '', 0, 'http://127.0.0.1/corbeilassoc/?post_type=event&#038;p=176', 0, 'event', '', 0),
(177, 1, '2015-05-29 19:46:21', '2015-05-29 17:46:21', '<p style="text-align: left;">Opération « Essonne verte, Essonne propre » Découverte de la biodiversité, activités ludiques et artistiques, samedi 30 mai de 10h à 17h, cirque de l’Essonne.</p>', 'Essonne verte, Essonne propre', '', 'publish', 'closed', 'closed', '', 'essonne-verte-essonne-propre', '', '', '2015-05-29 19:46:21', '2015-05-29 17:46:21', '', 0, 'http://127.0.0.1/corbeilassoc/?post_type=event&#038;p=177', 0, 'event', '', 0),
(178, 1, '2015-05-29 19:53:23', '2015-05-29 17:53:23', 'Bonjour,\r\n\r\nSuite à notre événement de la semaine dernière, vous pouvez retrouver des photos et une courte vidéo. Un grand merci à tous d''être venu.', 'La fête du slip !!', '', 'inherit', 'closed', 'open', '', '174-revision-v1', '', '', '2015-05-29 19:53:23', '2015-05-29 17:53:23', '', 174, 'http://127.0.0.1/corbeilassoc/174-revision-v1/', 0, 'revision', '', 0),
(180, 1, '2015-06-10 07:46:51', '0000-00-00 00:00:00', '', 'Brouillon auto', '', 'auto-draft', 'closed', 'open', '', '', '', '', '2015-06-10 07:46:51', '0000-00-00 00:00:00', '', 0, 'http://127.0.0.1/corbeilassoc/?p=180', 0, 'post', '', 0);

-- --------------------------------------------------------

--
-- Structure de la table `crbass_terms`
--

CREATE TABLE IF NOT EXISTS `crbass_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=19 ;

--
-- Contenu de la table `crbass_terms`
--

INSERT INTO `crbass_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Non classé', 'non-classe-assoc', 0),
(2, 'Marché de Corbeil-Essonnes, place du Comte-Haymon', 'marche', 0),
(3, 'Principal', 'principal', 0),
(4, 'Sport', 'sport_event', 0),
(5, 'Culture', 'culture-event', 0),
(6, 'piscine', 'piscine', 0),
(7, 'polo', 'polo', 0),
(8, 'kayak', 'kayak', 0),
(9, 'Stade Nautique', 'stade-nautique', 0),
(10, 'Sport', 'sport_assoc', 0),
(11, 'Culture', 'culture_assoc', 0),
(12, 'Education', 'education_assoc', 0),
(13, 'cinéma', 'cinema', 0),
(14, 'environnement', 'environnement', 0),
(15, 'eau', 'eau', 0),
(16, 'Cinéma Arcel', 'cinema-arcel', 0),
(17, 'Environnement', 'environnement', 0),
(18, 'Cirque de l''Essonne', 'cirque-de-lessonne', 0);

-- --------------------------------------------------------

--
-- Structure de la table `crbass_term_relationships`
--

CREATE TABLE IF NOT EXISTS `crbass_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `crbass_term_relationships`
--

INSERT INTO `crbass_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(4, 2, 0),
(7, 3, 0),
(12, 3, 0),
(43, 12, 0),
(50, 11, 0),
(65, 1, 0),
(67, 1, 0),
(69, 1, 0),
(71, 1, 0),
(73, 1, 0),
(75, 1, 0),
(77, 1, 0),
(91, 3, 0),
(97, 3, 0),
(105, 12, 0),
(106, 3, 0),
(109, 3, 0),
(130, 2, 0),
(130, 4, 0),
(131, 3, 0),
(132, 4, 0),
(132, 9, 0),
(139, 1, 0),
(140, 3, 0),
(155, 11, 0),
(174, 11, 0),
(176, 5, 0),
(176, 13, 0),
(176, 14, 0),
(176, 15, 0),
(176, 16, 0),
(177, 17, 0),
(177, 18, 0);

-- --------------------------------------------------------

--
-- Structure de la table `crbass_term_taxonomy`
--

CREATE TABLE IF NOT EXISTS `crbass_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=19 ;

--
-- Contenu de la table `crbass_term_taxonomy`
--

INSERT INTO `crbass_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'event-venue', '', 0, 1),
(3, 3, 'nav_menu', '', 0, 8),
(4, 4, 'event-category', '', 0, 0),
(5, 5, 'event-category', '', 0, 1),
(6, 6, 'event-tag', '', 0, 0),
(7, 7, 'event-tag', '', 0, 0),
(8, 8, 'event-tag', '', 0, 0),
(9, 9, 'event-venue', '', 0, 0),
(10, 10, 'category', '', 0, 0),
(11, 11, 'category', '', 0, 0),
(12, 12, 'category', '', 0, 2),
(13, 13, 'event-tag', '', 0, 1),
(14, 14, 'event-tag', '', 0, 1),
(15, 15, 'event-tag', '', 0, 1),
(16, 16, 'event-venue', '', 0, 1),
(17, 17, 'event-category', '', 0, 1),
(18, 18, 'event-venue', '', 0, 1);

-- --------------------------------------------------------

--
-- Structure de la table `crbass_usermeta`
--

CREATE TABLE IF NOT EXISTS `crbass_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=148 ;

--
-- Contenu de la table `crbass_usermeta`
--

INSERT INTO `crbass_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'webmasterflo'),
(2, 1, 'first_name', 'Mizi'),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'comment_shortcuts', 'false'),
(7, 1, 'admin_color', 'fresh'),
(8, 1, 'use_ssl', '0'),
(9, 1, 'show_admin_bar_front', 'true'),
(10, 1, 'crbass_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(11, 1, 'crbass_user_level', '10'),
(12, 1, 'dismissed_wp_pointers', 'wp360_locks,wp390_widgets,wp410_dfw'),
(13, 1, 'show_welcome_panel', '1'),
(15, 1, 'crbass_dashboard_quick_press_last_post_id', '180'),
(16, 1, 'closedpostboxes_event', 'a:0:{}'),
(17, 1, 'metaboxhidden_event', 'a:2:{i:0;s:23:"acf-group_549c576c510bc";i:1;s:7:"slugdiv";}'),
(18, 1, 'managenav-menuscolumnshidden', 'a:4:{i:0;s:11:"link-target";i:1;s:11:"css-classes";i:2;s:3:"xfn";i:3;s:11:"description";}'),
(19, 1, 'metaboxhidden_nav-menus', 'a:7:{i:0;s:8:"add-post";i:1;s:9:"add-event";i:2;s:12:"add-post_tag";i:3;s:15:"add-post_format";i:4;s:15:"add-event-venue";i:5;s:18:"add-event-category";i:6;s:13:"add-event-tag";}'),
(20, 1, 'nav_menu_recently_edited', '3'),
(21, 1, 'crbass_user-settings', 'editor=tinymce&libraryContent=upload&hidetb=1'),
(22, 1, 'crbass_user-settings-time', '1430858498'),
(23, 1, 'closedpostboxes_association', 'a:0:{}'),
(24, 1, 'metaboxhidden_association', 'a:1:{i:0;s:7:"slugdiv";}'),
(25, 1, 'closedpostboxes_post', 'a:0:{}'),
(26, 1, 'metaboxhidden_post', 'a:6:{i:0;s:23:"acf-group_549c576c510bc";i:1;s:11:"postexcerpt";i:2;s:13:"trackbacksdiv";i:3;s:16:"commentstatusdiv";i:4;s:7:"slugdiv";i:5;s:9:"authordiv";}'),
(27, 1, 'closedpostboxes_page', 'a:0:{}'),
(28, 1, 'metaboxhidden_page', 'a:5:{i:0;s:23:"acf-group_549c576c510bc";i:1;s:11:"postexcerpt";i:2;s:16:"commentstatusdiv";i:3;s:7:"slugdiv";i:4;s:9:"authordiv";}'),
(29, 1, 'account_status', 'approved'),
(30, 1, 'role', 'admin'),
(31, 1, 'meta-box-order_toplevel_page_ultimatemember', 'a:3:{s:4:"core";s:25:"um-metaboxes-contentbox-1";s:6:"normal";s:0:"";s:4:"side";s:45:"um-metaboxes-sidebox-1,um-metaboxes-sidebox-2";}'),
(55, 1, 'wysija_pref', 'YTowOnt9'),
(91, 5, 'nickname', 'samy'),
(92, 5, 'first_name', ''),
(93, 5, 'last_name', ''),
(94, 5, 'description', ''),
(95, 5, 'rich_editing', 'true'),
(96, 5, 'comment_shortcuts', 'false'),
(97, 5, 'admin_color', 'fresh'),
(98, 5, 'use_ssl', '0'),
(99, 5, 'show_admin_bar_front', 'true'),
(100, 5, 'crbass_capabilities', 'a:1:{s:17:"association_owner";b:1;}'),
(101, 5, 'crbass_user_level', '2'),
(102, 5, 'dismissed_wp_pointers', 'wp360_locks,wp390_widgets,wp410_dfw'),
(109, 6, 'nickname', 'sisi'),
(110, 6, 'first_name', ''),
(111, 6, 'last_name', ''),
(112, 6, 'description', ''),
(113, 6, 'rich_editing', 'true'),
(114, 6, 'comment_shortcuts', 'false'),
(115, 6, 'admin_color', 'fresh'),
(116, 6, 'use_ssl', '0'),
(117, 6, 'show_admin_bar_front', 'true'),
(118, 6, 'crbass_capabilities', 'a:1:{s:17:"association_owner";b:1;}'),
(119, 6, 'crbass_user_level', '2'),
(120, 6, 'dismissed_wp_pointers', 'wp360_locks,wp390_widgets'),
(125, 7, 'nickname', 'izi'),
(126, 7, 'first_name', ''),
(127, 7, 'last_name', ''),
(128, 7, 'description', ''),
(129, 7, 'rich_editing', 'true'),
(130, 7, 'comment_shortcuts', 'false'),
(131, 7, 'admin_color', 'fresh'),
(132, 7, 'use_ssl', '0'),
(133, 7, 'show_admin_bar_front', 'true'),
(134, 7, 'crbass_capabilities', 'a:1:{s:17:"association_owner";b:1;}'),
(135, 7, 'crbass_user_level', '2'),
(136, 7, 'dismissed_wp_pointers', 'wp360_locks,wp390_widgets,wp410_dfw'),
(138, 7, 'crbass_dashboard_quick_press_last_post_id', '154'),
(140, 1, 'manageedit-pagecolumnshidden', 'a:1:{i:0;s:0:"";}'),
(142, 5, 'crbass_dashboard_quick_press_last_post_id', '165'),
(146, 5, 'session_tokens', 'a:1:{s:64:"e06cf922f44e64de415601a4573039b7bdd434d3dffc2b3fd431eba5f4883a86";a:4:{s:10:"expiration";i:1430867252;s:2:"ip";s:9:"127.0.0.1";s:2:"ua";s:108:"Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36";s:5:"login";i:1430694452;}}'),
(147, 1, 'session_tokens', 'a:1:{s:64:"c7556a41f5defd96c8b2fcfec0301562ea059a631b6adaee590eb55eb784df64";a:4:{s:10:"expiration";i:1434087983;s:2:"ip";s:9:"127.0.0.1";s:2:"ua";s:108:"Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36";s:5:"login";i:1433915183;}}');

-- --------------------------------------------------------

--
-- Structure de la table `crbass_users`
--

CREATE TABLE IF NOT EXISTS `crbass_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=8 ;

--
-- Contenu de la table `crbass_users`
--

INSERT INTO `crbass_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'webmasterflo', '$P$BNRsE5irD0bFSOyFmoTw0exsdjgjmq1', 'webmasterflo', 'contact@tiar-florian.fr', '', '2014-12-24 18:35:50', '', 0, 'webmasterflo'),
(5, 'samy', '$P$BlWynFP3VrH5aUSqRxURb5UcocLtHo0', 'samy', 'sam@bkc.fr', '', '2015-05-03 17:24:30', '', 0, 'samy'),
(6, 'sisi', '$P$BcTNI6ue9stlDrG8bTqkitebgzV4NH.', 'sisi', 'sisi@fr.fr', '', '2015-05-03 18:25:47', '', 0, 'sisi'),
(7, 'izi', '$P$BxGT91PLT8ASN7sgn5iAxwrDB5lLZO/', 'izi', 'izi@bizi.fr', '', '2015-05-03 18:40:42', '', 0, 'izi');

-- --------------------------------------------------------

--
-- Structure de la table `crbass_wysija_campaign`
--

CREATE TABLE IF NOT EXISTS `crbass_wysija_campaign` (
  `campaign_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`campaign_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `crbass_wysija_campaign`
--

INSERT INTO `crbass_wysija_campaign` (`campaign_id`, `name`, `description`) VALUES
(1, 'Guide d''utilisation', '');

-- --------------------------------------------------------

--
-- Structure de la table `crbass_wysija_campaign_list`
--

CREATE TABLE IF NOT EXISTS `crbass_wysija_campaign_list` (
  `list_id` int(10) unsigned NOT NULL,
  `campaign_id` int(10) unsigned NOT NULL,
  `filter` text,
  PRIMARY KEY (`list_id`,`campaign_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `crbass_wysija_custom_field`
--

CREATE TABLE IF NOT EXISTS `crbass_wysija_custom_field` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `name` tinytext NOT NULL,
  `type` tinytext NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `settings` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `crbass_wysija_email`
--

CREATE TABLE IF NOT EXISTS `crbass_wysija_email` (
  `email_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `campaign_id` int(10) unsigned NOT NULL DEFAULT '0',
  `subject` varchar(250) NOT NULL DEFAULT '',
  `body` longtext,
  `created_at` int(10) unsigned DEFAULT NULL,
  `modified_at` int(10) unsigned DEFAULT NULL,
  `sent_at` int(10) unsigned DEFAULT NULL,
  `from_email` varchar(250) DEFAULT NULL,
  `from_name` varchar(250) DEFAULT NULL,
  `replyto_email` varchar(250) DEFAULT NULL,
  `replyto_name` varchar(250) DEFAULT NULL,
  `attachments` text,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `type` tinyint(4) NOT NULL DEFAULT '1',
  `number_sent` int(10) unsigned NOT NULL DEFAULT '0',
  `number_opened` int(10) unsigned NOT NULL DEFAULT '0',
  `number_clicked` int(10) unsigned NOT NULL DEFAULT '0',
  `number_unsub` int(10) unsigned NOT NULL DEFAULT '0',
  `number_bounce` int(10) unsigned NOT NULL DEFAULT '0',
  `number_forward` int(10) unsigned NOT NULL DEFAULT '0',
  `params` text,
  `wj_data` longtext,
  `wj_styles` longtext,
  PRIMARY KEY (`email_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `crbass_wysija_email`
--

INSERT INTO `crbass_wysija_email` (`email_id`, `campaign_id`, `subject`, `body`, `created_at`, `modified_at`, `sent_at`, `from_email`, `from_name`, `replyto_email`, `replyto_name`, `attachments`, `status`, `type`, `number_sent`, `number_opened`, `number_clicked`, `number_unsub`, `number_bounce`, `number_forward`, `params`, `wj_data`, `wj_styles`) VALUES
(1, 1, 'Guide d''utilisation', '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">\n<html xmlns="http://www.w3.org/1999/xhtml"  >\n<head>\n    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />\n    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>\n    <title>Guide d''utilisation</title>\n    <style type="text/css">body {\n        width:100% !important;\n        -webkit-text-size-adjust:100%;\n        -ms-text-size-adjust:100%;\n        margin:0;\n        padding:0;\n    }\n\n    body,table,td,p,a,li,blockquote{\n        -ms-text-size-adjust:100%;\n        -webkit-text-size-adjust:100%;\n    }\n\n    .ReadMsgBody{\n        width:100%;\n    }.ExternalClass {width:100%;}.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}#backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important; background:#e8e8e8;}img {\n        outline:none;\n        text-decoration:none;\n        -ms-interpolation-mode: bicubic;\n    }\n    a img {border:none;}\n    .image_fix {display:block;}p {\n        font-family: "Arial";\n        font-size: 16px;\n        line-height: 150%;\n        margin: 1em 0;\n        padding: 0;\n    }h1,h2,h3,h4,h5,h6{\n        margin:0;\n        padding:0;\n    }h1 {\n        color:#000000 !important;\n        display:block;\n        font-family:Trebuchet MS;\n        font-size:40px;\n        font-style:normal;\n        font-weight:normal;\n        line-height:125%;\n        letter-spacing:normal;\n        margin:0;\n        \n        text-align:left;\n    }h2 {\n        color:#424242 !important;\n        display:block;\n        font-family:Trebuchet MS;\n        font-size:30px;\n        font-style:normal;\n        font-weight:normal;\n        line-height:125%;\n        letter-spacing:normal;\n        margin:0;\n        \n        text-align:left;\n    }h3 {\n        color:#424242 !important;\n        display:block;\n        font-family:Trebuchet MS;\n        font-size:24px;\n        font-style:normal;\n        font-weight:normal;\n        line-height:125%;\n        letter-spacing:normal;\n        margin:0;\n        \n        text-align:left;\n    }table td {border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;}table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }a {\n        color:#4a91b0;\n        word-wrap:break-word;\n    }\n    #outlook a {padding:0;}\n    .yshortcuts { color:#4a91b0; }\n\n    #wysija_wrapper {\n        background:#e8e8e8;\n        color:#000000;\n        font-family:"Arial";\n        font-size:16px;\n        -webkit-text-size-adjust:100%;\n        -ms-text-size-adjust:100%;\n        \n    }\n\n    .wysija_header_container {\n        mso-border-right-alt: 0;\n        mso-border-left-alt: 0;\n        mso-border-top-alt: 0;\n        mso-border-bottom-alt: 0;\n        \n    }\n\n    .wysija_block {\n        mso-border-right-alt: 0;\n        mso-border-left-alt: 0;\n        mso-border-top-alt: 0;\n        mso-border-bottom-alt: 0;\n        background:#ffffff;\n    }\n\n    .wysija_footer_container {\n        mso-border-right-alt: 0;\n        mso-border-left-alt: 0;\n        mso-border-top-alt: 0;\n        mso-border-bottom-alt: 0;\n        \n    }\n\n    .wysija_viewbrowser_container, .wysija_viewbrowser_container a {\n        font-family: "Arial" !important;\n        font-size: 12px !important;\n        color: #000000 !important;\n    }\n    .wysija_unsubscribe_container, .wysija_unsubscribe_container a {\n        text-align:center;\n        color: #000000 !important;\n        font-size:12px;\n    }\n    .wysija_viewbrowser_container a, .wysija_unsubscribe_container a {\n        text-decoration:underline;\n    }\n    .wysija_list_item {\n        margin:0;\n    }@media only screen and (max-device-width: 480px), screen and (max-width: 480px) {a[href^="tel"], a[href^="sms"] {\n            text-decoration: none;\n            color: #4a91b0;pointer-events: none;\n            cursor: default;\n        }\n\n        .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {\n            text-decoration: default;\n            color: #4a91b0 !important;\n            pointer-events: auto;\n            cursor: default;\n        }body, table, td, p, a, li, blockquote { -webkit-text-size-adjust:none !important; }body{ width:100% !important; min-width:100% !important; }\n    }@media only screen and (min-device-width: 768px) and (max-device-width: 1024px), screen and (min-width: 768px) and (max-width: 1024px) {a[href^="tel"],\n        a[href^="sms"] {\n            text-decoration: none;\n            color: #4a91b0;pointer-events: none;\n            cursor: default;\n        }\n\n        .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {\n            text-decoration: default;\n            color: #4a91b0 !important;\n            pointer-events: auto;\n            cursor: default;\n        }\n    }\n\n    @media only screen and (-webkit-min-device-pixel-ratio: 2) {\n    }@media only screen and (-webkit-device-pixel-ratio:.75){}\n    @media only screen and (-webkit-device-pixel-ratio:1){}\n    @media only screen and (-webkit-device-pixel-ratio:1.5){}</style><!--[if IEMobile 7]>\n<style type="text/css">\n\n</style>\n<![endif]--><!--[if gte mso 9]>\n<style type="text/css">.wysija_image_container {\n        padding-top:0 !important;\n    }\n    .wysija_image_placeholder {\n        mso-text-raise:0;\n        mso-table-lspace:0;\n        mso-table-rspace:0;\n        margin-bottom: 0 !important;\n    }\n    .wysija_block .wysija_image_placeholder {\n        margin:2px 1px 0 1px !important;\n    }\n    p {\n        line-height: 110% !important;\n    }\n    h1, h2, h3 {\n        line-height: 110% !important;\n        margin:0 !important;\n        padding: 0 !important;\n    }\n</style>\n<![endif]-->\n\n<!--[if gte mso 15]>\n<style type="text/css">table { font-size:1px; mso-line-height-alt:0; line-height:0; mso-margin-top-alt:0; }\n    tr { font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px; }\n</style>\n<![endif]-->\n\n</head>\n<body bgcolor="#e8e8e8" yahoo="fix">\n    <span style="margin-bottom:0;margin-left:0;margin-right:0;margin-top:0;padding-bottom:0;padding-left:0;padding-right:0;padding-top:0;display:block;background:#e8e8e8;">\n    <table width="100%" cellpadding="0" cellspacing="0" border="0" id="wysija_wrapper">\n        <tr>\n            <td valign="top" align="center">\n                <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">\n                    \n                    <tr>\n                        <td width="600" style="min-width:600px;" valign="top" align="center"   >\n                            <p class="wysija_viewbrowser_container" style="font-family: Arial, ''Helvetica Neue'', Helvetica, sans-serif;font-size: 12px;color: #000000;color: #000000 !important;background-color: #e8e8e8;border: 0;text-align: center;padding-top: 8px;padding-right: 8px;padding-bottom: 8px;padding-left: 8px;" >Problèmes d&#039;affichage ? <a style="font-family: Arial, ''Helvetica Neue'', Helvetica, sans-serif;font-size: 12px;color: #000000;color: #000000 !important;background-color: #e8e8e8;border: 0;" href="[view_in_browser_link]" target="_blank">Affichez cette newsletter dans votre navigateur.</a></p>\n                        </td>\n                    </tr>\n                    \n                    <tr>\n                        <td width="600" style="min-width:600px;" valign="top" align="center">\n                            \n<table class="wysija_header" style="border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">\n <tr>\n <td height="1" align="center" class="wysija_header_container" style="font-size:1px;line-height:1%;mso-line-height-rule:exactly;border: 0;min-width: 100%;background-color: #e8e8e8;border: 0;" >\n \n <img width="600" height="72" src="http://127.0.0.1/corbeilassoc/wp-content/plugins/wysija-newsletters/img/default-newsletter/newsletter/header.png" border="0" alt="" class="image_fix" style="width:600px; height:72px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;" />\n </td>\n </tr>\n</table>\n                        </td>\n                    </tr>\n                    <tr>\n                        <td width="600" style="min-width:600px;" valign="top" align="left">\n                            \n\n<table class="wysija_block" style="border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">\n <tr>\n \n \n <td class="wysija_content_container left" style="border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;" align="left" >\n \n <div class="wysija_text_container"><h2 style="font-family: ''Trebuchet MS'', ''Lucida Grande'', ''Lucida Sans Unicode'', ''Lucida Sans'', Tahoma, sans-serif;font-size: 30px;color: #424242;color: #424242 !important;background-color: #ffffff;border: 0;font-weight: normal;font-style: normal;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 125%;margin-top: 0;margin-right: 0;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;"><strong>Étape 1 :</strong> hé, vous, cliquez sur ce texte !</h2><p style="font-family: Arial, ''Helvetica Neue'', Helvetica, sans-serif;font-size: 16px;color: #000000;color: #000000 !important;background-color: #ffffff;border: 0;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 150%;margin-top: 1em;margin-right: 0;margin-bottom: 1em;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;vertical-align: top;word-wrap: break-word;">Cliquer sur ce bloc de texte pour le modifier.</p></div>\n </td>\n \n </tr>\n</table>\n\n<table class="wysija_block" style="border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">\n <tr style="font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px;">\n <td width="100%" valign="middle" class="wysija_divider_container" style="height:1px;background-color: #ffffff;border: 0;padding-top: 15px;padding-right: 17px;padding-bottom: 15px;padding-left: 17px;" align="left">\n <div align="center">\n <img src="http://127.0.0.1/corbeilassoc/wp-content/uploads/wysija/dividers/solid.jpg" border="0" width="564" height="1" alt="---" class="image_fix" style="width:564px; height:1px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;" />\n </div>\n </td>\n </tr>\n</table>\n\n\n<table class="wysija_block" style="border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">\n <tr>\n \n \n <td class="wysija_content_container left" style="border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;" align="left" >\n \n <div class="wysija_text_container"><h2 style="font-family: ''Trebuchet MS'', ''Lucida Grande'', ''Lucida Sans Unicode'', ''Lucida Sans'', Tahoma, sans-serif;font-size: 30px;color: #424242;color: #424242 !important;background-color: #ffffff;border: 0;font-weight: normal;font-style: normal;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 125%;margin-top: 0;margin-right: 0;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;"><strong>Étape 2 :</strong> manipuler l''image</h2></div>\n </td>\n \n </tr>\n</table>\n\n<table class="wysija_block" style="border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">\n <tr>\n \n \n <td class="wysija_content_container left" style="border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;" align="left" >\n \n \n \n <table style="border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;" width="1%" height="190" cellspacing="0" cellpadding="0" border="0" align="left">\n <tr style="font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px;">\n <td class="wysija_image_container left" style="border: 0;border-collapse: collapse;border: 1px solid #ffffff;display: block;margin-top: 0;margin-right: 0;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 10px;padding-bottom: 0;padding-left: 0;" width="1%" height="190" valign="top">\n <div align="left" class="wysija_image_placeholder left" style="height:190px;width:281px;border: 0;display: block;margin-top: 0;margin-right: 10px;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;" >\n \n <img width="281" height="190" src="http://127.0.0.1/corbeilassoc/wp-content/plugins/wysija-newsletters/img/default-newsletter/newsletter/pigeon.png" border="0" alt="" class="image_fix" style="width:281px; height:190px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;" />\n </div>\n </td>\n </tr>\n </table>\n\n <div class="wysija_text_container"><p style="font-family: Arial, ''Helvetica Neue'', Helvetica, sans-serif;font-size: 16px;color: #000000;color: #000000 !important;background-color: #ffffff;border: 0;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 150%;margin-top: 1em;margin-right: 0;margin-bottom: 1em;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;vertical-align: top;word-wrap: break-word;">Survoler avec votre souris l''image à gauche.</p></div>\n </td>\n \n </tr>\n</table>\n\n<table class="wysija_block" style="border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">\n <tr style="font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px;">\n <td width="100%" valign="middle" class="wysija_divider_container" style="height:1px;background-color: #ffffff;border: 0;padding-top: 15px;padding-right: 17px;padding-bottom: 15px;padding-left: 17px;" align="left">\n <div align="center">\n <img src="http://127.0.0.1/corbeilassoc/wp-content/uploads/wysija/dividers/solid.jpg" border="0" width="564" height="1" alt="---" class="image_fix" style="width:564px; height:1px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;" />\n </div>\n </td>\n </tr>\n</table>\n\n\n<table class="wysija_block" style="border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">\n <tr>\n \n \n <td class="wysija_content_container left" style="border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;" align="left" >\n \n <div class="wysija_text_container"><h2 style="font-family: ''Trebuchet MS'', ''Lucida Grande'', ''Lucida Sans Unicode'', ''Lucida Sans'', Tahoma, sans-serif;font-size: 30px;color: #424242;color: #424242 !important;background-color: #ffffff;border: 0;font-weight: normal;font-style: normal;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 125%;margin-top: 0;margin-right: 0;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;"><strong>Étape 3 :</strong> déposer du contenu ici</h2><p style="font-family: Arial, ''Helvetica Neue'', Helvetica, sans-serif;font-size: 16px;color: #000000;color: #000000 !important;background-color: #ffffff;border: 0;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 150%;margin-top: 1em;margin-right: 0;margin-bottom: 1em;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;vertical-align: top;word-wrap: break-word;">Glisser et déposer <strong>textes, articles, séparateurs.</strong> Regardez à droite !</p><p style="font-family: Arial, ''Helvetica Neue'', Helvetica, sans-serif;font-size: 16px;color: #000000;color: #000000 !important;background-color: #ffffff;border: 0;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 150%;margin-top: 1em;margin-right: 0;margin-bottom: 1em;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;vertical-align: top;word-wrap: break-word;">Vous pouvez même déposer <strong>vos icônes de réseaux sociaux</strong> comme ceci :</p></div>\n </td>\n \n </tr>\n</table>\n<table class="wysija_block" style="border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">\n <tr>\n <td class="wysija_gallery_container" style="border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;" >\n <table class="wysija_gallery_table center" style="border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;text-align: center;margin-top: 0;margin-right: auto;margin-bottom: 0;margin-left: auto;" width="184" cellspacing="0" cellpadding="0" border="0" align="center">\n <tr>\n \n \n <td class="wysija_cell_container" style="border: 0;border-collapse: collapse;background-color: #ffffff;border: 1px solid #ffffff;" width="61" height="32" valign="top">\n <div align="center">\n <a style="color: #4a91b0;color: #4a91b0 !important;background-color: #ffffff;border: 0;word-wrap: break-word;" href="http://www.facebook.com/mailpoetplugin"><img src="http://127.0.0.1/corbeilassoc/wp-content/uploads/wysija/bookmarks/medium/02/facebook.png" border="0" alt="Facebook" style="width:32px; height:32px;" /></a>\n </div>\n </td>\n \n \n \n <td class="wysija_cell_container" style="border: 0;border-collapse: collapse;background-color: #ffffff;border: 1px solid #ffffff;" width="61" height="32" valign="top">\n <div align="center">\n <a style="color: #4a91b0;color: #4a91b0 !important;background-color: #ffffff;border: 0;word-wrap: break-word;" href="http://www.twitter.com/mail_poet"><img src="http://127.0.0.1/corbeilassoc/wp-content/uploads/wysija/bookmarks/medium/02/twitter.png" border="0" alt="Twitter" style="width:32px; height:32px;" /></a>\n </div>\n </td>\n \n \n \n <td class="wysija_cell_container" style="border: 0;border-collapse: collapse;background-color: #ffffff;border: 1px solid #ffffff;" width="61" height="32" valign="top">\n <div align="center">\n <a style="color: #4a91b0;color: #4a91b0 !important;background-color: #ffffff;border: 0;word-wrap: break-word;" href="https://plus.google.com/+Mailpoet"><img src="http://127.0.0.1/corbeilassoc/wp-content/uploads/wysija/bookmarks/medium/02/google.png" border="0" alt="Google" style="width:32px; height:32px;" /></a>\n </div>\n </td>\n \n \n </tr>\n </table>\n </td>\n </tr>\n</table>\n\n<table class="wysija_block" style="border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">\n <tr style="font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px;">\n <td width="100%" valign="middle" class="wysija_divider_container" style="height:1px;background-color: #ffffff;border: 0;padding-top: 15px;padding-right: 17px;padding-bottom: 15px;padding-left: 17px;" align="left">\n <div align="center">\n <img src="http://127.0.0.1/corbeilassoc/wp-content/uploads/wysija/dividers/solid.jpg" border="0" width="564" height="1" alt="---" class="image_fix" style="width:564px; height:1px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;" />\n </div>\n </td>\n </tr>\n</table>\n\n\n<table class="wysija_block" style="border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">\n <tr>\n \n \n <td class="wysija_content_container left" style="border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;" align="left" >\n \n <div class="wysija_text_container"><h2 style="font-family: ''Trebuchet MS'', ''Lucida Grande'', ''Lucida Sans Unicode'', ''Lucida Sans'', Tahoma, sans-serif;font-size: 30px;color: #424242;color: #424242 !important;background-color: #ffffff;border: 0;font-weight: normal;font-style: normal;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 125%;margin-top: 0;margin-right: 0;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;"><strong>Étape 4 :</strong> et le pied de page ?</h2><p style="font-family: Arial, ''Helvetica Neue'', Helvetica, sans-serif;font-size: 16px;color: #000000;color: #000000 !important;background-color: #ffffff;border: 0;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 150%;margin-top: 1em;margin-right: 0;margin-bottom: 1em;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;vertical-align: top;word-wrap: break-word;">Changer le contenu du pied de page dans les <strong>Paramètres</strong> de MailPoet.</p></div>\n </td>\n \n </tr>\n</table>\n                        </td>\n                    </tr>\n                    <tr>\n                        <td width="600" style="min-width:600px;" valign="top" align="center"   >\n                            \n<table class="wysija_footer" style="border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">\n <tr>\n <td height="1" align="center" class="wysija_footer_container" style="font-size:1px;line-height:1%;mso-line-height-rule:exactly;border: 0;min-width: 100%;background-color: #e8e8e8;border: 0;" >\n \n <img width="600" height="46" src="http://127.0.0.1/corbeilassoc/wp-content/plugins/wysija-newsletters/img/default-newsletter/newsletter/footer.png" border="0" alt="" class="image_fix" style="width:600px; height:46px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;" />\n </td>\n </tr>\n</table>\n                        </td>\n                    </tr>\n                    \n                    <tr>\n                        <td width="600" style="min-width:600px;" valign="top" align="center"  >\n                            <p class="wysija_unsubscribe_container" style="font-family: Verdana, Geneva, sans-serif;font-size: 12px;color: #000000;color: #000000 !important;background-color: #e8e8e8;border: 0;text-align: center;padding-top: 8px;padding-right: 8px;padding-bottom: 8px;padding-left: 8px;" ><a style="color: #000000;color: #000000 !important;background-color: #e8e8e8;border: 0;" href="[unsubscribe_link]" target="_blank">Se désabonner</a><br /><br /></p>\n                        </td>\n                    </tr>\n                    \n                </table>\n            </td>\n        </tr>\n    </table>\n    </span>\n</body>\n</html>', 1426946054, 1426946054, NULL, 'info@127.0.0.1', 'webmasterflo', 'info@127.0.0.1', 'webmasterflo', NULL, 0, 1, 0, 0, 0, 0, 0, 0, 'YToxOntzOjE0OiJxdWlja3NlbGVjdGlvbiI7YToxOntzOjY6IndwLTMwMSI7YTo1OntzOjEwOiJpZGVudGlmaWVyIjtzOjY6IndwLTMwMSI7czo1OiJ3aWR0aCI7aToyODE7czo2OiJoZWlnaHQiO2k6MTkwO3M6MzoidXJsIjtzOjExMjoiaHR0cDovLzEyNy4wLjAuMS9jb3JiZWlsYXNzb2Mvd3AtY29udGVudC9wbHVnaW5zL3d5c2lqYS1uZXdzbGV0dGVycy9pbWcvZGVmYXVsdC1uZXdzbGV0dGVyL25ld3NsZXR0ZXIvcGlnZW9uLnBuZyI7czo5OiJ0aHVtYl91cmwiO3M6MTIwOiJodHRwOi8vMTI3LjAuMC4xL2NvcmJlaWxhc3NvYy93cC1jb250ZW50L3BsdWdpbnMvd3lzaWphLW5ld3NsZXR0ZXJzL2ltZy9kZWZhdWx0LW5ld3NsZXR0ZXIvbmV3c2xldHRlci9waWdlb24tMTUweDE1MC5wbmciO319fQ==', 'YTo0OntzOjc6InZlcnNpb24iO3M6NjoiMi42LjE1IjtzOjY6ImhlYWRlciI7YTo1OntzOjQ6InRleHQiO047czo1OiJpbWFnZSI7YTo1OntzOjM6InNyYyI7czoxMTI6Imh0dHA6Ly8xMjcuMC4wLjEvY29yYmVpbGFzc29jL3dwLWNvbnRlbnQvcGx1Z2lucy93eXNpamEtbmV3c2xldHRlcnMvaW1nL2RlZmF1bHQtbmV3c2xldHRlci9uZXdzbGV0dGVyL2hlYWRlci5wbmciO3M6NToid2lkdGgiO2k6NjAwO3M6NjoiaGVpZ2h0IjtpOjcyO3M6OToiYWxpZ25tZW50IjtzOjY6ImNlbnRlciI7czo2OiJzdGF0aWMiO2I6MDt9czo5OiJhbGlnbm1lbnQiO3M6NjoiY2VudGVyIjtzOjY6InN0YXRpYyI7YjowO3M6NDoidHlwZSI7czo2OiJoZWFkZXIiO31zOjQ6ImJvZHkiO2E6OTp7czo3OiJibG9jay0xIjthOjY6e3M6NDoidGV4dCI7YToxOntzOjU6InZhbHVlIjtzOjE2NDoiUEdneVBqeHpkSEp2Ym1jK3c0bDBZWEJsSURFZ09qd3ZjM1J5YjI1blBpQm93NmtzSUhadmRYTXNJR05zYVhGMVpYb2djM1Z5SUdObElIUmxlSFJsSUNFOEwyZ3lQanh3UGtOc2FYRjFaWElnYzNWeUlHTmxJR0pzYjJNZ1pHVWdkR1Y0ZEdVZ2NHOTFjaUJzWlNCdGIyUnBabWxsY2k0OEwzQSsiO31zOjU6ImltYWdlIjtOO3M6OToiYWxpZ25tZW50IjtzOjQ6ImxlZnQiO3M6Njoic3RhdGljIjtiOjA7czo4OiJwb3NpdGlvbiI7aToxO3M6NDoidHlwZSI7czo3OiJjb250ZW50Ijt9czo3OiJibG9jay0yIjthOjU6e3M6ODoicG9zaXRpb24iO2k6MjtzOjQ6InR5cGUiO3M6NzoiZGl2aWRlciI7czozOiJzcmMiO3M6NzQ6Imh0dHA6Ly8xMjcuMC4wLjEvY29yYmVpbGFzc29jL3dwLWNvbnRlbnQvdXBsb2Fkcy93eXNpamEvZGl2aWRlcnMvc29saWQuanBnIjtzOjU6IndpZHRoIjtpOjU2NDtzOjY6ImhlaWdodCI7aToxO31zOjc6ImJsb2NrLTMiO2E6Njp7czo0OiJ0ZXh0IjthOjE6e3M6NToidmFsdWUiO3M6NzI6IlBHZ3lQanh6ZEhKdmJtYyt3NGwwWVhCbElESWdPand2YzNSeWIyNW5QaUJ0WVc1cGNIVnNaWElnYkNkcGJXRm5aVHd2YURJKyI7fXM6NToiaW1hZ2UiO047czo5OiJhbGlnbm1lbnQiO3M6NDoibGVmdCI7czo2OiJzdGF0aWMiO2I6MDtzOjg6InBvc2l0aW9uIjtpOjM7czo0OiJ0eXBlIjtzOjc6ImNvbnRlbnQiO31zOjc6ImJsb2NrLTQiO2E6Njp7czo0OiJ0ZXh0IjthOjE6e3M6NToidmFsdWUiO3M6NzI6IlBIQStVM1Z5ZG05c1pYSWdZWFpsWXlCMmIzUnlaU0J6YjNWeWFYTWdiQ2RwYldGblpTRERvQ0JuWVhWamFHVXVQQzl3UGc9PSI7fXM6NToiaW1hZ2UiO2E6NTp7czozOiJzcmMiO3M6MTEyOiJodHRwOi8vMTI3LjAuMC4xL2NvcmJlaWxhc3NvYy93cC1jb250ZW50L3BsdWdpbnMvd3lzaWphLW5ld3NsZXR0ZXJzL2ltZy9kZWZhdWx0LW5ld3NsZXR0ZXIvbmV3c2xldHRlci9waWdlb24ucG5nIjtzOjU6IndpZHRoIjtpOjI4MTtzOjY6ImhlaWdodCI7aToxOTA7czo5OiJhbGlnbm1lbnQiO3M6NDoibGVmdCI7czo2OiJzdGF0aWMiO2I6MDt9czo5OiJhbGlnbm1lbnQiO3M6NDoibGVmdCI7czo2OiJzdGF0aWMiO2I6MDtzOjg6InBvc2l0aW9uIjtpOjQ7czo0OiJ0eXBlIjtzOjc6ImNvbnRlbnQiO31zOjc6ImJsb2NrLTUiO2E6NTp7czo4OiJwb3NpdGlvbiI7aTo1O3M6NDoidHlwZSI7czo3OiJkaXZpZGVyIjtzOjM6InNyYyI7czo3NDoiaHR0cDovLzEyNy4wLjAuMS9jb3JiZWlsYXNzb2Mvd3AtY29udGVudC91cGxvYWRzL3d5c2lqYS9kaXZpZGVycy9zb2xpZC5qcGciO3M6NToid2lkdGgiO2k6NTY0O3M6NjoiaGVpZ2h0IjtpOjE7fXM6NzoiYmxvY2stNiI7YTo2OntzOjQ6InRleHQiO2E6MTp7czo1OiJ2YWx1ZSI7czozMzY6IlBHZ3lQanh6ZEhKdmJtYyt3NGwwWVhCbElETWdPand2YzNSeWIyNW5QaUJrdzZsd2IzTmxjaUJrZFNCamIyNTBaVzUxSUdsamFUd3ZhREkrUEhBK1IyeHBjM05sY2lCbGRDQmt3Nmx3YjNObGNpQThjM1J5YjI1blBuUmxlSFJsY3l3Z1lYSjBhV05zWlhNc0lIUERxWEJoY21GMFpYVnljeTQ4TDNOMGNtOXVaejRnVW1WbllYSmtaWG9ndzZBZ1pISnZhWFJsSUNFOEwzQStQSEErVm05MWN5QndiM1YyWlhvZ2JjT3FiV1VnWk1PcGNHOXpaWElnUEhOMGNtOXVaejUyYjNNZ2FXUER0RzVsY3lCa1pTQnl3Nmx6WldGMWVDQnpiMk5wWVhWNFBDOXpkSEp2Ym1jK0lHTnZiVzFsSUdObFkya2dPand2Y0Q0PSI7fXM6NToiaW1hZ2UiO047czo5OiJhbGlnbm1lbnQiO3M6NDoibGVmdCI7czo2OiJzdGF0aWMiO2I6MDtzOjg6InBvc2l0aW9uIjtpOjY7czo0OiJ0eXBlIjtzOjc6ImNvbnRlbnQiO31zOjc6ImJsb2NrLTciO2E6NTp7czo1OiJ3aWR0aCI7aToxODQ7czo5OiJhbGlnbm1lbnQiO3M6NjoiY2VudGVyIjtzOjU6Iml0ZW1zIjthOjM6e2k6MDthOjc6e3M6MzoidXJsIjtzOjM4OiJodHRwOi8vd3d3LmZhY2Vib29rLmNvbS9tYWlscG9ldHBsdWdpbiI7czozOiJhbHQiO3M6ODoiRmFjZWJvb2siO3M6OToiY2VsbFdpZHRoIjtpOjYxO3M6MTA6ImNlbGxIZWlnaHQiO2k6MzI7czozOiJzcmMiO3M6ODg6Imh0dHA6Ly8xMjcuMC4wLjEvY29yYmVpbGFzc29jL3dwLWNvbnRlbnQvdXBsb2Fkcy93eXNpamEvYm9va21hcmtzL21lZGl1bS8wMi9mYWNlYm9vay5wbmciO3M6NToid2lkdGgiO2k6MzI7czo2OiJoZWlnaHQiO2k6MzI7fWk6MTthOjc6e3M6MzoidXJsIjtzOjMyOiJodHRwOi8vd3d3LnR3aXR0ZXIuY29tL21haWxfcG9ldCI7czozOiJhbHQiO3M6NzoiVHdpdHRlciI7czo5OiJjZWxsV2lkdGgiO2k6NjE7czoxMDoiY2VsbEhlaWdodCI7aTozMjtzOjM6InNyYyI7czo4NzoiaHR0cDovLzEyNy4wLjAuMS9jb3JiZWlsYXNzb2Mvd3AtY29udGVudC91cGxvYWRzL3d5c2lqYS9ib29rbWFya3MvbWVkaXVtLzAyL3R3aXR0ZXIucG5nIjtzOjU6IndpZHRoIjtpOjMyO3M6NjoiaGVpZ2h0IjtpOjMyO31pOjI7YTo3OntzOjM6InVybCI7czozMzoiaHR0cHM6Ly9wbHVzLmdvb2dsZS5jb20vK01haWxwb2V0IjtzOjM6ImFsdCI7czo2OiJHb29nbGUiO3M6OToiY2VsbFdpZHRoIjtpOjYxO3M6MTA6ImNlbGxIZWlnaHQiO2k6MzI7czozOiJzcmMiO3M6ODY6Imh0dHA6Ly8xMjcuMC4wLjEvY29yYmVpbGFzc29jL3dwLWNvbnRlbnQvdXBsb2Fkcy93eXNpamEvYm9va21hcmtzL21lZGl1bS8wMi9nb29nbGUucG5nIjtzOjU6IndpZHRoIjtpOjMyO3M6NjoiaGVpZ2h0IjtpOjMyO319czo4OiJwb3NpdGlvbiI7aTo3O3M6NDoidHlwZSI7czo3OiJnYWxsZXJ5Ijt9czo3OiJibG9jay04IjthOjU6e3M6ODoicG9zaXRpb24iO2k6ODtzOjQ6InR5cGUiO3M6NzoiZGl2aWRlciI7czozOiJzcmMiO3M6NzQ6Imh0dHA6Ly8xMjcuMC4wLjEvY29yYmVpbGFzc29jL3dwLWNvbnRlbnQvdXBsb2Fkcy93eXNpamEvZGl2aWRlcnMvc29saWQuanBnIjtzOjU6IndpZHRoIjtpOjU2NDtzOjY6ImhlaWdodCI7aToxO31zOjc6ImJsb2NrLTkiO2E6Njp7czo0OiJ0ZXh0IjthOjE6e3M6NToidmFsdWUiO3M6MjAwOiJQR2d5UGp4emRISnZibWMrdzRsMFlYQmxJRFFnT2p3dmMzUnliMjVuUGlCbGRDQnNaU0J3YVdWa0lHUmxJSEJoWjJVZ1B6d3ZhREkrUEhBK1EyaGhibWRsY2lCc1pTQmpiMjUwWlc1MUlHUjFJSEJwWldRZ1pHVWdjR0ZuWlNCa1lXNXpJR3hsY3lBOGMzUnliMjVuUGxCaGNtRnR3NmgwY21WelBDOXpkSEp2Ym1jK0lHUmxJRTFoYVd4UWIyVjBMand2Y0Q0PSI7fXM6NToiaW1hZ2UiO047czo5OiJhbGlnbm1lbnQiO3M6NDoibGVmdCI7czo2OiJzdGF0aWMiO2I6MDtzOjg6InBvc2l0aW9uIjtpOjk7czo0OiJ0eXBlIjtzOjc6ImNvbnRlbnQiO319czo2OiJmb290ZXIiO2E6NTp7czo0OiJ0ZXh0IjtOO3M6NToiaW1hZ2UiO2E6NTp7czozOiJzcmMiO3M6MTEyOiJodHRwOi8vMTI3LjAuMC4xL2NvcmJlaWxhc3NvYy93cC1jb250ZW50L3BsdWdpbnMvd3lzaWphLW5ld3NsZXR0ZXJzL2ltZy9kZWZhdWx0LW5ld3NsZXR0ZXIvbmV3c2xldHRlci9mb290ZXIucG5nIjtzOjU6IndpZHRoIjtpOjYwMDtzOjY6ImhlaWdodCI7aTo0NjtzOjk6ImFsaWdubWVudCI7czo2OiJjZW50ZXIiO3M6Njoic3RhdGljIjtiOjA7fXM6OToiYWxpZ25tZW50IjtzOjY6ImNlbnRlciI7czo2OiJzdGF0aWMiO2I6MDtzOjQ6InR5cGUiO3M6NjoiZm9vdGVyIjt9fQ==', 'YToxMDp7czo0OiJodG1sIjthOjE6e3M6MTA6ImJhY2tncm91bmQiO3M6NjoiZThlOGU4Ijt9czo2OiJoZWFkZXIiO2E6MTp7czoxMDoiYmFja2dyb3VuZCI7czo2OiJlOGU4ZTgiO31zOjQ6ImJvZHkiO2E6NDp7czo1OiJjb2xvciI7czo2OiIwMDAwMDAiO3M6NjoiZmFtaWx5IjtzOjU6IkFyaWFsIjtzOjQ6InNpemUiO2k6MTY7czoxMDoiYmFja2dyb3VuZCI7czo2OiJmZmZmZmYiO31zOjY6ImZvb3RlciI7YToxOntzOjEwOiJiYWNrZ3JvdW5kIjtzOjY6ImU4ZThlOCI7fXM6MjoiaDEiO2E6Mzp7czo1OiJjb2xvciI7czo2OiIwMDAwMDAiO3M6NjoiZmFtaWx5IjtzOjEyOiJUcmVidWNoZXQgTVMiO3M6NDoic2l6ZSI7aTo0MDt9czoyOiJoMiI7YTozOntzOjU6ImNvbG9yIjtzOjY6IjQyNDI0MiI7czo2OiJmYW1pbHkiO3M6MTI6IlRyZWJ1Y2hldCBNUyI7czo0OiJzaXplIjtpOjMwO31zOjI6ImgzIjthOjM6e3M6NToiY29sb3IiO3M6NjoiNDI0MjQyIjtzOjY6ImZhbWlseSI7czoxMjoiVHJlYnVjaGV0IE1TIjtzOjQ6InNpemUiO2k6MjQ7fXM6MToiYSI7YToyOntzOjU6ImNvbG9yIjtzOjY6IjRhOTFiMCI7czo5OiJ1bmRlcmxpbmUiO2I6MDt9czoxMToidW5zdWJzY3JpYmUiO2E6MTp7czo1OiJjb2xvciI7czo2OiIwMDAwMDAiO31zOjExOiJ2aWV3YnJvd3NlciI7YTozOntzOjU6ImNvbG9yIjtzOjY6IjAwMDAwMCI7czo2OiJmYW1pbHkiO3M6NToiQXJpYWwiO3M6NDoic2l6ZSI7aToxMjt9fQ=='),
(2, 0, 'Confirmation de votre abonnement à Associations de Corbeil-Essonnes', 'Bonjour ! \r\n\r\nFélicitations ! Vous êtes abonné(e) à notre site.\r\nVous devez cependant activer votre abonnement à [lists_to_confirm] en cliquant sur le lien ci-dessous : \r\n\r\n[activation_link]Cliquez ici pour confirmer votre abonnement.[/activation_link]\r\n\r\nMerci, \r\n\r\nL''équipe !\r\n', 1426946055, 1426946055, NULL, 'info@127.0.0.1', 'CorbeilAssoc', 'info@127.0.0.1', 'Webmaster CorbeilAssoc', NULL, 99, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `crbass_wysija_email_user_stat`
--

CREATE TABLE IF NOT EXISTS `crbass_wysija_email_user_stat` (
  `user_id` int(10) unsigned NOT NULL,
  `email_id` int(10) unsigned NOT NULL,
  `sent_at` int(10) unsigned NOT NULL,
  `opened_at` int(10) unsigned DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`email_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `crbass_wysija_email_user_url`
--

CREATE TABLE IF NOT EXISTS `crbass_wysija_email_user_url` (
  `email_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `url_id` int(10) unsigned NOT NULL,
  `clicked_at` int(10) unsigned DEFAULT NULL,
  `number_clicked` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`email_id`,`url_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `crbass_wysija_form`
--

CREATE TABLE IF NOT EXISTS `crbass_wysija_form` (
  `form_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` tinytext CHARACTER SET utf8 COLLATE utf8_bin,
  `data` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `styles` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `subscribed` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`form_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `crbass_wysija_form`
--

INSERT INTO `crbass_wysija_form` (`form_id`, `name`, `data`, `styles`, `subscribed`) VALUES
(1, 'Abonnez-vous à notre newsletter', 'YTo0OntzOjc6InZlcnNpb24iO3M6MzoiMC40IjtzOjg6InNldHRpbmdzIjthOjQ6e3M6MTA6Im9uX3N1Y2Nlc3MiO3M6NzoibWVzc2FnZSI7czoxNToic3VjY2Vzc19tZXNzYWdlIjtzOjkzOiJWw6lyaWZpZXogdm90cmUgYm/DrnRlIGRlIHLDqWNlcHRpb24gb3Ugdm9zIGluZMOpc2lyYWJsZXMgYWZpbiBkZSBjb25maXJtZXIgdm90cmUgYWJvbm5lbWVudC4iO3M6NToibGlzdHMiO2E6MTp7aTowO3M6MToiMSI7fXM6MTc6Imxpc3RzX3NlbGVjdGVkX2J5IjtzOjU6ImFkbWluIjt9czo0OiJib2R5IjthOjI6e2k6MDthOjQ6e3M6NDoibmFtZSI7czo2OiJFLW1haWwiO3M6NDoidHlwZSI7czo1OiJpbnB1dCI7czo1OiJmaWVsZCI7czo1OiJlbWFpbCI7czo2OiJwYXJhbXMiO2E6Mjp7czo1OiJsYWJlbCI7czo2OiJFLW1haWwiO3M6ODoicmVxdWlyZWQiO2I6MTt9fWk6MTthOjQ6e3M6NDoibmFtZSI7czo3OiJFbnZveWVyIjtzOjQ6InR5cGUiO3M6Njoic3VibWl0IjtzOjU6ImZpZWxkIjtzOjY6InN1Ym1pdCI7czo2OiJwYXJhbXMiO2E6MTp7czo1OiJsYWJlbCI7czoxMzoiSmUgbSdhYm9ubmUgISI7fX19czo3OiJmb3JtX2lkIjtpOjE7fQ==', NULL, 0);

-- --------------------------------------------------------

--
-- Structure de la table `crbass_wysija_list`
--

CREATE TABLE IF NOT EXISTS `crbass_wysija_list` (
  `list_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `namekey` varchar(255) DEFAULT NULL,
  `description` text,
  `unsub_mail_id` int(10) unsigned NOT NULL DEFAULT '0',
  `welcome_mail_id` int(10) unsigned NOT NULL DEFAULT '0',
  `is_enabled` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `is_public` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `created_at` int(10) unsigned DEFAULT NULL,
  `ordering` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`list_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `crbass_wysija_list`
--

INSERT INTO `crbass_wysija_list` (`list_id`, `name`, `namekey`, `description`, `unsub_mail_id`, `welcome_mail_id`, `is_enabled`, `is_public`, `created_at`, `ordering`) VALUES
(1, 'Ma première liste', 'ma-premiere-liste', 'Cette liste a été créée automatiquement lors de l''installation de MailPoet.', 0, 0, 1, 1, 1426946054, 0),
(2, 'Utilisateurs WordPress', 'users', 'La liste créée automatiquement lors de l''importation des abonnés de l''extension : "WordPress', 0, 0, 0, 0, 1426946054, 0);

-- --------------------------------------------------------

--
-- Structure de la table `crbass_wysija_queue`
--

CREATE TABLE IF NOT EXISTS `crbass_wysija_queue` (
  `user_id` int(10) unsigned NOT NULL,
  `email_id` int(10) unsigned NOT NULL,
  `send_at` int(10) unsigned NOT NULL DEFAULT '0',
  `priority` tinyint(4) NOT NULL DEFAULT '0',
  `number_try` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`email_id`),
  KEY `SENT_AT_INDEX` (`send_at`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `crbass_wysija_url`
--

CREATE TABLE IF NOT EXISTS `crbass_wysija_url` (
  `url_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `url` text,
  PRIMARY KEY (`url_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `crbass_wysija_url_mail`
--

CREATE TABLE IF NOT EXISTS `crbass_wysija_url_mail` (
  `email_id` int(11) NOT NULL AUTO_INCREMENT,
  `url_id` int(10) unsigned NOT NULL,
  `unique_clicked` int(10) unsigned NOT NULL DEFAULT '0',
  `total_clicked` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`email_id`,`url_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `crbass_wysija_user`
--

CREATE TABLE IF NOT EXISTS `crbass_wysija_user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `wpuser_id` int(10) unsigned NOT NULL DEFAULT '0',
  `email` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL DEFAULT '',
  `lastname` varchar(255) NOT NULL DEFAULT '',
  `ip` varchar(100) NOT NULL,
  `confirmed_ip` varchar(100) NOT NULL DEFAULT '0',
  `confirmed_at` int(10) unsigned DEFAULT NULL,
  `last_opened` int(10) unsigned DEFAULT NULL,
  `last_clicked` int(10) unsigned DEFAULT NULL,
  `keyuser` varchar(255) NOT NULL DEFAULT '',
  `created_at` int(10) unsigned DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `domain` varchar(255) DEFAULT '',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `EMAIL_UNIQUE` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Contenu de la table `crbass_wysija_user`
--

INSERT INTO `crbass_wysija_user` (`user_id`, `wpuser_id`, `email`, `firstname`, `lastname`, `ip`, `confirmed_ip`, `confirmed_at`, `last_opened`, `last_clicked`, `keyuser`, `created_at`, `status`, `domain`) VALUES
(1, 1, 'contact@tiar-florian.fr', 'Mizi', '', '', '0', NULL, NULL, NULL, '63a854b5af9de947c0b6726cd2d3c081', 1426946055, 1, 'tiar-florian.fr'),
(6, 6, 'sisi@fr.fr', 'sisi', '', '127.0.0.1', '0', NULL, NULL, NULL, '7ec60ed548f4e37422ee8240bdde7068', 1430677548, 1, 'fr.fr'),
(7, 7, 'izi@bizi.fr', 'izi', '', '127.0.0.1', '0', NULL, NULL, NULL, 'e46fc7c48288d512e710563389e6360c', 1430678444, 1, 'bizi.fr'),
(5, 5, 'sam@bkc.fr', 'samy', '', '127.0.0.1', '0', NULL, NULL, NULL, '1af5bfe2f381944226ca8f18e60bca79', 1430673874, 1, 'bkc.fr');

-- --------------------------------------------------------

--
-- Structure de la table `crbass_wysija_user_field`
--

CREATE TABLE IF NOT EXISTS `crbass_wysija_user_field` (
  `field_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `column_name` varchar(250) NOT NULL DEFAULT '',
  `type` tinyint(3) unsigned DEFAULT '0',
  `values` text,
  `default` varchar(250) NOT NULL DEFAULT '',
  `is_required` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `error_message` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`field_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `crbass_wysija_user_field`
--

INSERT INTO `crbass_wysija_user_field` (`field_id`, `name`, `column_name`, `type`, `values`, `default`, `is_required`, `error_message`) VALUES
(1, 'Prénom', 'firstname', 0, NULL, '', 0, 'Merci de saisir le prénom'),
(2, 'Nom', 'lastname', 0, NULL, '', 0, 'Merci de saisir le nom de famille');

-- --------------------------------------------------------

--
-- Structure de la table `crbass_wysija_user_history`
--

CREATE TABLE IF NOT EXISTS `crbass_wysija_user_history` (
  `history_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `email_id` int(10) unsigned DEFAULT '0',
  `type` varchar(250) NOT NULL DEFAULT '',
  `details` text,
  `executed_at` int(10) unsigned DEFAULT NULL,
  `executed_by` int(10) unsigned DEFAULT NULL,
  `source` text,
  PRIMARY KEY (`history_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `crbass_wysija_user_list`
--

CREATE TABLE IF NOT EXISTS `crbass_wysija_user_list` (
  `list_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `sub_date` int(10) unsigned DEFAULT '0',
  `unsub_date` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`list_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `crbass_wysija_user_list`
--

INSERT INTO `crbass_wysija_user_list` (`list_id`, `user_id`, `sub_date`, `unsub_date`) VALUES
(1, 1, 1426946054, 0),
(2, 1, 1426946054, 0),
(2, 7, 1430678444, 0),
(2, 6, 1430677548, 0),
(2, 5, 1430673874, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
