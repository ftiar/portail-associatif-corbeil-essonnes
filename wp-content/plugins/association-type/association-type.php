<?php
/*
Plugin Name: Association Type
Description: Un plugin créant un type de contenu "Association" pour créer des fiches personnalisées
Version: 0.1
Author: Florian TIAR
Author URI: http://tiar-florian.fr
License: GPL2
*/
	
	/* Ajout du custom type Association */
	add_action( 'init', 'create_post_type' );
	function create_post_type() {
		register_post_type('association', array(
			'labels' => 
				array (
				    'name' => 'Associations',
				    'singular_name' => 'Association',
				    'add_new' => 'Nouvelle association',
				    'add_new_item' => 'Ajouter une nouvelle association',
				    'edit_item' => 'Modifier l\'association',
				    'new_item' => 'Nouvelle association',
				    'view_item' => 'Voir l\'association',
				    'search_items' => 'Chercher une association',
				    'not_found' => 'Aucune association trouvée',
				    'not_found_in_trash' => 'Aucune association trouvée dans la corbeille',
				    'parent_item_colon' => 'Association parente :',
				),

			'has_archive' => true,
			'query_var' => 'associations',
			'rewrite' => 
				array (
				    'slug' => 'associations',
				    'with_front' => true,
				    'pages' => true,
				    'feeds' => true,
				),

			'capabilities' => array(
				'publish_posts' => 'publish_associations',
				'edit_posts' => 'edit_associations',
				'edit_others_posts' => 'edit_others_associations',
				'delete_posts' => 'delete_associations',
				'delete_others_posts' => 'delete_others_associations',
				'read_private_posts' => 'read_private_associations',
				'edit_post' => 'edit_association',
				'delete_post' => 'delete_association',
				'read_post' => 'read_association',
			),
			// 'map_meta_cap' => true, // on autorise l'attribution des capacités

			'description' => "Les associations disposent de fiches personnalisées pour mieux les connaître",
	  		'public' => true,
			'menu_position' => 5,
			'menu_icon' => 'dashicons-analytics',
	  		'capability_type' => 'page',
	  		'hierarchical' => false,
	  		'supports' => array('title','editor','thumbnail', 'author'), // 'post-formats','custom-fields','author','page-attributes'
		));
	}

	add_action(  'draft_to_publish',  'on_publish_association_send_email', 10, 1 );
	function on_publish_association_send_email( $post ) {
		if ( $post->post_type !== "association" ) {
			return; // Stop the script
		}

		$user_info = get_userdata( $post->post_author );
		$message = 'Félicitation, votre association a été acceptée sur notre plateforme. Vous pouvez désormais y ajouter vos articles et vos événements.';
		$message .= "\n\n";
		$message .= '<a href="' . get_home_url( '/connexion/' ) . '">Connexion</a>';
		$headers = 'From: ' . get_bloginfo( 'name' ) . ' <' . get_bloginfo('admin_email') . "> \r\n";

		$email_sent = wp_mail( $user_info->user_email, 'Votre association a été approuvée', $message, $headers );
	}


	add_action(  'draft_to_publish',  'on_publish_post_send_email', 10, 1 );
	function on_publish_post_send_email( $post ) {
		if ( $post->post_type !== "post" ) {
			return; // Stop the script
		}

		$user_info = get_userdata( $post->post_author );
		$message = 'Bonjour, votre article "' . get_the_title($post) . '" vient d\'être publié.';
		$message .= "\n\n";
		$message .= '<a href="' . get_permalink($post) . '">' . get_permalink($post) . '</a>';
		$headers = 'From: ' . get_bloginfo( 'name' ) . ' <' . get_bloginfo('admin_email') . "> \r\n";

		$email_sent = wp_mail( $user_info->user_email, 'Votre article a été publié', $message, $headers );
	}


	add_action(  'draft_to_publish',  'on_publish_event_send_email', 10, 1 );
	function on_publish_event_send_email( $post ) {
		if ( $post->post_type !== "event" ) {
			return; // Stop the script
		}

		$user_info = get_userdata( $post->post_author );
		$message = 'Bonjour, votre événement "' . get_the_title($post) . '" vient d\'être publié.';
		$message .= "\n\n";
		$message .= '<a href="' . get_permalink($post) . '">' . get_permalink($post) . '</a>';
		$headers = 'From: ' . get_bloginfo( 'name' ) . ' <' . get_bloginfo('admin_email') . "> \r\n";

		$email_sent = wp_mail( $user_info->user_email, 'Votre événement a été publié', $message, $headers );
	}

	add_action( 'init', 'register_acf_fields' );
	function register_acf_fields() {
		require __DIR__ . '/assets/acf/associations.php';
	}
