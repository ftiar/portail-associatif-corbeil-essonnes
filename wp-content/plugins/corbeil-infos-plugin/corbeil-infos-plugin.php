<?php
/*
    Plugin Name: Corbeil Infos Widget
    Description: Un widget qui affiche les 4 dernières actualités du site corbeil-infos.fr
    Version: 1.0.4
    Author: Florian TIAR
    Author URI: http://tiar-florian.fr
    License: GPL2
 */

add_action("widgets_init", "corbeilinfos_init");

function corbeilinfos_init(){
    register_widget("corbeilinfos_widget");
}

class corbeilinfos_widget extends WP_widget {

    function corbeilinfos_widget(){
		$options = array(
			"classname" => "corbeilinfos-widget",
			"description" => "Widget qui affiche les 4 dernières actualités du site Corbeil Infos"
		);
	    parent::__construct( "widget-corbeilinfos", "Widget Corbeil-Infos", $options );
    }

    function widget( $args, $d ){

        $xml_call = wp_remote_get("http://www.corbeil-infos.fr/rss_1.xml");
	    if ( is_wp_error($xml_call) ) {
		    return;
	    }
        $xml_response = wp_remote_retrieve_body($xml_call);
	    if ( $xml_response === '' ) {
		    return;
	    }
        $xml = simplexml_load_string($xml_response);
	    if ( $xml == false ) {
		    return;
	    }

        echo '<aside id="corbeilinfos" class="widget widget_meta">';
            echo "<h1 class='widget-title'>Actualité Corbeil-infos</h1>";

            echo '<ul>';
                for ( $i=3; $i>=0; $i-- ) { // Affichage du plus récent au plus vieux
	                $dateArticle = date_i18n( get_option( 'date_format' ), strtotime( $xml->channel->item[$i]->pubDate ) );
                    printf("<li><a href='%s'>%s - %s (%s)</a></li>", $xml->channel->item[$i]->link, $xml->channel->item[$i]->category, $xml->channel->item[$i]->title, $dateArticle);
                }
            echo '</ul>';
        echo '</aside>';
    }
}

?>