var $j = jQuery.noConflict();

$j(function(){
    'use strict';

    $j('.datetimepicker').datetimepicker({
        format:	'd/m/Y H:i',
        //formatDate:'d/m/Y',
        dayOfWeekStart: 1,
        minDate: 0,
        lang: 'fr'
    });
});