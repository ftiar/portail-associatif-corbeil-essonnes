<?php
/**
*    Plugin Name: Event Organiser FrontEnd Form
*    Description: Un plugin permettant à l'utilisateur de créer son propre événement
*    Version: 1.0
*    Author: Florian TIAR
*    Author URI: http://tiar-florian.fr
*    License: GPL2
*
 * This is a basic example of implementing front end creation of events with Event Organiser
 * @see http://www.stephenharris.info/2012/front-end-event-posting/
 * @see http://wordpress.org/extend/plugins/event-organiser/
 *
*/
add_shortcode('my_event_form','my_event_form_shortcode_handler');
function my_event_form_shortcode_handler( ){

	if( ! is_user_logged_in() ){
		return '<p>Seul les utilisateurs connectés peuvent publier un événement</p><a href="' . home_url('connexion') . '" class="button button-primary">Connexion</a>';
	}

	$associations = new WP_Query( array(
		'posts_per_page' => -1,
		'post_type'      => 'association',
		'post_status'    => 'publish',
		'author'         => get_the_author_meta('ID')
		)
	);

	if ( ! $associations->have_posts() ) {
		return '<p>Seul les utilisateurs possédant une association peuvent publier un événement</p><a href="' . home_url('ajouter-mon-association') . '" class="button button-primary">Créer mon association</a>';
	}

	wp_enqueue_style( 'jquery-datepicker-style' );
	wp_enqueue_script( 'jquery-datepicker-script' );
	wp_enqueue_script( 'eo-fes-script' );

	$html = '<form method="POST" enctype="multipart/form-data">';

	//Create hidden 'action' field and corresponding nonce
	$html .= '<input type="hidden"	name="my-action" value="post-event" >';
	$html .= wp_nonce_field( 'post-event', '_mynonce', false, false );

	$html .= '<p><label for="my-frontend-event-association">Association</label>
         		<select name="my_frontend_event[association]">';

	foreach ( $associations->posts as $assoc ) {
		$html .= '<option value="' . $assoc->ID . '">' . $assoc->post_title . '</option>';
	}
	wp_reset_postdata();

	$html .= '</select></p>';

	// Event title
	$html .= sprintf('<p><label for="my-frontend-event-%1$s"> %2$s *
				<input type="text" name="my_frontend_event[%1$s]" id="my-frontend-event-%1$s" required >
			</label></p>',
			'title',
			'Nom de l\'événement'
	);

	// Featured image
	$html .= sprintf('<p><label for="my-frontend-event-%1$s"> %2$s
				<input type="file" name="%1$s" id="my-frontend-event-%1$s" >
			</label></p>',
		'image',
		'Image de l\'événement'
	);

	//Event Description
	$html .= sprintf('<p><label for="my-frontend-event-%1$s"> %2$s
				<textarea name="my_frontend_event[%1$s]" id="my-frontend-event-%1$s"></textarea>
			</label></p>',
		'description',
		'Description de l\'événement'
	);

	//Start date
	$html .= sprintf('<p><label for="my-frontend-event-%1$s"> %2$s *
				<input type="text" name="my_frontend_event[%1$s]" id="my-frontend-event-%1$s" class="datetimepicker"
				placeholder="JJ/MM/AAAA HH:MM" required >
			</label></p>',
			'startdate',
			'Date de début'
	);

	//End date
	$html .= sprintf('<p><label for="my-frontend-event-%1$s"> %2$s *
				<input type="text" name="my_frontend_event[%1$s]" id="my-frontend-event-%1$s" class="datetimepicker"
				placeholder="JJ/MM/AAAA HH:MM" required >
			</label></p>',
			'enddate',
			'Date de fin'
	);

	//Venue
	$html .= sprintf('<p><label for="my-frontend-event-%1$s"> %2$s *
				<input type="text" name="my_frontend_event[%1$s]" id="my-frontend-event-%1$s" required >
			</label></p>',
		'venue',
		'Lieu'
	);

	//Category - checklist
	$cats = get_terms( 'event-category', array('hide_empty' => 0) );
	if( $cats ){
		$html .= '<p><label for="my-frontend-event-category">Catégorie <ul>';
		foreach ( $cats as $cat ) {
			$html .= sprintf('<label for="my-frontend-event-%1$s-%2$d">
									<input type="checkbox" name="my_frontend_event[%1$s][]" value="%2$s" id="my-frontend-event-%1$s-%2$d" >
									%3$s
								</label>',
					'category',
					$cat->term_id,
					esc_html($cat->name)
					);
		}
		$html .= '</label></p>';
	}

	//The post button
	$html .= '<p><input name="submit" type="submit" id="submit" value="Valider"></p>';

	$html .='</form>';

	return $html;
}

add_action('init','my_frontend_event_post_listner');
function my_frontend_event_post_listner() {

	if( ! isset($_POST['my-action']) || 'post-event' != $_POST['my-action'] ) {
		return;
	}

	// Check nonce
	check_admin_referer( 'post-event',  '_mynonce');

	// Check user is logged in:
	if( ! is_user_logged_in() ) {
		return;
	}

	// Collect raw input
	$input = $_POST['my_frontend_event'];

	$user_venue = sanitize_text_field($input['venue']);
	$venue = eo_get_venue_by( 'name', $user_venue);
	if( $venue != false ) { // If venue already exist, dont create it again
		$event_venue = (int) $venue->term_id;
	} else {
		$venue_term = eo_insert_venue( $user_venue );
		$event_venue = isset( $venue_term['term_id'] ) ? (int) $venue_term['term_id'] : 0;
	}

	// Event cats are an array of IDs
	$event_cats = ! empty( $input['category'] ) ? $input['category'] : array();
	$event_cats = array_map( 'intval', $event_cats );

	$post_data = array(
		'post_title' => sanitize_text_field( $input['title'] ),
		'post_content' => wp_kses_post( $input['description'] ),
		'tax_input' => array(
			'event-venue' => array($event_venue),
			'event-category' => $event_cats,
		)
	);


	/**
	 * Set the event data
	 */
	// Start and end dates need to be given as DateTime objects (timezone is UTC unless a timezone is given)
	$start = DateTime::createFromFormat( "d/m/Y H:i", sanitize_text_field($input['startdate']), eo_get_blog_timezone() );
	$end = DateTime::createFromFormat( "d/m/Y H:i", sanitize_text_field($input['enddate']), eo_get_blog_timezone() );
	$event_data = array(
		'schedule' =>'once',  //specifies the reoccurrence pattern
		'all_day' =>  0, //1 if its an all day event, 0 if not - if not you'll need to specify a start/end time for the DateTimeobjects
		'start' =>  $start, //start date (of first occurrence)  as a datetime object
		'end' =>  $end, //end date (of first occurrence)  as a datetime object
	);

	// Finally, Insert event.
	$post_id = eo_insert_event( $post_data, $event_data );

	// Link event association
	add_post_meta( $post_id, 'event_p2p2_association', absint($input['association']) );

	// save Featured image
	if ( $_FILES['image']['error'] > 0 ) {
		return;
	}

	$wp_upload_dir = wp_upload_dir();
	$file = $_FILES['image'];
	$uploadfile = $wp_upload_dir['path'] . '/' . basename( $file['name'] );

	$move = move_uploaded_file( $file['tmp_name'] , $uploadfile );
	if ( $move == false ) {
		return;
	}
	$filename = basename( $uploadfile );

	$wp_filetype = wp_check_filetype( basename( $filename ), null );

	$attachment = array(
		'guid'           => $wp_upload_dir['url'] . '/' . basename( $filename ),
		'post_mime_type' => $wp_filetype['type'],
		'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
		'post_content'   => '',
		'post_status'    => 'inherit'
	);
	$attach_id = wp_insert_attachment( $attachment, $uploadfile, $post_id );
	if ( $attach_id == 0 ) {
		return;
	}

	// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
	require_once( ABSPATH . 'wp-admin/includes/image.php' );

	// Generate the metadata for the attachment, and update the database record.
	$attach_data = wp_generate_attachment_metadata( $attach_id, $uploadfile );
	wp_update_attachment_metadata( $attach_id, $attach_data );

	set_post_thumbnail( $post_id, $attach_id );
}

add_shortcode('my_event_form_edit','my_event_form_edit_shortcode_handler');
function my_event_form_edit_shortcode_handler( ){

	if( ! is_user_logged_in() ){
		return '<p>Seul les utilisateurs connectés peuvent publier un événement</p><a href="' . home_url('connexion') . '" class="button button-primary">Connexion</a>';
	}

	if ( ! isset($_GET['event_id']) ) {
		return '<p>Aucun événement sélectionné</p><a href="' . home_url('evenements') . '" class="button button-primary">Liste des événements</a>';
	}
	$event_id = absint( $_GET['event_id'] );

	$event = get_post( $event_id );
	if ( is_null($event) ) {
		return '<p>Evénement introuvable</p><a href="' . home_url('evenements') . '" class="button button-primary">Liste des événements</a>';
	}

	if ( (int) $event->post_author !== get_current_user_id() ) {
		return '<p>Vous n\'êtes pas l\'auteur de cet événement</p><a href="' . home_url('evenements') . '" class="button button-primary">Liste des événements</a>';
	}

	wp_enqueue_style( 'jquery-datepicker-style' );
	wp_enqueue_script( 'jquery-datepicker-script' );
	wp_enqueue_script( 'eo-fes-script' );

	$html = '<form method="POST" enctype="multipart/form-data">';

	//Create hidden 'action' field and corresponding nonce
	$html .= '<input type="hidden"	name="my-action" value="post-event-edit" >';
	$html .= wp_nonce_field( 'post-event-edit', '_mynonce', false, false );

	// Event title
	$html .= sprintf('<p><label for="my-frontend-event-%1$s"> %2$s *
				<input type="text" name="my_frontend_event[%1$s]" id="my-frontend-event-%1$s" required
				value="%3$s">
			</label></p>',
		'title',
		'Nom de l\'événement',
		esc_html( $event->post_title )
	);

	// Featured image
	$event_image = get_the_post_thumbnail( $event->ID, 'thumbnail' );
	if ( $event_image !== '' ) {
		echo '<p>Ancienne image de l\'événement</p>';
		echo $event_image;
	}

	$html .= sprintf('<p><label for="my-frontend-event-%1$s"> %2$s
				<input type="file" name="%1$s" id="my-frontend-event-%1$s" >
			</label></p>',
		'image',
		'Image de l\'événement'
	);

	//Event Description
	$html .= sprintf('<p><label for="my-frontend-event-%1$s"> %2$s
				<textarea name="my_frontend_event[%1$s]" id="my-frontend-event-%1$s">%3$s</textarea>
			</label></p>',
		'description',
		'Description de l\'événement',
		esc_textarea( $event->post_content )
	);

	$dates = eo_get_the_occurrences_of( $event->ID );
	$date = reset($dates); // get the first occurence
	$start = $date['start']; // DateTime object
	$end = $date['end']; // DateTime object

	//Start date
	$html .= sprintf('<p><label for="my-frontend-event-%1$s"> %2$s *
				<input type="text" name="my_frontend_event[%1$s]" id="my-frontend-event-%1$s" class="datetimepicker"
				placeholder="JJ/MM/AAAA HH:MM" required value="%3$s">
			</label></p>',
		'startdate',
		'Date de début',
		$start->format('d/m/Y H:i')
	);

	//End date
	$html .= sprintf('<p><label for="my-frontend-event-%1$s"> %2$s *
				<input type="text" name="my_frontend_event[%1$s]" id="my-frontend-event-%1$s" class="datetimepicker"
				placeholder="JJ/MM/AAAA HH:MM" required value="%3$s">
			</label></p>',
		'enddate',
		'Date de fin',
		$end->format('d/m/Y H:i')
	);

	//Venue
	$venue_id = eo_get_venue($event->ID);
	$venue_name = eo_get_venue_name($venue_id);
	$html .= sprintf('<p><label for="my-frontend-event-%1$s"> %2$s *
				<input type="text" name="my_frontend_event[%1$s]" id="my-frontend-event-%1$s" required value="%3$s">
			</label></p>',
		'venue',
		'Lieu',
		$venue_name
	);

	//Category - checklist
	$cats = get_terms( 'event-category', array('hide_empty' => 0) );
	$event_cats = wp_get_post_terms( $event->ID, 'event-category', array('hide_empty' => 0) );
	if( $cats ){
		$html .= '<p><label for="my-frontend-event-category">Catégorie <ul>';
		foreach ( $cats as $cat ) {
			$checked = '';
			if ( ! is_wp_error($event_cats) && ! empty($event_cats) ) {
				foreach( $event_cats as $event_cat ) {
					$checked = checked( $cat->term_id, $event_cat->term_id, false);
					if ( $checked !== '' ) {
						break;
					}
				}
			}
			$html .= sprintf('<label for="my-frontend-event-%1$s-%2$d">
								<input type="checkbox" name="my_frontend_event[%1$s][]" value="%2$s"
								%4$s
								id="my-frontend-event-%1$s-%2$d" >
								%3$s
							</label>',
				'category',
				$cat->term_id,
				esc_html($cat->name),
				$checked
			);
		}
		$html .= '</label></p>';
	}

	//The post button
	$html .= '<p><input name="submit" type="submit" id="submit" value="Valider"></p>';

	$html .='</form>';

	return $html;
}

add_action('init','my_frontend_event_edit_post_listner');
function my_frontend_event_edit_post_listner() {

	if( ! isset($_POST['my-action']) || 'post-event-edit' != $_POST['my-action'] ) {
		return;
	}

	// Check nonce
	check_admin_referer( 'post-event-edit',  '_mynonce');

	// Check user is logged in:
	if( ! is_user_logged_in() ) {
		return;
	}

	if ( ! isset($_GET['event_id']) ) {
		return;
	}
	$event_id = absint( $_GET['event_id'] );

	$event = get_post( $event_id );
	if ( is_null($event) ) {
		return;
	}

	if ( (int) $event->post_author !== get_current_user_id() ) {
		return;
	}

	// Collect raw input
	$input = $_POST['my_frontend_event'];

	$user_venue = sanitize_text_field($input['venue']);
	$venue = eo_get_venue_by( 'name', $user_venue);
	if( $venue != false ) { // If venue already exist, dont create it again
		$event_venue = (int) $venue->term_id;
	} else {
		$venue_term = eo_insert_venue( $user_venue );
		$event_venue = isset( $venue_term['term_id'] ) ? (int) $venue_term['term_id'] : 0;
	}

	// Event cats are an array of IDs
	$event_cats = ! empty( $input['category'] ) ? $input['category'] : array();
	$event_cats = array_map( 'intval', $event_cats );

	$post_data = array(
		'post_title' => sanitize_text_field( $input['title'] ),
		'post_content' => wp_kses_post( $input['description'] ),
		'post_status'   =>  'pending',
		'tax_input' => array(
			'event-venue' => array($event_venue),
			'event-category' => $event_cats,
		)
	);

	/**
	 * Set the event data
	*/
	// Start and end dates need to be given as DateTime objects (timezone is UTC unless a timezone is given)
	$start = DateTime::createFromFormat( "d/m/Y H:i", sanitize_text_field($input['startdate']), eo_get_blog_timezone() );
	$end = DateTime::createFromFormat( "d/m/Y H:i", sanitize_text_field($input['enddate']), eo_get_blog_timezone() );
	$event_data = array(
		'schedule' =>'once',  //specifies the reoccurrence pattern
		'all_day' =>  0, //1 if its an all day event, 0 if not - if not you'll need to specify a start/end time for the DateTimeobjects
		'start' =>  $start, //start date (of first occurrence)  as a datetime object
		'end' =>  $end, //end date (of first occurrence)  as a datetime object
	);

	// Finally, Update event.
	$post_id = eo_update_event( $event_id, $post_data, $event_data );

	// save Featured image
	if ( $_FILES['image']['error'] > 0 ) {
		return;
	}

	$wp_upload_dir = wp_upload_dir();
	$file = $_FILES['image'];
	$uploadfile = $wp_upload_dir['path'] . '/' . basename( $file['name'] );

	$move = move_uploaded_file( $file['tmp_name'] , $uploadfile );
	if ( $move == false ) {
		return;
	}
	$filename = basename( $uploadfile );

	$wp_filetype = wp_check_filetype( basename( $filename ), null );

	$attachment = array(
		'guid'           => $wp_upload_dir['url'] . '/' . basename( $filename ),
		'post_mime_type' => $wp_filetype['type'],
		'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
		'post_content'   => '',
		'post_status'    => 'inherit'
	);
	$attach_id = wp_insert_attachment( $attachment, $uploadfile, $post_id );
	if ( $attach_id == 0 ) {
		return;
	}

	// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
	require_once( ABSPATH . 'wp-admin/includes/image.php' );

	// Generate the metadata for the attachment, and update the database record.
	$attach_data = wp_generate_attachment_metadata( $attach_id, $uploadfile );
	wp_update_attachment_metadata( $attach_id, $attach_data );

	set_post_thumbnail( $post_id, $attach_id );
}

add_action( 'wp_enqueue_scripts', 'eo_fes_enqueue_datepicker' );
function eo_fes_enqueue_datepicker() {
	wp_register_style( 'jquery-datepicker-style', plugins_url( '/assets/css/jquery.datetimepicker.css' , __FILE__ ), array(), '2.4.5' );
	wp_register_script( 'jquery-datepicker-script', plugins_url( '/assets/js/jquery.datetimepicker.js' , __FILE__ ), array('jquery'), '2.4.5', true );
	wp_register_script( 'eo-fes-script', plugins_url( '/assets/js/eo_fes_script.js' , __FILE__ ), array('jquery-datepicker-script'), '1.0.0', true );
}