<?php
/**
*    Plugin Name: Event Organiser - Connect Associations
*    Description: Un plugin permettant de lier un événement à une association
*    Version: 1.0
*    Author: Florian TIAR
*    Author URI: http://tiar-florian.fr
*    License: GPL2
*/

add_action( 'add_meta_boxes', 'add_event_p2p2_association_metabox' );
function add_event_p2p2_association_metabox(){
	add_meta_box(
		'event_association',
		'Association',
		'event_p2p2_association_metabox_content',
		'event',
		'side',
		'default'
	);
}

function event_p2p2_association_metabox_content() {
	$associations = get_posts( array(
			'posts_per_page' => -1,
			'post_type'      => 'association',
			'post_status'    => 'publish',
		)
	);

	if ( ! is_array($associations) || empty($associations) ) {
		return;
	}

	wp_nonce_field( plugin_basename( __FILE__ ), 'event_p2p2_association_nonce' );
	$assoc_id = (int) get_post_meta( get_the_ID(), 'event_p2p2_association', true );

	echo "<p>Association liée à cet événement</p>";
	echo "<select id='event_p2p2_association' name='event_p2p2_association'>";

	echo '<option value=0>Aucune association</option>';
	foreach ( $associations as $association ) {
		$id = $association->ID;
		$selected = "";

		if( $id === $assoc_id ){
			$selected = ' selected="selected"';
		}
		echo '<option' . $selected . ' value=' . $id . '>' . $association->post_title . '</option>';
	}
	echo "</select>";
}

add_action('save_post', 'event_p2p2_save_assoc_metabox', 1, 2);
function event_p2p2_save_assoc_metabox( $post_id, $post ){
	// Don't wanna save this now, right?
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
		return;
	if ( ! isset( $_POST['event_p2p2_association_nonce'] ) )
		return;
	if ( ! wp_verify_nonce( $_POST['event_p2p2_association_nonce'], plugin_basename( __FILE__ ) ) )
		return;

	// We do want to save? Ok!
	$key = 'event_p2p2_association';
	$value = $_POST["event_p2p2_association"];
	if ( ! $value || $value == 0 ) {
		delete_post_meta( $post->ID, $key );
	}

	if ( get_post_meta( $post->ID, $key, FALSE ) ) { // If the custom field already has a value
		update_post_meta( $post->ID, $key, $value );
	} else { // If the custom field doesn't have a value
		add_post_meta( $post->ID, $key, $value );
	}
}

add_filter('manage_edit-event_columns', 'p2p2_add_event_columns');
function p2p2_add_event_columns( $columns ){
	$columns['p2p2_association'] = 'Association';

	return $columns;
}

add_action('manage_event_posts_custom_column', 'p2p2_fill_event_columns', 10, 2);
function p2p2_fill_event_columns( $column_name, $id ) {
	switch ($column_name) {
		case 'p2p2_association':
			$assoc_id = get_post_meta($id, 'event_p2p2_association', true);
			if ( $assoc_id == 0 ) {
				echo 'Aucune association';
			} else {
				$assoc = get_post($assoc_id);
				$permalink = get_permalink($assoc_id);
				echo "<a href='" . $permalink . "'>" . $assoc->post_title . "</a>";
			}
			break;
		default:
			break;
	} // end switch
}