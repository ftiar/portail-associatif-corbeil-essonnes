<?php
/**
*    Boite à outils : Toutes les fonctions créées pour aider
*/

/* Retourne un tableau de tous les rôles utilisateur Wordpress */
function get_roles() {
    $wp_roles = new WP_Roles();
    $roles = $wp_roles->get_names();
    $roles = array_map( 'translate_user_role', $roles );
 
    return $roles;
}

/* Retourne une date en français au format type "Mercredi 15 Avril 2015" */
function date_fran($date)
{
  $mois = array("Janvier", "Fevrier", "Mars",
                "Avril","Mai", "Juin", 
                "Juillet", "Août","Septembre",
                "Octobre", "Novembre", "Decembre");

  $jours= array("Dimanche", "Lundi", "Mardi",
                "Mercredi", "Jeudi", "Vendredi",
                "Samedi");

  $moisDate = $mois[$date->format("n")-1];
  $jourDate = $jours[$date->format("w")];

  return $jourDate." ".$date->format("j").($date->format("j")==1 ? "er":" ").
         $moisDate." ".$date->format("Y");
}

?>