<?php 

// Enregistrement de l'utilisateur
add_action( 'admin_post_nopriv_nouvel_utilisateur', 'ajouter_utilisateur' );

/* Formulaire d'inscription : Adhérents, Entreprises */
function register_user_form($atts) {
	//récupère les paramètres
	$atts = shortcode_atts(
		array(
			'user_role' => 'subscriber'
		), $atts
	);

	// On vérifie si l'utilisateur n'est pas déjà connecté
	if ( is_user_logged_in() ) {
        return "Vous possédez déjà un compte.";
	}

	else { // On propose l'inscription

		ob_start(); // Buffer pour stocker les echo

		echo '<form action="' . admin_url( 'admin-post.php?action=nouvel_utilisateur' ) . '" method="post" id="register-user">';
		// Les champs requis
		echo '<p><label for="nom-user">Pseudo (*) (sans espace, c\'est votre login de conexion)</label><input type="text" name="username" id="nom-user" required></p>';
		echo '<p><label for="email-user">Email (*)</label><input type="email" name="email" id="email-user" required></p>';
		echo '<p><label for="pass-user">Mot de passe (*)</label><input type="password" name="pass" id="pass-user" required><br>';
		echo '<input type="checkbox" id="show-password"><label for="show-password">Voir le mot de passe</label></p>';
		echo '<input type="hidden" name="user_role" value="'. $atts['user_role'] .'">';
		
		// Nonce (pour vérifier plus tard que l'action a bien été initié par l'utilisateur)
		wp_nonce_field( 'create-' . $_SERVER['REMOTE_ADDR'], 'user-front', false );

		// Validation
		echo '<input type="submit" value="Créer mon compte">';
		echo '</form>';

		wp_enqueue_script( 'inscription-front' ); // Enqueue de scripts qui vont nous permettre de vérifier les champs

		$return_code = ob_get_contents(); // On récupère tout le contenu
		ob_end_clean();

		return $return_code; 
	}
}
add_shortcode( 'registerform', 'register_user_form' );

function ajouter_utilisateur() {
	// Vérifier le nonce (et n'exécuter l'action que s'il est valide)
	if( isset( $_POST['user-front'] ) && wp_verify_nonce( $_POST['user-front'], 'create-' . $_SERVER['REMOTE_ADDR'] ) ) {

		// Vérifier les champs requis
		if ( ! isset( $_POST['username'] ) || ! isset( $_POST['email'] ) || ! isset( $_POST['pass'] ) ) {
			wp_redirect( site_url( '/inscription/?message=not-user' ) );
			exit();
		}
		
		$user_role = $_POST['user_role'];
		$username = $_POST['username'];
		$email = $_POST['email'];
		$pass = $_POST['pass'];

		// Vérifier que l'user (email ou nom) n'existe pas
		if ( is_email( $email ) && ! username_exists( $username )  && ! email_exists( $email ) ) {

			// Création de l'utilisateur
			$userdata = array(
				'role'	=> $user_role,
			    'user_login'  =>  $username,
			    'user_email'  =>  $email,
			    'user_pass'   =>  $pass,
			);

			// Insertion de l'utilisateur dans la BDD
			$user_id = wp_insert_user( $userdata );
	        $user = new WP_User( $user_id );

	        // Envoie un mail de notification au nouvel utilisateur
	        wp_new_user_notification( $user_id, null, 'both' );
	    } else {
	    	wp_redirect( site_url( '/inscription/?message=already-registered' ) );
			exit();
	    }

		// Connexion automatique du nouvel utilisateur
	    $creds = array();
		$creds['user_login'] = $username;
		$creds['user_password'] = $pass;
		$creds['remember'] = false;
		$user = wp_signon( $creds, false );

		// Redirection
		wp_redirect( site_url( '/ajouter-mon-association/?message=bienvenue' ) );
		exit();
	}
}

?>