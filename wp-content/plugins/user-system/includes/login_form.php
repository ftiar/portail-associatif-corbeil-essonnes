<?php 

function login_form() {
	if ( is_user_logged_in() ) {
		$current_user = wp_get_current_user();
	    if ( !($current_user instanceof WP_User) )
	    	return;
	    else {
			$code = '<p>Bonjour ' . $current_user->user_login . '.</p>
	    			<p><a href="' . admin_url( 'user-edit.php?user_id='. get_current_user_id() ) .'">Accès au profil</a></p>
	    			<p><a href="' . home_url() .'/deconnexion">Déconnexion</a></p>';
		}
	}
	else {
		$code = '<form id="login" method="post" action="'.wp_login_url(get_permalink(65)).'">
					<fieldset>
						<label>Login :</label>
						<input type="text" value="" name="log" />
						<label>Mot de passe : </label>
						<input type="password" value="" name="pwd"  />
						<input type="submit" value="Connexion" />
						<a class="button-minimal" href="' . home_url() .'/inscription">Inscription</a>
					</fieldset>
					<a href="' . wp_lostpassword_url() . '">Mot de passe perdu ?</a>
				</form>';
	}
    return $code;
}
add_shortcode( 'loginform', 'login_form' );

?>