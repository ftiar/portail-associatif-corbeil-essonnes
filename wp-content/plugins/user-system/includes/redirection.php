<?php 

add_action('send_headers', 'site_router');
function site_router() {
	// Manipulation d'url
	$root = str_replace('index.php', '', $_SERVER['SCRIPT_NAME']);
	$url = str_replace($root, '', $_SERVER['REQUEST_URI']);
	$url = explode('/', $url);

	// En cas de deconnexion, on redirige immédiatement sur la page d'accueil
	if($url[0] === 'deconnexion') {
		wp_logout(); // On déconnecte le membre
		header('location: '.$root); // On redirige
		die();
	}

	// Si l'utilisateur n'est pas connecté, pas de vue profil
	if($url[0] === 'profil') {
		if ( !is_user_logged_in() ) {
			header('location: '.$root.'/connexion/'); // On redirige
			die();
		}
	}

	// Si l'utilisateur est déjà connecté, il ne peut pas voir la page inscription et connexion
	if($url[0] === 'connexion' || $url[0] === 'inscription') {
		if ( is_user_logged_in() ) {
			header('location: '.$root.'/profil/'); // On redirige
			die();
		}
	}

}

?>