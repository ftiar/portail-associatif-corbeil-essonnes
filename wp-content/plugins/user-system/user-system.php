<?php
/**
*    Plugin Name: User System
*    Description: Un plugin permettant d'ajouter des rôles utilisateur, des formulaires d'inscription et de connexion en front etc...
*    Version: 1.1
*    Author: Florian TIAR
*    Author URI: http://tiar-florian.fr
*    License: GPL2
*/

/* !---------------------------------------------------------------------------------*/
/* !	SETTINGS CONSTANTS								 */
/*-----------------------------------------------------------------------------------*/

add_action( 'after_setup_theme', '_usersystem_set_constants' );
function _usersystem_set_constants(){
	/*
 	* Defines the plug-in directory url
 	* <code>url:http://mysite.com/wp-content/plugins/event-organiser</code>
	*/
	if ( ! defined( 'USER_SYSTEM_URL' ) ) {
		define( 'USER_SYSTEM_URL', plugin_dir_url( __FILE__ ) );
	}
}

/*
 * Defines the plug-in directory path
 * <code>/home/mysite/public_html/wp-content/plugins/event-organiser</code>
*/
define( 'USER_SYSTEM_DIR', plugin_dir_path( __FILE__ ) );


/* !---------------------------------------------------------------------------------*/
/* !	CREATE NEW ROLE								 */
/*-----------------------------------------------------------------------------------*/

// Fonction se déclenchant à l'activation du plugin
register_activation_hook( __FILE__, 'sfor_create_role' );
	    
// DOC Rôles et Capacités : http://codex.wordpress.org/fr:R%C3%B4les_et_Capacit%C3%A9s
function sfor_create_role() {
	if ( $author = get_role('author') ) {	    
		remove_role( 'association_owner' );
		$association_owner = add_role( 'association_owner', 'Responsable associatif', $author->capabilities);

	    // On rajoute les capacités du Custom Post Type    
	    $association_owner->add_cap('publish_associations');
	    $association_owner->add_cap('edit_associations');
	    $association_owner->add_cap('delete_associations');

	    $administrator = get_role('administrator');
	    $administrator->add_cap('publish_associations');
	    $administrator->add_cap('edit_associations');
	    $administrator->add_cap('delete_associations');
	    $administrator->add_cap('edit_others_associations');
	    $administrator->add_cap('delete_others_associations');

	    $editor = get_role('editor');
	    $editor->add_cap('publish_associations');
	    $editor->add_cap('edit_associations');
	    $editor->add_cap('delete_associations');
	    $editor->add_cap('edit_others_associations');
	    $editor->add_cap('delete_others_associations');
	}
}

// Filtres nécessaire pour pouvoir ajouter/éditer
add_filter( 'map_meta_cap', 'my_map_meta_cap', 10, 4 );

function my_map_meta_cap( $caps, $cap, $user_id, $args ) {

	/* If editing, deleting, or reading a association, get the post and post type object. */
	if ( 'edit_association' == $cap || 'delete_association' == $cap || 'read_association' == $cap ) {
		$post = get_post( $args[0] );
		$post_type = get_post_type_object( $post->post_type );

		/* Set an empty array for the caps. */
		$caps = array();
	}

	/* If editing a association, assign the required capability. */
	if ( 'edit_association' == $cap ) {
		if ( $user_id == $post->post_author )
			$caps[] = $post_type->cap->edit_posts;
		else
			$caps[] = $post_type->cap->edit_others_posts;
	}

	/* If deleting a association, assign the required capability. */
	elseif ( 'delete_association' == $cap ) {
		if ( $user_id == $post->post_author )
			$caps[] = $post_type->cap->delete_posts;
		else
			$caps[] = $post_type->cap->delete_others_posts;
	}

	/* If reading a private association, assign the required capability. */
	elseif ( 'read_association' == $cap ) {

		if ( 'private' != $post->post_status )
			$caps[] = 'read';
		elseif ( $user_id == $post->post_author )
			$caps[] = 'read';
		else
			$caps[] = $post_type->cap->read_private_posts;
	}

	/* Return the capabilities required by the user. */
	return $caps;
}


include_once( 'functions.php'); // Toolbox
include_once( 'includes/redirection.php'); // Redirections de l'utilisateur
include_once( 'includes/login_form.php'); // Shortcode : Formulaire de connexion
include_once( 'includes/register_form.php'); // Shortcode : Formulaire d'inscription
include_once( 'includes/enqueue_scripts.php'); // Chargement des fichiers js
	
?>