<?php
/**
*    Plugin Name: Post - Connect Associations
*    Description: Un plugin permettant de lier un article à une association
*    Version: 1.0
*    Author: Florian TIAR
*    Author URI: http://tiar-florian.fr
*    License: GPL2
*/

add_action( 'add_meta_boxes', 'add_post_p2p2_association_metabox' );
function add_post_p2p2_association_metabox(){
	add_meta_box(
		'post_association',
		'Association',
		'post_p2p2_association_metabox_content',
		'post',
		'side',
		'default'
	);
}

function post_p2p2_association_metabox_content() {
	$associations = get_posts( array(
			'posts_per_page' => -1,
			'post_type'      => 'association',
			'post_status'    => 'publish',
		)
	);

	if ( ! is_array($associations) || empty($associations) ) {
		return;
	}

	wp_nonce_field( plugin_basename( __FILE__ ), 'post_p2p2_association_nonce' );
	$assoc_id = (int) get_post_meta( get_the_ID(), 'post_p2p2_association', true );

	echo "<p>Association liée à cet article</p>";
	echo "<select id='post_p2p2_association' name='post_p2p2_association'>";

	echo '<option value=0>Aucune association</option>';
	foreach ( $associations as $association ) {
		$id = $association->ID;
		$selected = "";

		if( $id === $assoc_id ){
			$selected = ' selected="selected"';
		}
		echo '<option' . $selected . ' value=' . $id . '>' . $association->post_title . '</option>';
	}
	echo "</select>";
}

add_action('save_post', 'post_p2p2_save_assoc_metabox', 1, 2);
function post_p2p2_save_assoc_metabox( $post_id, $post ){
	// Don't wanna save this now, right?
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
		return;
	if ( ! isset( $_POST['post_p2p2_association_nonce'] ) )
		return;
	if ( ! wp_verify_nonce( $_POST['post_p2p2_association_nonce'], plugin_basename( __FILE__ ) ) )
		return;

	// We do want to save? Ok!
	$key = 'post_p2p2_association';
	$value = $_POST["post_p2p2_association"];
	if ( ! $value || $value == 0 ) {
		delete_post_meta( $post_id, $key );
	}

	if ( get_post_meta( $post_id, $key, FALSE ) ) { // If the custom field already has a value
		update_post_meta( $post_id, $key, $value );
	} else { // If the custom field doesn't have a value
		add_post_meta( $post_id, $key, $value );
	}
}

add_filter('manage_edit-post_columns', 'p2p2_add_post_columns');
function p2p2_add_post_columns( $columns ){
	$columns['p2p2_association'] = 'Association';

	return $columns;
}

add_action('manage_post_posts_custom_column', 'p2p2_fill_post_columns', 10, 2);
function p2p2_fill_post_columns( $column_name, $id ) {
	switch ($column_name) {
		case 'p2p2_association':
			$assoc_id = get_post_meta($id, 'post_p2p2_association', true);
			if ( $assoc_id == 0 ) {
				echo 'Aucune association';
			} else {
				$assoc = get_post($assoc_id);
				$permalink = get_permalink($assoc_id);
				echo "<a href='" . $permalink . "'>" . $assoc->post_title . "</a>";
			}
			break;
		default:
			break;
	} // end switch
}

add_action( 'save_post', 'cb_save_frontend_post' );
function cb_save_frontend_post( $post_id ) {

	if( get_post_type($post_id) !== 'post' ) {
		return;
	}

	if( is_admin() ) {
		return;
	}

	// add link between association and post
	update_post_meta( $post_id, 'post_p2p2_association', absint( $_POST['association'] ) );
}