<?php
/**
 * The template used for displaying featured page content in single-association.php
 *
 * @package Sequential
 */
?>

<?php
	$president = get_field("president");
	$adresse = get_field("adresse");
	$tel = get_field("tel");
	$email = get_field("email");
	$site_internet = get_field("site_internet");

	$logo = get_field('logo');
	if( empty($logo) ) {
		$size = 'medium'; // (thumbnail, medium, large, full or custom size)
		$logo = wp_get_attachment_image( $logo, $size );
	} else {
		$logo = null;
	}
?>

<?php if ( $logo !== null ): ?>
	<?php echo $logo; ?>
<?php endif; ?>

<h2><?php the_title(); ?></h2>

<?php $category = get_the_category( $post->ID );
if ( ! empty( $category ) ): ?>
	<p>Catégorie : <?php echo $category[0]->cat_name; ?></p>
<?php endif; ?>

<h3>Présentation de l'association</h3>

<div class="entry-content">
	<?php the_content(); ?>
	<hr />
</div>

<div class="entry-identity">
	<p><span class="genericon genericon-user"></span>Président : <?php echo $president; ?></p>
	<?php if ( $adresse ): ?>
		<p><span class="genericon genericon-location"></span>Adresse : <?php echo $adresse; ?></p>
	<?php endif; ?>
	<?php if ( $tel ): ?>
		<p><span class="genericon genericon-handset"></span>Téléphone : <?php echo $tel; ?></p>
	<?php endif; ?>
	<p><span class="genericon genericon-mail"></span>Adresse email : <?php echo $email; ?></p>
	<?php if ( $site_internet ): ?>
		<a href="<?php echo $site_internet;?>"><span class="genericon genericon-website"></span>Visiter le site de l'association : <?php echo $site_internet; ?></a>
	<?php endif; ?>
	<hr />
</div>

<?php 
 /**
  * Associations, posts and events of an association
  * @author: Florian TIAR
 **/

	$assoc_posts = new WP_Query( array(
		'post_type' => 'post',
		'post_status' => 'publish',
		'posts_per_page' => -1,
		'meta_key'   => 'post_p2p2_association',
		'meta_value' => get_queried_object_id()
	) );

	$assoc_events = new WP_Query( array(
		'post_type' => 'event',
		'post_status' => 'publish',
		'posts_per_page' => -1,
		'meta_key'   => 'event_p2p2_association',
		'meta_value' => get_queried_object_id()
	) );
?>

	<?php if ( $assoc_posts->have_posts() ) : ?>
		<div class="grid-area">
			<div class="wrapper clear">

				<h3>Les articles de l'association</h3>

				<?php while ( $assoc_posts->have_posts() ) : $assoc_posts->the_post(); ?>
					<div class="column-1-3">
						<?php get_template_part( 'content', 'grid' ); ?>
					</div>
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>

			</div><!-- .wrapper -->
		</div><!-- .grid-area -->
	<?php endif; ?>

	<?php if ( $assoc_events->have_posts() ) : ?>
		<div class="grid-area">
			<div class="wrapper clear">

				<h3>Les événements de l'association</h3>

				<?php while ( $assoc_events->have_posts() ) : $assoc_events->the_post(); ?>
					<div class="column-1-3">
						<?php get_template_part( 'content', 'grid' ); ?>
					</div>
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>

			</div><!-- .wrapper -->
		</div><!-- .grid-area -->
	<?php endif; ?>
