<?php
/**
 * Template Name: Single Page Association
 *
 */

get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="wrapper">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'content', 'association' ); ?>
				<?php endwhile; ?>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>