<?php
/**
 * The template for displaying lists of events
 */

//Call the template header
get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<h1 class="entry-title">
		<?php
			if( eo_is_event_archive('day') )
				//Viewing date archive
				echo __('Events: ','eventorganiser').' '.eo_get_event_archive_date('jS F Y');
			elseif( eo_is_event_archive('month') )
				//Viewing month archive
				echo __('Events: ','eventorganiser').' '.eo_get_event_archive_date('F Y');
			elseif( eo_is_event_archive('year') )
				//Viewing year archive
				echo __('Events: ','eventorganiser').' '.eo_get_event_archive_date('Y');
			else
				_e('Events','eventorganiser');
		?>
		</h1>

		<?php if ( have_posts() ) : ?>

			<!-- Navigate between pages-->
			<!-- In TwentyEleven theme this is done by twentyeleven_content_nav-->
			<?php 
			global $wp_query;
			if ( $wp_query->max_num_pages > 1 ) : ?>
				<nav id="nav-above">
					<div class="nav-next events-nav-newer"><?php next_posts_link( __( 'Later events <span class="meta-nav">&rarr;</span>' , 'eventorganiser' ) ); ?></div>
					<div class="nav-previous events-nav-newer"><?php previous_posts_link( __( ' <span class="meta-nav">&larr;</span> Newer events', 'eventorganiser' ) ); ?></div>
				</nav><!-- #nav-above -->
			<?php endif; ?>

			<?php /* Start the Loop */ ?>

			<?php while ( have_posts() ) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<header class="entry-header">

					<h2 class="entry-title" style="display: inline;">
					<?php
						//If it has one, display the thumbnail
						the_post_thumbnail('thumbnail', array('style'=>'float:left;margin-right:20px;'));
					?>
						<a href="<?php the_permalink(); ?>"><?php the_title();?></a>
					</h2>

					<div class="entry-meta">

						<!-- Output the date of the occurrence-->
						<?php
							//Format date/time according to whether its an all day event.
							//Use microdata https://support.google.com/webmasters/bin/answer.py?hl=en&answer=176035
	                        if( eo_is_all_day() ){
								$format = 'd F Y';
								$microformat = 'Y-m-d';
							} else{
								$format = 'd F Y '.get_option('time_format');
								$microformat = 'c';
							}?>
							<time itemprop="startDate" datetime="<?php eo_the_start($microformat); ?>"><?php eo_the_start($format); ?></time>

						<!-- Display event meta list -->
						<?php echo eo_get_event_meta_list(); ?>

					</div><!-- .event-entry-meta -->

					<div style="clear:both;"></div>
				</header><!-- .entry-header -->

				<div class="entry-content">
					<?php
						the_excerpt();
					?>
				</div><!-- .entry-content -->

			</article><!-- #post-<?php the_ID(); ?> -->

    		<?php endwhile; ?><!--The Loop ends-->

			<!-- Navigate between pages-->
			<?php if ( $wp_query->max_num_pages > 1 ) : ?>
				<nav id="nav-below">
					<div class="nav-next events-nav-newer"><?php next_posts_link( __( 'Later events <span class="meta-nav">&rarr;</span>' , 'eventorganiser' ) ); ?></div>
					<div class="nav-previous events-nav-newer"><?php previous_posts_link( __( ' <span class="meta-nav">&larr;</span> Newer events', 'eventorganiser' ) ); ?></div>
				</nav><!-- #nav-below -->
			<?php endif; ?>

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>

		<?php sequential_paging_nav(); ?>

	</main><!-- #main -->
</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>