<?php
/**
 * @package Sequential
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
		if ( 'post' == get_post_type() ) {
			sequential_post_thumbnail();
		}
	?>

	<header class="entry-header">
		<div class="entry-meta">
			<?php
				$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
				$time_string = sprintf( $time_string,
					esc_attr( get_the_date( 'c' ) ),
					esc_html( get_the_date() ),
					esc_attr( get_the_modified_date( 'c' ) ),
					esc_html( get_the_modified_date() )
				);

				$assoc = get_posts( array(
					'order' => 'ASC',
					'post_type' => 'association',
					'author' => $post->post_author
				));

				$category = get_the_category( $post->ID );
				$cat_name = $category[0]->cat_name;
			?>
			<span class="posted-on"><?php echo $time_string ?></span>
			<span class="posted-on">Catégorie : <?php echo $cat_name ?></span>
			<?php if ( !empty($assoc) ) : ?>
				<span class="author vcard">Publié par : <a class="url fn n" href="<?php echo esc_url( $assoc[0]->guid )  ?>"><?php echo esc_html( $assoc[0]->post_title ) . ' (' . esc_html( get_the_author() ) . ')'; ?></a></span>
			<?php else: ?>
				<span class="author vcard">Publié par : <a class="url fn n" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) ?>"><?php echo esc_html( get_the_author() ) ?></a></span>
			<?php endif; ?>
			<?php sequential_entry_meta(); ?>
		</div><!-- .entry-meta -->

		<?php the_title( sprintf( '<h1 class="entry-title"><a href="%s" rel="bookmark">', esc_url( sequential_get_link_url() ) ), '</a></h1>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			if ( ! is_search() ) {
				the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'sequential' ) );
				wp_link_pages( array(
					'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'sequential' ) . '</span>',
					'after'       => '</div>',
					'link_before' => '<span>',
					'link_after'  => '</span>',
				) );
			} else {
				the_excerpt();
			}
		?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->
