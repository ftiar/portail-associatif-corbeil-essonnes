<?php 

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}

if ( ! function_exists( 'sequential_entry_meta' ) ) :
/**
 * Prints HTML with meta information for the post format, date and edit link.
 */
function sequential_entry_meta() {
    $format = get_post_format();
    $formats = get_theme_support( 'post-formats' );

    if ( $format && in_array( $format, $formats[0] ) ) : // If has post format ?>

        <span class="format-badge"><a href="<?php echo esc_url( get_post_format_link( $format ) ); ?>" title="<?php echo esc_attr( sprintf( __( 'All %s posts', 'sequential' ), get_post_format_string( $format ) ) ); ?>"><?php echo get_post_format_string( $format ); ?></a></span>

    <?php endif;

        if ( is_sticky() && ! is_single() ) {

            $featured = '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . __( 'Featured', 'sequential' ) . '</a>';
            echo '<span class="featured">' . $featured . '</span> ';

        }

        edit_post_link( __( 'Edit', 'sequential' ), '<span class="edit-link">', '</span>' );
}
endif;

add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar() {
	if ( ! current_user_can('administrator') && ! is_admin() ) {
		show_admin_bar(false);
	}
}