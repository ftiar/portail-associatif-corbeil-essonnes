<?php
/*
Template Name: Ajouter Association
*/
acf_form_head();
get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<h2>Ajouter mon association</h2>

			<?php if ( ! is_user_logged_in() ) : ?>
				<p>Vous devez être connecté pour créer votre association</p>
				<a href="<?php echo home_url('connexion') ?>" class="button button-primary">Connexion</a>
			<?php else: ?>

				<?php acf_form(array(
					'post_id'		=> 'new_post',
					'post_title' 	=> true,
					'post_content'  => true,
					'new_post'		=> array(
						'post_type'		=> 'association',
						'post_status'	=> 'draft'
					),
					'field_groups' 	=> array(36),
					'submit_value'	=> 'Créer mon association',
					'return' 		=> 'association-en-brouillon/'
				)); ?>

			<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>