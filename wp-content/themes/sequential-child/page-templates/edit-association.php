<?php
/*
Template Name: Modifier Association
*/
acf_form_head(); ?>
<?php get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<h2>Modifier mon association</h2>

			<?php
				if( is_user_logged_in() && isset($_GET['association_id'] )) {
					$assoc_id = $_GET['association_id'];

					$post = get_post( $assoc_id );
					if ( is_null($post) ) {
						echo '<p>Association introuvable</p>';
					} else {
						if ( (int) $post->post_author === get_current_user_id() ) {
							acf_form( array(
								'post_id'	=> $assoc_id,
								'post_title'	=> true,
								'post_content'  => true,
								'post_status'   => 'pending',
								'submit_value'	=> 'Mettre à jour mon association !',
							));
						} else {
							echo '<p>Vous n\'êtes pas l\'auteur de cet article.</p>';
						}
					}
				}
				else {
					echo '<p>Vous n\'êtes pas connecté.</p><a href="' . home_url('connexion') . '" class="button button-primary">Connexion</a>';
				}
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>