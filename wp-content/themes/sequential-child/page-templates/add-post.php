<?php
/*
Template Name: Ajouter Article
*/
acf_form_head(); ?>
<?php get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<h2>Ajouter un article</h2>

			<?php

			if ( ! is_user_logged_in() ) : ?>
				<p>Vous devez être connecté pour publier un article</p>
				<a href="<?php echo home_url('connexion') ?>" class="button button-primary">Connexion</a>
			<?php else :

				$associations = new WP_Query( array(
					'posts_per_page' => -1,
					'post_type'      => 'association',
					'post_status'    => 'publish',
					'author'         => get_the_author_meta('ID')
				));

				if ( ! $associations->have_posts() ) : ?>

					<p>Seul les utilisateurs possédant une association peuvent publier un article</p>
					<a href="<?php echo home_url('ajouter-mon-association') ?>" class="button button-primary">Créer mon association</a>;

				<?php else: ?>

					<form id="post" class="acf-form" action="" method="post">

						<p><label for="my-frontend-post-association">Association</label>
							<select id="my-frontend-post-association" name="association">
								<?php foreach ( $associations->posts as $assoc ) : ?>
									<option value="<?php echo $assoc->ID ?>"><?php echo $assoc->post_title ?></option>;
								<?php endforeach; ?>
								<?php wp_reset_postdata(); ?>
							</select>
						</p>

						<?php acf_form( array(
							'post_id'		=> 'new_post',
							'post_title' 	=> true,
							'post_content'  => true,
							'new_post'		=> array(
								'post_type'		=> 'post',
								'post_status'	=> 'draft'
							),
							'form' => false,
						)); ?>

						<input type="submit" value="Envoyer" />

					</form>

				<?php endif; ?>

			<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>