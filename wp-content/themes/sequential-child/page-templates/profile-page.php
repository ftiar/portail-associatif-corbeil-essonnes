<?php
/**
 * The template for displaying profile page.
 *
 * Template Name: Profile Page
 *
 * Template Author: Florian TIAR
 *
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php the_content(); ?>

				<?php if ( is_user_logged_in() ) {
					$user_info = get_userdata(get_current_user_id());
			        $registerDate = new DateTime($user_info->user_registered);
			        $registerDate = date_fran($registerDate); // On françise la date

			        echo '<div class="user-infos">';
				        echo '<p>Bienvenue ' . $user_info->user_login . ' !' . "</p>";
						echo '<p>Adresse email: ' . $user_info->user_email . "</p>";
						echo '<p>Date d\'inscription: ' . $registerDate. "</p>";
						if ($user_info->first_name)
							echo '<p>Prénom: ' . $current_user->first_name . "</p>";
						if ($user_info->last_name)
							echo '<p>Nom: ' . $current_user->last_name . "</p>";
			        echo '</div>';

			        echo '<div class="user-associations">';
						$args = array(
							'order' => 'ASC',
							'post_type' => 'association',
							'author' => get_current_user_id()
						 );
						// On récupére toutes les associations de l'auteur
						$associationsPost = get_posts( $args ); 

						echo '<table><thead><tr><th>Nom de l\'association</th><th>Date de création</th><th>Actions</th></tr></thead>';
						echo '<tbody>';
						foreach ($associationsPost as $key => $association) {
							echo '<tr>';
							echo '<td><a href="'.$association->guid.'">'.$association->post_title.'</a></td>';
					        $post_date = new DateTime($association->post_date);
							echo '<td>'.$post_date->format('d/m/Y').'</td>';
							echo '<td><a href="'.get_site_url().'/modifier-mon-association/?association_id='.$association->ID.'">Modifier</a>';
							echo ' | ';
							echo '<a href="'.get_site_url() . wp_nonce_url('/wp-admin/post.php?action=delete&post='.$association->ID, 'delete-post_'. $association->ID).' ">Supprimer</a></td>';
							echo '</tr>';
						}
						echo '</tbody></table>';
			        echo '</div>';

			        echo '<div class="user-posts">';
						$args = array(
							'order' => 'ASC',
							'post_type' => 'post',
							'author' => get_current_user_id()
						 );
						// On récupére toutes les user_posts de l'auteur
						$user_posts = get_posts( $args ); 

						echo '<table><thead><tr><th>Nom de l\'article</th><th>Date de création</th><th>Actions</th></tr></thead>';
						echo '<tbody>';
						foreach ($user_posts as $key => $post) {
							echo '<tr>';
							echo '<td><a href="'.$post->guid.'">'.$post->post_title.'</a></td>';
					        $post_date = new DateTime($post->post_date);
							echo '<td>'.$post_date->format('d/m/Y').'</td>';
							echo '<td><a href="'.get_site_url().'/modifier-mon-article/?post_id='.$post->ID.'">Modifier</a>';
							echo ' | ';
							echo '<a href="'.get_site_url() . wp_nonce_url('/wp-admin/post.php?action=delete&post='.$post->ID, 'delete-post_'. $post->ID).' ">Supprimer</a></td>';
							echo '</tr>';
						}
						echo '</tbody></table>';
			        echo '</div>';

			        echo '<div class="user-events">';
						$args = array(
							'order' => 'ASC',
							'post_type' => 'event',
							'author' => get_current_user_id()
						 );
						// On récupére toutes les user_events de l'auteur
						$user_events = get_posts( $args ); 

						echo '<table><thead><tr><th>Nom de l\'événement</th><th>Date de création</th><th>Actions</th></tr></thead>';
						echo '<tbody>';
						foreach ($user_events as $key => $event) {
							echo '<tr>';
							echo '<td><a href="'.$event->guid.'">'.$event->post_title.'</a></td>';
					        $post_date = new DateTime($event->post_date);
							echo '<td>'.$post_date->format('d/m/Y').'</td>';
							echo '<td><a href="'.get_site_url().'/modifier-mon-evenement/?event_id='.$event->ID.'">Modifier</a>';
							echo ' | ';
							echo '<a href="'.get_site_url() . wp_nonce_url('/wp-admin/post.php?action=delete&post='.$event->ID, 'delete-post_'. $event->ID).' ">Supprimer</a></td>';
							echo '</tr>';
						}
						echo '</tbody></table>';
			        echo '</div>';

				} else {  
					echo '<h3>Vous n\'êtes pas connecté.</h3>';
				} ?> 

			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>