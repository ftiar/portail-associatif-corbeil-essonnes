<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Sequential
 */

get_header(); 

$catargs = array(
    'orderby'                  => 'name',
    'order'                    => 'ASC',
); 

$categories = get_categories( $catargs );

?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

            <h2>Annuaire des associations :</h2>

            <?php foreach ($categories as $category) :?>
                <h3><?php echo $category->name; ?></h3>

                <?php $args = array ( 
                    'posts_per_page'   => -1,
                    'cat'              => $category->cat_ID,
                    'orderby'          => 'title',
                    'order'            => 'ASC',
                    'post_type'        => 'association',
                );
                $query = new WP_Query( $args ); ?>
                <?php if ( $query->have_posts() ) : ?>
                    <ul>
                    <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                        <li><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></li>
                    <?php endwhile; ?>
                    </ul>
                    <hr />
                <?php endif; ?>

                <?php wp_reset_postdata(); ?>

            <?php endforeach; ?>

			<?php sequential_paging_nav(); ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>