<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clefs secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur 
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C'est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d'installation. Vous n'avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'corbeilassoc');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', '');

/** Adresse de l'hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8');

/** Type de collation de la base de données. 
  * N'y touchez que si vous savez ce que vous faites. 
  */
define('DB_COLLATE', '');

/**#@+
 * Clefs uniques d'authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant 
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n'importe quel moment, afin d'invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '^K|nU|--lY<L1+;.~O,#9- UU#n38tpZJ:qC/a]Z2whfIvci~iY>=R&W>C.<h,wV');
define('SECURE_AUTH_KEY',  '[JBp-qe(eMfy]=4,j#~k_~]IVgctV|a}M]tjmwk<k(ZK@z{o^du?[xmX1wLAjPlH');
define('LOGGED_IN_KEY',    '0H6+j9+|DXvyo4t*ql-|y%B>nH-VBQaeW_K~#Qe/{9.3YYXm5g%!P<&Fd![x0rxm');
define('NONCE_KEY',        'J6*B|h+@XbYeLRfU:(l5C5eHd5WA`HAslxRMzZxb/0N:n@BfUXt>uZRMkwPrEv@0');
define('AUTH_SALT',        'lr)l(>k+w>s6^e1Tj2{Px=Ed%t;Ki[k 1I}V>W-@K-<a}/@0OU_;V:W.W~rAlQCK');
define('SECURE_AUTH_SALT', 't=GkKA+F9sis*.`DZ|:R_kUBhJl?uw84BOB9]G|XnAtf*+}myy;qm%<|SG_vN>M_');
define('LOGGED_IN_SALT',   'K$Zq$Ad:wG,BjG4hM3hl<XnTm&mvggHaDdK|4$?-zn%vN>8:,PN=2b,Zw50-K0wk');
define('NONCE_SALT',       '_?R@w^$:C1h4g#Yhf}lz8KKhu<OsZGk3&-a@Vj)SA|9ixBJ=0bRe+Ea{i[=f)h0o');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique. 
 * N'utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés!
 */
$table_prefix  = 'crbass_';

/** 
 * Pour les développeurs : le mode deboguage de WordPress.
 * 
 * En passant la valeur suivante à "true", vous activez l'affichage des
 * notifications d'erreurs pendant votre essais.
 * Il est fortemment recommandé que les développeurs d'extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de 
 * développement.
 */ 
define('WP_DEBUG', true); 

set_time_limit(60); /* Problème d'execution */


/* C'est tout, ne touchez pas à ce qui suit ! Bon blogging ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');